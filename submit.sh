#!/bin/bash

# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

set -x

TARGET=$ALL_CCFRWORK/rag_time-1
HERE=$(dirname $(realpath $BASH_SOURCE))

rsync -v  -r $HERE/ $TARGET \
 --exclude .git \
 --exclude .venv \
 --exclude data

