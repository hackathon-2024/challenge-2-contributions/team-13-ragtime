# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

# Create a Params class to store all the parameters for the model and its run
import os
from ragtime.utils import JSONable

current_folder = os.path.dirname(os.path.abspath(__file__))


class Params(JSONable):
    def __init__(self):
        self.write_instruction = ""
        self.device = "LOCAL"
        with open(f"{current_folder}/../../vars/hf_dir.var", "r") as f:
            self.hf_dir = f.read().strip()

    def __str__(self):
        return str(self.toJSON())
