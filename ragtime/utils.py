# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import json
from pathlib import PosixPath
import numpy as np
import pandas as pd
from langchain_core.documents import Document
from ragtime.logger.logger import log
from dataclasses import dataclass
from typing import List
from pathlib import Path
import psutil
import GPUtil
from langchain.prompts import PromptTemplate


@dataclass
class QASample:
    id: int
    question: str
    answers: List[str]
    sources: List[str]


def load_qa_samples(dataset_path: Path, is_file=False):
    if not is_file:
        questions = pd.read_csv(dataset_path / "questions.csv", sep=";")
    else:
        questions = pd.read_csv(dataset_path, sep=";")
    expected_columns = ["id", "question"]
    assert all(
        col in questions.columns for col in expected_columns
    ), "Columns 'id' and 'question' are missing in the DataFrame."

    answers_dir = dataset_path / "output/answers"
    source_dir = dataset_path / "output/sources"

    for id, question in questions.itertuples(index=False):
        answers =[]
        sources = []

        yield QASample(id, question, answers, sources)


def convert_to_json(value):
    if isinstance(value, (int, float, str)):
        return value
    elif isinstance(value, dict):
        return {key: convert_to_json(value) for key, value in value.items()}
    elif isinstance(value, (list, set)):
        return [convert_to_json(item) for item in value]
    elif isinstance(value, JSONable):
        return value.toJSON()
    elif isinstance(value, np.ndarray):
        return [convert_to_json(item) for item in value.tolist()]
    elif isinstance(value, Document):
        return value.__dict__
    elif isinstance(value, PosixPath):
        return str(value)
    elif isinstance(value, bytes):
        return "bytes"
    else:
        log(
            f"Warning: The value of type {type(value)} is not JSONable. Returning the json.dumps or the __dict__.",
        )
        try:
            return json.dumps(value)
        except TypeError:
            try:
                d = value.__dict__
                return {key: convert_to_json(value) for key, value in d.items()}
            except AttributeError:
                log(f"We tried with {type(value)}, but can't")
                return ""


class JSONable(object):
    def toJSON(self):
        return {key: convert_to_json(value) for key, value in self.__dict__.items()}


def get_system_usage():
    # Get CPU usage
    cpu_usage = psutil.cpu_percent(interval=1)

    # Get RAM usage
    ram = psutil.virtual_memory()
    ram_usage = ram.percent

    # Get GPU usage (for NVIDIA GPUs)
    gpus = GPUtil.getGPUs()
    if gpus:
        gpu_usage = gpus[0].load * 100  # Assuming you want to monitor the first GPU
    else:
        gpu_usage = "No NVIDIA GPU detected."

    print(f"CPU Usage: {cpu_usage}%")
    print(f"RAM Usage: {ram_usage}%")
    print(f"GPU Usage: {gpu_usage if isinstance(gpu_usage, str) else f'{gpu_usage}%'}")

def similarity(a, b):
    """
    Get the cosine similarity of two strings.

    Args:
        a (str): The first string.
        b (str): The second string.

    Returns:
        float: The cosine similarity of the two strings.
    """
    a = a.split()
    b = b.split()
    a = set(a)
    b = set(b)
    c = a.intersection(b)
    return float(len(c)) / (len(a) * len(b))

def get_cosine_similarity(res, platforms):
    """
    Get the cosine similarity of the results.

    Args:
        res (object): The results.
        platforms (list): The list of platforms.

    Returns:
        dict: The cosine similarity of the results.
    """

    res = res.split("\n")
    res = list(filter(None, res))
    print(res)
    cos_scores = {}
    for platform in platforms:
        # print(f"Platform: {platform}")
        cos_scores[platform] = 0
        for r in res:
            cos_scores[platform] += similarity(r, platform)
    print(cos_scores)
    return cos_scores

def translate_text(txt, param_obj, log_obj, llm_obj, language):
    prompt_template = PromptTemplate.from_template(
        "Let's translate a text.\n"
        "\n--------\n"
        "BEGINNING OF EXAMPLES\n"
        "--------\n\n"
        "\nExample 1\n"
        "--------\n"
        "Text:\n"
        "I think you misunderstand what the cosine similarity is."
        "I suggest reading up on what comparing texts based on their cosine.\n"
        "Target Language: French\n"
        "Translation:\n"
        "Je pense que tu ne comprends pas ce qu'est la similarité cosine."
        "Je suggère de lire ce qu'est la comparaison des textes en fonction de leur cosinus \n\n\n"
        "\nExample 2\n"
        "--------\n"
        "Text:\n"
        "Machthaber Kim Jong-un hat Nordkoreas Staatsideologie umgeschrieben."
        "Südkorea erklärt er zum verfeindeten Nachbarn. \n"
        "Target Language: English\n"
        "Translation:\n"
        "Ruler Kim Jong-un has rewritten North Korea's state ideology."
        "He declares South Korea a hostile neighbor. \n\n\n"
        "\n--------\n"
        "END OF EXAMPLES\n"
        "--------\n"
        "Text:\n"
        "{question}\n"
        "Target Language: {language}\n"
        "Translation:\n"
    )
    final_template = prompt_template.format(question=txt, language=language)
    res = llm_obj.invoke(final_template, max_new_tokens=200)
    return res
