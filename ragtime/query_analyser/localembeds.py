# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import os
from typing import Any, Dict, List, Tuple
from langchain_core.pydantic_v1 import Field
from langchain_core.embeddings import Embeddings
from transformers import AutoModel, AutoTokenizer
from ragtime.logger.logger import log
import numpy as np
from pathlib import Path


data_dir = Path(__file__).parents[2].absolute() / "data"


class LocalEmbeddings(Embeddings):
    """Interface for embedding models."""

    model_kwargs: Dict[str, Any] = Field(default_factory=dict)
    """Keyword arguments to pass to the model."""
    device: str
    """The device to run the model on."""

    def __init__(self, **kwargs: Any):
        """Initialize the LocalEmbeddings class.

        Args:
            **kwargs: Additional keyword arguments.

        """
        self.cache_dir = self._cache_dir_name(kwargs)
        self.cache_dir = data_dir / "embeddings" / self.cache_dir
        self.cache_dir.mkdir(parents=True, exist_ok=True)
        self.cache_dir = str(self.cache_dir)
        log(f"Cache dir: {self.cache_dir}")
        log("Initializing the sentence_transformer...")
        self.device = kwargs.get("device", "cpu")
        self.checkpoint = kwargs.get(
            "embedding_model", "intfloat/multilingual-e5-large"
        )
        print(f"Using {self.device} for inference on {self.checkpoint}.")
        if self.device == "cpu":
            os.environ[
                "SENTENCE_TRANSFORMERS_HOME"
            ] = f"/gpfswork/rech/jdh/ujg48ut/tmp/.cache/huggingface/hub"

        self.tokenizer = AutoTokenizer.from_pretrained(
            self.checkpoint, trust_remote_code=True
        )
        self.model = AutoModel.from_pretrained(
            self.checkpoint, trust_remote_code=True
        ).to(self.device)
        self.param_obj = kwargs.get("param_obj", None)
        self.max_seq_length = self.param_obj.max_seq_length

    def _hash(self, text: str) -> str:
        from hashlib import sha256

        hash = sha256()
        hash.update(text.encode("utf-8"))
        return hash.hexdigest()

    def _cache_dir_name(self, kwargs) -> str:
        from hashlib import sha256

        hash = sha256()
        for var in ["checkpoint", "param_obj"]:
            var = str(kwargs.get(var, "")).encode("utf-8")
            log(f"Hashing param {var}")
            hash.update(var)
        return hash.hexdigest()

    def _cache_get(self, text: str) -> Tuple[bool, List[float]]:
        if not Path(self.cache_dir).exists():
            return False, []
        h = self._hash(text)
        if not (Path(self.cache_dir) / h).exists():
            return False, []

        with open(Path(self.cache_dir) / h, "r") as f:
            return True, list(map(float, f.read().split(",")))

    def _cache(self, text: str, embedding: List[float]):
        if hasattr(self, '_toldyouso'):
            return
        self._toldyouso = True
        log("💀 Caching is disabled because it breaks the inode limit.")
        #self.cache_dir.mkdir(parents=True, exist_ok=True)
        # h = self._hash(text)
        #with open(self.cache_dir / h, 'w') as f:
        #    f.write(','.join(map(str, embedding)))

    def embed_text(self, text: str) -> List[float]:
        """Embed a string of text.

        Args:
            text (str): The document to be embedded.

        Returns:
            List[float]: The embedded document as a list of floats.
        """
        hit, embedding = self._cache_get(text)
        if hit:
            return embedding
        encoding = self.tokenizer.__call__(
            text,
            padding="max_length",
            truncation=True,
            max_length=self.max_seq_length,
            return_attention_mask=True,
            return_tensors="pt",
        )
        inputs = encoding["input_ids"].to(self.device)
        attention_mask = encoding["attention_mask"].to(self.device)
        outputs = self.model(
            input_ids=inputs, attention_mask=attention_mask, return_dict=True
        )
        last_hidden_states = outputs.last_hidden_state
        embedding = last_hidden_states.detach().cpu().numpy()
        averaged_embedding = np.mean(embedding, axis=1)
        reshaped_embedding = averaged_embedding.reshape(-1)
        self._cache(text, reshaped_embedding)
        return reshaped_embedding

    def embed_documents(self, texts: List[str]) -> List[List[float]]:
        """Embed a list of documents.

        Args:
            texts: A list of documents to be embedded.

        Returns:
            A list of embedded documents, where each document is
            represented as a list of floats.

        """
        log(f"Embedding {len(texts)} documents...")
        vectors = []
        i = 0
        for text in texts:
            if i % 100 == 0:
                log(f"Embedding document #{i}")
            i += 1
            if len(text) >= 3 * self.max_seq_length:
                log(f"Warning! Document #{i} might be too long.")
                log(f"Compare {len(text)} >= {3 * self.max_seq_length}.")
            vectors.append(self.embed_text(text))
        vectors_array = np.vstack(vectors)
        return vectors_array

    def embed_query(self, text: str) -> List[float]:
        """Embed a query text.

        Args:
            text: The query text to be embedded.

        Returns:
            The embedded query text as a list of floats.

        """
        return self.embed_text(text)


if __name__ == "__main__":
    localembeds = LocalEmbeddings()
