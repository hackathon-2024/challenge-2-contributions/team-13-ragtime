# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

from ragtime.utils import JSONable, similarity, translate_text
from ragtime.logger.logger import log
from langchain.prompts import PromptTemplate

class Query(JSONable):
    def __init__(self, query_txt):
        self.query_original_txt = query_txt


def convert_to_embeddings(query_txt, embedder_obj, param_obj, log_obj):
    """
    Convert the query text to embeddings.

    Args:
        query_txt (str): The query text.
        param_obj (Params): An object containing parameters for the conversion.
        log_obj (Logger): An object for logging purposes.

    Returns:
        List[float]: The embedded document as a list of floats.
    """

    # TODO: Allow for some OAI embeddings?

    param_obj.embedding_model_obj = embedder_obj
    query_embeddings = embedder_obj.embed_text(query_txt)
    return query_embeddings

def detect_nonenglish(query_txt, param_obj, log_obj, llm_obj):
    """
    Detect if the query is non-English.

    Args:
        query_txt (str): The query text.
        param_obj (Params): The Params object.
        log_obj (Logger): The Logger object.

    Returns:
        bool: True if the query is non-English, False otherwise.
    """

    prompt_template = PromptTemplate.from_template(
        "Let's detect the language of texts.\n"
        "\n--------\n"
        "BEGINNING OF EXAMPLES\n"
        "--------\n\n"
        "Example 1\n"
        "--------\n"
        "Text:\n"
        "I think you misunderstand what the cosine similarity is."
        "I suggest reading up on what comparing texts based on their cosine\n"
        "Language:\n"
        "English\n\n\n"
        "Example 2\n"
        "--------\n"
        "Text:\n"
        "Machthaber Kim Jong-un hat Nordkoreas Staatsideologie umgeschrieben."
        "Südkorea erklärt er zum verfeindeten Nachbarn. \n"
        "Language:\n"
        "German\n\n\n"
        "\n--------\n"
        "END OF EXAMPLES\n"
        "--------\n"
        "Text\n"
        "{question}\n"
        "Language:\n"
    )
    final_template = prompt_template.format(question=query_txt)
    res = llm_obj.invoke(final_template, max_new_tokens=50)
    print(f"res is {res}")
    for word in res.split():
        log(f"{word}")
        if word.strip() == "":
            print("blank")
            continue
        elif word == 'English':
            return False, "english"
        elif (similarity(word, 'English') > 0.8) or (similarity(word, 'english') > 0.8):
            return False, "english"
    return True, res.strip()

def analyse(query_txt, embedder_obj, param_obj, log_obj, llm_obj):
    """
    Analyse the query.

    Args:
        query (str): The query to be analysed.
        param_obj (Params): The Params object.
        log_obj (Logger): The Logger object.

    Returns:
        None
    """
    log("Received the query to be analysed.")
    query_obj = Query(query_txt)

    param_obj.non_english, param_obj.language = detect_nonenglish(query_txt, param_obj, log_obj, llm_obj)
    print(param_obj.non_english, param_obj.language)

    if param_obj.non_english:
        query_txt = translate_text(query_txt, param_obj, log_obj, llm_obj, "English")

    log("Converting the query text to embeddings.")
    query_embeddings = convert_to_embeddings(
        query_txt, embedder_obj, param_obj, log_obj
    )
    log("Writing the embeddings from the query to a file.")
    filename = f"{log_obj.log_folder}/embed/{log_obj.run_id}_input.txt"
    with open(filename, "w") as f:
        f.write(str(query_embeddings))
    query_obj.embedding_file = filename
    query_obj.embeddings = query_embeddings
    query_obj.query_txt_for_embed = query_txt
    param_obj.query_txt_for_embed = query_txt
    param_obj.query_original_txt = query_obj.query_original_txt

    return query_obj
