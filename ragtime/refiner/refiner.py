# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

from ragtime.utils import JSONable
from langchain.prompts import PromptTemplate
import json
from ragtime.logger.logger import log
from ragtime.utils import get_cosine_similarity


class RefinedResults(JSONable):
    """
    A class to store the refined results from the datastore.
    """

    def __init__(self, raw_results):
        self.original_snippets = raw_results.snippets
        self.refined_results = raw_results.snippets


def refine(raw_results, param_obj, log_obj, llm_obj):
    """
    Refine the results from the datastore.

    Args:
        raw_results (RawResults): The raw results from the datastore.
        param_obj (Params): The Params object.
        log_obj (Logger): The Logger object.

    Returns:
        None
    """
    # v0: Pass to Datastore Interface
    refined_results = RefinedResults(raw_results)

    def load_parameters(file_path):
        with open(file_path, "r") as file:
            parameters = json.load(file)
        return parameters

    parameters = load_parameters("platforms.json")
    extended_platforms = parameters.keys()
    platforms = [
        "Amazon",
        "Apple",
        "Bing",
        "Booking.com",
        "CrowdTangle",
        "Facebook",
        "Google",
        "Instagram",
        "LinkedIn",
        "Meta",
        "Microsoft",
        "Pinterest",
        "Snap",
        "Tiktok",
        "Twitter",
        "Wikipedia",
        "x",
        "YouTube",
    ]
    prompt_template = PromptTemplate.from_template(
        "For a question, let's detect which digital platform it is talking about.\n"
        "The possible platforms are {options}\n"
        "\n--------\n"
        "BEGINNING OF EXAMPLES\n"
        "--------\n\n"
        "\nExample 1\n"
        "--------\n"
        "Question:\n"
        "What are the conditions that Apple puts in terms of "
        "law enforcement requests?\n"
        "Platform:\n"
        "Apple\n\n\n"
        "Example 2\n"
        "--------\n"
        "Question:\n"
        "Write me a curl command that lets me query how many rooms are "
        "available on Booking right now.\n"
        "Platform:\n"
        "Booking.com\n\n\n"
        "\n--------\n"
        "END OF EXAMPLES\n"
        "\n--------\n"
        "Question:\n"
        "{question}\n"
        "Platform:\n"
    )
    final_template = prompt_template.format(
        question=param_obj.query_txt_for_embed, options=", ".join(platforms)
    )
    log(f"Final Refining Template: {final_template}")
    res = llm_obj.invoke(final_template, max_new_tokens=10)

    # Check cosine similarity to determine the platform closest to it
    # If the cosine similarity is not high enough, return the raw results
    # If the cosine similarity is high enough, return the platform
    # If the cosine similarity is high enough, but there are multiple platforms, return the raw results

    # print(res)
    # print(f"Platforms: {platforms}")
    # print(f"Results: {refined_results}")
    # print(f"Finding cosine similarity...")
    cos_scores = get_cosine_similarity(res, extended_platforms)
    print(f"Cosine similarity: {cos_scores}")

    # Take the platform with the highest cosine similarity
    max_platform = max(cos_scores, key=cos_scores.get)
    print(f"Max platform: {max_platform}")
    print(list(map(float, cos_scores.values())))

    if max(list(map(float, cos_scores.values()))) > 0.2:
        # The values of paramaters are a list of folders where it's ok
        # to retrieve files from
        # So for every item in raw_results, we need to ensure that the source
        # contains one of the folders

        log("Filtering the results")
        filtered_results = []
        for result in refined_results.original_snippets:
            # log(f"for result {result.metadata['source']}")
            for platform in parameters[max_platform]:
                # log(f"Checking if {platform} is in {result.metadata['source']}")
                if platform in result.metadata["source"]:
                    filtered_results.append(result)

        print(f"Filtered results: {filtered_results}")
        if len(filtered_results) < 1:
            print("TOO FEW RESULTS")
            filtered_results = refined_results.original_snippets

        refined_results.refined_results = filtered_results
    else:
        print("NO PLATFORM FOUND")

    if len(refined_results.refined_results) > 10:
        refined_results.refined_results = refined_results.refined_results[0:10]
    # Step 1: Ask the model what he thinks the platform is
    # Step 2: Get a cosine similarity on the key
    # Step 3: Retrieve the platforms.json
    # Step 4: Filter based on the platforms.json values
    # Step 5: If numbers of results < 2, pass the raw_results
    return refined_results
