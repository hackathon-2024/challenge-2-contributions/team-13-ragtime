# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

from ragtime.utils import JSONable, translate_text
from langchain.prompts import PromptTemplate
from ragtime.logger.logger import log
from pathlib import Path


class Response(JSONable):
    """
    A class to store the response.
    """

    def __init__(self, response):
        self.response = response


def write(refined_results, llm_obj, param_obj, log_obj):
    """
    Write the results to the output file.

    Args:
        refined_results (object): The refined results.
        param_obj (object): The parameters.
        log_obj (object): The logger.

    Returns:
        None
    """
    response = Response(refined_results)
    if param_obj.device == "GPU":
        # v0: Pass to Datastore Interface
        prompt_template = PromptTemplate.from_template(
            "From some context, let's retrieve information to answer questions.\n"
            "In the following examples, you will not have context, but you will have it"
            " for the actual questions.\n"
            "\n--------\n"
            "BEGINNING OF EXAMPLES\n"
            "--------\n\n"
            "Example 1\n"
            "--------\n"
            "Question:\n"
            "How can I limit the search results returned by the WorldCorp "
            "Custom Search JSON API to only include specific fields, in order "
            "to improve the performance of my application and reduce bandwidth "
            "usage?\n"
            "Answer:\n"
            "To limit the search results returned by the WorldCorp Custom Search "
            "JSON API to only include specific fields, thereby improving your "
            "application's performance and reducing bandwidth usage, you can"
            "use the fields request parameter in your search request. This "
            "parameter allows you to specify exactly which fields in the "
            "response JSON you want the server to return. By requesting "
            "only the parts of the data you actually need, you can make "
            "your application more efficient in terms of network, CPU, "
            "and memory usage.\n"
            "Here's how you might structure a request to only include "
            "specific fields:\n"
            "```\nGET https://www.worldcorp.com/customsearch/v1?"
            "key=YOUR_API_KEY&cx=YOUR_SEARCH_ENGINE_ID&q=YOUR_"
            "SEARCH_QUERY&fields=items(link,title,snippet)\n```\n"
            "In this example, the fields parameter is set to items "
            "(link,title,snippet), which means that the response will only "
            "include the link, title, and snippet fields of each item in the "
            "items array of the search results. This effectively narrows down "
            "the response to contain only the essential information you need, "
            "omitting any additional metadata or fields that are not specified "
            "in the fields parameter.\n\n\n"
            "Example 2\n"
            "--------\n"
            "Question:\n"
            "How can I search for ads related to specific social issues, "
            "elections, or politics within the MegaCool Ad Library API, "
            "particularly those that targeted the United States audience "
            "and mentioned 'California'? Only return the curl request.\n"
            "Answer:\n"
            "```curl -G \\ \n"
            "-d \"search_terms='california'\" \\ "
            "-d \"ad_type=POLITICAL_AND_ISSUE_ADS\" \\ "
            "-d \"ad_reached_countries=['US']\" \\ "
            "-d \"access_token=<ACCESS_TOKEN>\" \\ "
            "\"https://graph.megacool.com/<API_VERSION>/ads_archive\" \\ "
            "```\n\n\n"
            "\n--------\n"
            "END OF EXAMPLES\n"
            "--------\n"
            "Context:\n"
            "{context}\n"
            "Question:\n"
            "{question}\n"
            "Answer:\n"
        )
        better_context = []
        i = 0
        for result in refined_results.refined_results:
            i += 1
            content = result.page_content
            source = result.metadata["source"]
            url = result.metadata["url"]
            better_context.append(
                f"Beginning of document #{i}\n"
                f"From the following URL: {url}:\n"
                f"\t {content}\n"
                f"End of document #{i}\n\n") 
        final_template = prompt_template.format(
            context="\n".join(better_context), question=param_obj.query_txt_for_embed
        )
        # log(f"Final Template: {final_template}")
        log("Calling the model...")
        res = llm_obj.invoke(final_template, max_new_tokens=500)
        response.prompt = final_template
        response.response = res
        response.sources = list(
            map(lambda doc: doc.metadata["url"], refined_results.refined_results)
        )
        print(response.sources)
        # Transform response sources in a string of bullet points
        # Filter out None values from the list
        filtered_sources = [source for source in response.sources if source is not None]
        response.sources = "\n".join(set(filtered_sources))
    return response


def outwrite(response, question_id, param_obj, log_obj, llm_obj):
    """
    Write the results to the output file.

    Args:
        response (object): The response.
        param_obj (object): The parameters.
        log_obj (object): The logger.

    Returns:
        None
    """
    # v0: Pass to Datastore Interface
    print("Output:", response.response)

    if param_obj.non_english:
        response.response = translate_text(
            response.response, param_obj, log_obj, llm_obj, param_obj.language
        )

    output_dir_run = Path(param_obj.path_output_dir_run)
    output_dir_run.mkdir(parents=True, exist_ok=True)
    output_dir_run = str(output_dir_run)
    print(param_obj.path_output_dir_run)
    print(output_dir_run)
    # Write the answer to param_obj.output_dir/param_obj.run_id/question_id.txt
    for dir in ["answers", "prompts", "sources"]:
        (Path(output_dir_run) / dir).mkdir(parents=True, exist_ok=True)
    filename_answer = f"{output_dir_run}/answers/{question_id}.txt"
    log(f"Writing the answer {response.response} to the output file {filename_answer}")
    with open(filename_answer, "w") as f:
        f.write(response.response)
    filename_prompt = f"{output_dir_run}/prompts/{question_id}.txt"
    log(
        f"Writing the prompt {param_obj.query_original_txt} to the output file {filename_prompt}"
    )
    with open(filename_prompt, "w") as f:
        f.write(param_obj.query_original_txt)
    filename_source = f"{output_dir_run}/sources/{question_id}.txt"
    log(f"Writing the sources {response.sources} to the output file {filename_source}")
    with open(filename_source, "w") as f:
        f.write(response.sources)
    # TODO: Implement
