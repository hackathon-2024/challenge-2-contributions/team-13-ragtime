# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import torch
import time
import bitsandbytes

from typing import Any, List, Mapping, Optional
from transformers import AutoModelForCausalLM, AutoTokenizer, TextStreamer
from langchain_core.callbacks.manager import CallbackManagerForLLMRun
from langchain_core.language_models.llms import LLM
from ragtime.logger.logger import log
from ragtime.utils import get_system_usage
from ragtime.params import params

from transformers import StoppingCriteria


class MyStoppingCriteria(StoppingCriteria):
    def __init__(self, target_sequence, prompt, tokenizer):
        self.target_sequence = target_sequence
        self.prompt = prompt
        self.tokenizer = tokenizer

    def __call__(self, input_ids, scores, **kwargs):
        # Get the generated text as a string
        generated_text = self.tokenizer.decode(input_ids[0])
        generated_text = generated_text.replace(self.prompt, "")
        # Check if the target sequence appears in the generated text
        if self.target_sequence in generated_text:
            return True  # Stop generation

        return False  # Continue generation

    def __len__(self):
        return 1

    def __iter__(self):
        yield self


class CustomLLM(LLM):
    model: str = "mistralai/Mistral-7B-v0.1"
    model_filepath: str = ""
    tokenizer: Optional[AutoTokenizer] = None
    model: Optional[AutoModelForCausalLM] = None
    param_obj: params.Params = None

    """bnb_config = BitsAndBytesConfig(
        load_in_4bit= True,
        bnb_4bit_quant_type= "fp4",
    )"""


    def __init__(self, model, param_obj, log_obj):
        super().__init__()
        self.model = model
        #self.model_filepath = f"{param_obj.hf_dir}/{self.model}"
        self.model_filepath = f"{self.model}"
        self.param_obj = param_obj
        log(f"Setting up {self.model_filepath}")

    def start_up(self):
        start_time = time.time()

        # Log initial GPU memory usage
        if torch.cuda.is_available():
            initial_mem_allocated = torch.cuda.memory_allocated()
            log(f"Initial GPU Memory allocated: {initial_mem_allocated} bytes")
            max_mem_allocated = torch.cuda.max_memory_allocated()
            log(f"Initial GPU Memory allocated: {max_mem_allocated} bytes")
            total_mem = torch.cuda.get_device_properties(0).total_memory
            total_mem_gb = total_mem / (1024**3)  # Convert bytes to GB
            log(f"Total GPU Memory: {total_mem_gb:.2f} GB")

        log("initializing tokenizer")
        tokenizer_start_time = time.time()
        self.tokenizer = AutoTokenizer.from_pretrained(self.model_filepath)
        log(
            f"Tokenizer initialized in {time.time() - tokenizer_start_time:.2f} seconds"
        )

        log(f"model_filepath: {self.model_filepath}")

        log("initializing model")
        model_start_time = time.time()
        self.model = AutoModelForCausalLM.from_pretrained(self.model_filepath,
                                                          load_in_4bit=True)
        
        log(f"Model loaded in {time.time() - model_start_time:.2f} seconds")

        if self.param_obj.device == "GPU":
            self.model = self.model#.to("cuda")
            # Log GPU memory after loading the model
            final_mem_allocated = torch.cuda.memory_allocated()
            log(f"Final GPU Memory allocated: {final_mem_allocated} bytes")
            log(
                f"Memory difference after loading model: {final_mem_allocated - initial_mem_allocated} bytes"
            )

        log(f"Total startup time: {time.time() - start_time:.2f} seconds")
        log("model initialized")

        return self

    @property
    def _llm_type(self) -> str:
        return "custom"

    @property
    def _identifying_params(self) -> Mapping[str, Any]:
        """Get the identifying parameters."""
        return {"model": self.model}

    def _call(
        self,
        prompt: str,
        stop: Optional[List[str]] = None,
        run_manager: Optional[CallbackManagerForLLMRun] = None,
        **kwargs: Any,
    ) -> str:
        if self.tokenizer is None:
            raise ValueError("Tokenizer is not initialized.")
        if self.model is None:
            raise ValueError("Model is not initialized.")
        if stop is not None:
            raise ValueError("stop kwargs are not permitted.")
        # log(f"Prompt: {prompt}")
        self.tokenizer.pad_token = self.tokenizer.eos_token
        max_new_tokens = kwargs.get("max_new_tokens", 100)
        log("Tokenizing...")
        inputs = self.tokenizer(prompt, return_tensors="pt").to("cuda")
        # For debugging, ensure device consistency
        device = next(self.model.parameters()).device
        log(f"Device: {device}")  # Check which device is being used
        log(
            f"Input tensors device: {list(inputs.values())[0].device}"
        )  # Ensure inputs are on the correct device

        # inputs = {name: tensor.to('cuda') for name, tensor in inputs.items()}
        log(f"Generating {max_new_tokens} tokens...")
        streamer = TextStreamer(self.tokenizer)
        get_system_usage()
        outputs = self.model.generate(
            **inputs,
            streamer=streamer,
            max_new_tokens=max_new_tokens,
            stopping_criteria=MyStoppingCriteria("\n\n\n", prompt, self.tokenizer),
        )
        # log(f"Generated {outputs}")
        generated_text = self.tokenizer.decode(outputs[0], skip_special_tokens=True)
        # log(f"Generated Text: {generated_text}")
        clean_text = generated_text.replace(prompt, "")
        #print(f"Generated Text: {clean_text}")
        return clean_text


if __name__ == "__main__":
    llm = CustomLLM()
    log(llm.invoke("This is a foobar thing"))
    log(llm)
