# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

from ragtime.utils import JSONable, QASample
from typing import List


from rouge_score import rouge_scorer
import sacrebleu
import numpy as np
from Levenshtein import distance as levenshtein_distance


def simple_f1_score(pred_answer, true_answer):
    pred_tokens = set(pred_answer.lower().split())
    true_tokens = set(true_answer.lower().split())
    if not pred_tokens or not true_tokens:
        return 0.0
    precision = len(pred_tokens & true_tokens) / len(pred_tokens)
    recall = len(pred_tokens & true_tokens) / len(true_tokens)
    if precision + recall == 0:
        return 0.0
    return 2 * (precision * recall) / (precision + recall)


def edit_distance_score(pred_answer, true_answer):
    return levenshtein_distance(pred_answer.lower(), true_answer.lower())


def compute_metrics_pair(predicted: QASample, truth: QASample):
    assert predicted.id == truth.id, "IDs must match for comparison."

    # Exact Match
    exact_match = any(
        pred_answer.lower().strip() == truth_answer.lower().strip()
        for pred_answer in predicted.answers
        for truth_answer in truth.answers
    )

    # BLEU Score
    bleu_score = sacrebleu.corpus_bleu(
        [" ".join(pred_answer.split()) for pred_answer in predicted.answers],
        [[" ".join(truth_answer.split()) for truth_answer in truth.answers]],
    ).score

    # ROUGE Score
    scorer = rouge_scorer.RougeScorer(["rouge1", "rouge2", "rougeL"], use_stemmer=True)
    rouge_scores = {key: [] for key in ["rouge1", "rouge2", "rougeL"]}
    f1s = []
    levenshteins = []

    for truth_ans in truth.answers:
        for pred_ans in predicted.answers:
            scores = scorer.score(truth_ans, pred_ans)
            f1s.append(simple_f1_score(pred_ans, truth_ans))
            levenshteins.append(edit_distance_score(pred_ans, truth_ans))
            for key in rouge_scores.keys():
                rouge_scores[key].append(scores[key].fmeasure)

    # Average the ROUGE scores if multiple scores were accumulated
    for key in rouge_scores:
        rouge_scores[key] = max(rouge_scores[key])
    f1 = max(f1s)
    levenshtein = min(levenshteins)

    true_src = set([s.strip().lower() for s in truth.sources])
    pred_src = set([s.strip().lower() for s in predicted.sources])
    src_recall = len(true_src & pred_src) / len(true_src)
    src_prec = len(true_src & pred_src) / len(pred_src)
    src_f1 = 2 * (src_recall * src_prec) / (src_recall + src_prec) if src_recall + src_prec > 0 else 0

    return {
        "exact_match": exact_match,
        "bleu": bleu_score,
        "rouge": rouge_scores,
        "simple_f1": f1,
        "levenshtein": levenshtein,
        "source_recall": src_recall,
        "source_precision": src_prec,
        "source_f1": src_f1,
    }


def compute_aggregate_metrics(predicted: List[QASample], truth: List[QASample]):
    metrics = []
    for pred, truth in zip(predicted, truth):
        metrics.append(compute_metrics_pair(pred, truth))

    result = {}
    for key in ['exact_match', 'bleu', 'simple_f1', 'levenshtein', 'source_recall', 'source_precision', 'source_f1']:
        result[f'avg_{key}'] = np.mean([m[key] for m in metrics])
        #result[f'max_{key}'] = np.max([m[key] for m in metrics])
        #result[f'min_{key}'] = np.min([m[key] for m in metrics])
        result[f'std_{key}'] = np.std([m[key] for m in metrics])
    for rouge in ['rouge1', 'rouge2', 'rougeL']:
        result[f'avg_{rouge}'] = np.mean([m['rouge'][rouge] for m in metrics])
        #result[f'max_{rouge}'] = np.max([m['rouge'][rouge] for m in metrics])
        #result[f'min_{rouge}'] = np.min([m['rouge'][rouge] for m in metrics])
        result[f'std_{rouge}'] = np.std([m['rouge'][rouge] for m in metrics])

    return result


class Metrics(JSONable):
    """
    A class to store the metrics.
    """

    def __init__(self, payload):
        self.metrics = payload


def calculate(sample: QASample, response, param_obj, log_obj):
    """
    Calculate the metrics.

    Args:
        refined_results (RefinedResults): The refined results.
        param_obj (Params): The Params object.
        log_obj (Logger): The Logger object.

    Returns:
        None
    """

    responsetext = response.response

    # TODO: source eval?
    response_sample = QASample(sample.id, sample.question, [responsetext], ["model"])

    metrics = Metrics(compute_metrics_pair(response_sample, sample))
    return metrics
