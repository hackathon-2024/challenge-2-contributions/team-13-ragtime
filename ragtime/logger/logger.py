# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import json
import os
import time
import sys
from pathlib import Path

project_root = Path(__file__).absolute().parents[2]

log_folder = project_root / "log"


def log(*args, **kwargs):
    print("▶️ ", *args, **kwargs, file=sys.stderr)


def generate_run_id():
    """
    Generate a timestamp as the run ID.

    Returns:
        str: The generated run ID.
    """
    return str(int(time.time()))


class RunLogger:
    def __init__(self, run_id=None, log_file="default.jsonl"):
        if run_id is None:
            run_id = generate_run_id()
        self.run_id = run_id
        self.log_file = log_file
        self.log_folder = log_folder
        self.log_folder.mkdir(parents=True, exist_ok=True)
        self.log_folder = str(self.log_folder)

    def log_run(self, data):
        """
        Append data to a log file.

        Args:
            data (Any): The data to be appended to the log.

        Returns:
            None
        """

        # Clean some data
        def remove_duplicate_data(obj):
            bad_categories = ["embeddings", "docs", "model", "tokenizer"]
            if isinstance(obj, dict):
                return {
                    k: remove_duplicate_data(v)
                    for k, v in obj.items()
                    if k not in bad_categories
                }
            elif isinstance(obj, list):
                return [remove_duplicate_data(item) for item in obj]
            else:
                return obj

        cleaned_data = remove_duplicate_data(data)

        json_file = os.path.join(self.log_folder, self.log_file)
        data = {**cleaned_data, "run_id": self.run_id}

        # Write the updated log to the file
        with open(json_file, "a") as f:
            try:
                json.dump(data, f)
                f.write("\n")
            except Exception as e:
                log(f"❌❌❌ Error writing to log file: {e}")
                log(f"❌❌❌ {data}")
