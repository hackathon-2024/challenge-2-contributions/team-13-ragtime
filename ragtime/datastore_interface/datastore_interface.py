# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

from ragtime.utils import JSONable
from ragtime.logger.logger import log
from langchain_community.vectorstores import FAISS
from langchain.retrievers import ParentDocumentRetriever
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.storage import LocalFileStore


class RawResults(JSONable):
    """
    A class to store the raw results from the datastore.
    """

    def __init__(self):
        self.snippets = []
        self.docs = []


def intialize_retriever(vectorstore, param_obj, log_obj):
    child_splitter = RecursiveCharacterTextSplitter(chunk_size=param_obj.seq_chunk_size)
    log(child_splitter, param_obj.seq_chunk_size)
    log("Initializing the parent document retriever...")
    # The storage layer for the parent documents
    retriever = ParentDocumentRetriever(
        vectorstore=vectorstore,
        docstore=LocalFileStore(f"{param_obj.db}.docstore"),
        # TODO Storage
        child_splitter=child_splitter,
    )
    return retriever


def retrieve(query, param_obj, log_obj):
    """
    Interface with the datastore.

    Args:
        query (Query): The query to be analysed.
        param_obj (Params): The Params object.
        log_obj (Logger): The Logger object.

    Returns:
        None
    """
    from ast import literal_eval
    import json

    raw_results = RawResults()
    vectorstore = FAISS.load_local(param_obj.db, param_obj.embedding_model_obj)
    retriever = intialize_retriever(vectorstore, param_obj, log_obj)
    log(f"searching for {query.query_txt_for_embed}")
    sub_docs = vectorstore.similarity_search(query.query_txt_for_embed, k=20)
    retrieved_docs = retriever.get_relevant_documents(query.query_txt_for_embed)
    for doc in sub_docs:
        doc.metadata["source"] = str(doc.metadata["source"])
    for doc in retrieved_docs:
        doc.metadata["source"] = str(doc.metadata["source"])
    filename = f"{log_obj.log_folder}/retrieval/{log_obj.run_id}_docs.json"
    with open(filename, "w") as f:
        decoded_docs = []
        for doc in retrieved_docs:
            print(type(doc))
            print(doc)
            decoded_doc = literal_eval(doc.decode("utf-8"))
            decoded_docs.append(decoded_doc)
        f.write(json.dumps(decoded_docs))
    raw_results.retrieval_docs_filename = filename
    log(f"Most relevant snippets {sub_docs}")
    # TODO: Implementing a more manual search, using FAISS directly
    # TODO: Implement retrieval (means you need to redo the
    # embedding process to store it on file)
    raw_results.snippets = sub_docs
    raw_results.docs = retrieved_docs

    return raw_results

    # Step 2. Acquire the Database

    # Step 3. Search and select

    # v0: Pass to Datastore Interface
