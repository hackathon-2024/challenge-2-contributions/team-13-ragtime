# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import argparse
import os
from pathlib import Path
from ragtime.logger import logger
from ragtime.logger.logger import log
from ragtime.params import params
from ragtime.query_analyser import query_analyser
from ragtime.datastore_interface import datastore_interface
from ragtime.refiner import refiner
from ragtime.writer import writer
from ragtime.metrics import metrics
from ragtime.writer.llm import CustomLLM
from ragtime.query_analyser import localembeds

from ragtime.utils import QASample

from ragtime.utils import load_qa_samples

# TODO: Implement the pipeline.


def run(
    sample: QASample,
    param_obj,
    log_obj: logger.RunLogger,
    llm_obj, 
    embedder_obj
):
    """
    Run the pipeline.

    Args:
        input_text (str): The input text.
        question_id (str): The ID of the question.
        param_obj (object): The parameter object.
        log_obj (object): The log object.
        llm_obj (object): The LLM object.
        embedder_obj (object): The embedder object.

    Returns:
        None
    """
    question_id = sample.id
    input_text = sample.question
    param_obj.question_id = question_id
    path_output_dir = Path(param_obj.output_dir)
    emb_mod = param_obj.embedding_model.replace("/", "-")
    inf_mod = param_obj.inference_model.replace("/", "-")
    param_obj.path_output_dir_run = (
        path_output_dir
        # / f"{param_obj.run_id}_{emb_mod}_{param_obj.max_seq_length}_{inf_mod}"
    )
    param_obj.path_output_dir_run.mkdir(parents=True, exist_ok=True)
    param_obj.path_output_dir_run = str(param_obj.path_output_dir_run)
    print(param_obj.path_output_dir_run)
    log("Input:", input_text)
    log("Output directory:", param_obj.output_dir)
    log("Run ID:", param_obj.run_id)
    log("Device:", param_obj.device)
    log("Current question", param_obj.question_id)

    query_obj = query_analyser.analyse(input_text, embedder_obj, param_obj, log_obj, llm_obj)
    log("🔥 Query:", query_obj.query_original_txt)
    raw_results = datastore_interface.retrieve(query_obj, param_obj, log_obj)
    log("🔥 Raw results processed")
    refined_results = refiner.refine(raw_results, param_obj, log_obj, llm_obj)
    log("🔥 Refined results processed")
    response = writer.write(refined_results, llm_obj, param_obj, log_obj)
    log("🔥 Response:", response.response)
    writer.outwrite(response, question_id, param_obj, log_obj, llm_obj)
    log("🔥 Output dir:", param_obj.output_dir)

    # metrics_obj = metrics.calculate(sample, response, param_obj, log_obj)

    # log("🔥📏 Metrics:", metrics_obj.toJSON())

    log(f"🔥 Logging now to {log_obj.log_folder}")

    query_json = query_obj.toJSON()
    raw_results_json = raw_results.toJSON()
    refined_results_json = refined_results.toJSON()
    response_json = response.toJSON()
    # metrics_json = metrics_obj.toJSON()
    try:
        del param_obj.embedding_model_obj
        params_json = param_obj.toJSON()
        log_obj.log_run(
            {
                "question_id": question_id,
                "query": query_json,
                "raw_results": raw_results_json,
                "refined_results": refined_results_json,
                "response": response_json,
                # "metrics": metrics_json,
                "params": params_json,
            }
        )
    except Exception as e:
        log(f"🔥❌❌❌❌ Error logging: {e}")
        with open("error.log", "a") as f:
            f.write(str(param_obj.__dict__))
            f.write('\n')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--inputdir",
        help="input Directory",
        default=os.path.join(os.path.dirname(__file__), "../data/train/"),
    )
    parser.add_argument(
        "--output",
        help="output directory",
        default=os.path.join(os.path.dirname(__file__), "../data/out"),
    )
    parser.add_argument(
        "--device",
        help="device to run on (LOCAL, CPU, or GPU)",
        default="LOCAL",
        choices=["LOCAL", "CPU", "GPU"],
    )
    parser.add_argument(
        "--db",
        help="(vector) database to use",
        default="data/reference.db",
    )
    parser.add_argument(
        "--seq_chunk_size",
        help="Size of Sequence Chunks",
        type=int,
        default=350,
    )
    parser.add_argument(
        "--seq_token_max_size",
        help="Max size of token sequences",
        type=int,
        default=128,
    )
    parser.add_argument(
        "--embedding_model",
        help="Which Embedding Model to use?",
        type=str,
        default="intfloat/multilingual-e5-large",
    )
    parser.add_argument(
        "--embedding_dimensions",
        help="how many dimensions in the model",
        type=int,
        default=1024,
    )
    parser.add_argument(
        "--inference_model",
        help="what inference model to use",
        type=str,
        default="mistralai/Mistral-7B-v0.1",
    )

    args = parser.parse_args()

    input_dir = args.inputdir
    output_dir = args.output
    device = args.device
    db = args.db
    seq_chunk_size = args.seq_chunk_size
    seq_token_max_size = args.seq_token_max_size
    embedding_model = args.embedding_model
    embedding_dimensions = args.embedding_dimensions
    inference_model = args.inference_model
    print(embedding_model)

    log_obj = logger.RunLogger()
    param_obj = params.Params()

    param_obj.run_id = log_obj.run_id

    # Yes, it's doubling, but whatever
    param_obj.output_dir = output_dir
    param_obj.device = device
    param_obj.db = db
    param_obj.embedding_model = embedding_model
    print(param_obj.embedding_model)
    param_obj.embedding_dimensions = embedding_dimensions

    # Default values
    param_obj.max_seq_length = seq_token_max_size
    param_obj.seq_chunk_size = seq_chunk_size
    param_obj.inference_model = inference_model

    llm_obj = CustomLLM(inference_model, param_obj, logger.RunLogger())
    llm_obj = llm_obj.start_up()

    embedder_obj = localembeds.LocalEmbeddings(param_obj=param_obj)

    # Get the CSV
    # For each question in the CSV
    # Do run

    #dataset = load_qa_samples(Path(input_dir, is_file=True))
    import pandas as pd
    questions = pd.read_csv(input_dir, sep=";")

    for id, question in questions.itertuples(index=False):
        sample = QASample(id, question, [], [])
        run(sample, param_obj, log_obj, llm_obj, embedder_obj)
