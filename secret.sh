#!/bin/bash

# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

PASSWORD="your_password_here"
SSH_KEY_PATH="~/Downloads/id_ed25519"
PROXY_JUMP="hackathon@infra2.peren.fr:1502"
REPO="discovery"
LOCAL_PATH="/Users/l/code/HKBR/$REPO"
REMOTE_PATH="uxh65dw@jean-zay.idris.fr:/linkhome/rech/genner01/uxh65dw/code/"
