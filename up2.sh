#!/usr/bin/bash

# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

# upload assuming SSH host alias is defined with keys and proxy jump

# directory where this script is located
REPO="$(dirname $(realpath $BASH_SOURCE))"

# read vars from var files
HOST_ALIAS="$(cat $REPO/vars/host_alias.var)"
REMOTE_CODE_DIR="$(cat $REPO/vars/remote_code_dir.var)"

rsync -a -v \
  -r "$REPO/" \
  $HOST_ALIAS:$REMOTE_CODE_DIR \
  --exclude ".git" \
  --exclude ".venv" \
  --exclude "venv" \
  --exclude "log" \

