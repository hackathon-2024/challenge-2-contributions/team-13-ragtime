# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

# directory where this script is located
REPODIR=/home/lemebfr
REPO=2024-peren-hackathon-rag-time

# read vars from var files
HOST_ALIAS=jean-zay
REMOTE_CODE_DIR="$(cat $REPODIR/$REPO/vars/remote_code_dir.var)"

rsync -a -v \
  -r "${HOST_ALIAS}:$REMOTE_CODE_DIR/$REPO" "${REPODIR}/" \
  --exclude ".git" \
  --exclude ".venv" \
  --exclude "data/embeddings"
