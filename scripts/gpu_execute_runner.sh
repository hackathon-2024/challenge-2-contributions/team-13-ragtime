# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

# This script is used to execute the 'embedhtml' module with customizable arguments.
# It activates the virtual environment and parses command line arguments to override default values.
# The script then calls the 'embedhtml' module with the provided arguments.

# Usage:
#   - Pass the desired arguments using the corresponding flags:
#       --device: Specify the device to use (default: GPU)
#       --inputdir: Specify the input directory (default: data/snapshots)
#       --output: Specify the output file (default: data/exp3.db)
#       --starter_id: Specify the starter ID (default: 0)
#       --ender_id: Specify the ender ID (default: -1)
#       --seq_chunk_size: Specify the sequence chunk size (default: 350)
#       --seq_token_max_size: Specify the maximum sequence token size (default: 128)
#       --file_format: Specify the file format (default: HTML)
#   - Any unknown option will result in an error message and the script will exit.

source .venv/bin/activate

# This is so arguments can be passed and override the defaults
while [[ $# -gt 0 ]]; do
    case "$1" in
        --input)
            input="$2"
            shift 2
            ;;
        --device)
            device="$2"
            shift 2
            ;;
        --inputdir)
            inputdir="$2"
            shift 2
            ;;
        --output)
            output="$2"
            shift 2
            ;;
        --db)
            db="$2"
            shift 2
            ;;
        --starter_id)
            starter_id="$2"
            shift 2
            ;;
        --ender_id)
            ender_id="$2"
            shift 2
            ;;
        --seq_chunk_size)
            seq_chunk_size="$2"
            shift 2
            ;;
        --seq_token_max_size)
            seq_token_max_size="$2"
            shift 2
            ;;
        --file_format)
            file_format="$2"
            shift 2
            ;;
        --embedding_model)
            embedding_model="$2"
            shift 2
            ;;
        --embedding_dimensions)
            embedding_dimensions="$2"
            shift 2
            ;;
        --inference_model)
            inference_model="$2"
            shift 2
            ;;
        *)
            echo "Unknown option: $1"
            exit 1
            ;;
    esac
done

# python -m scripts.embedhtml \
#     --device "${device:-GPU}" \
#     --inputdir "${inputdir:-data/snapshots}" \
#     --output "${output:-data/exp3.db}" \
#     --starter_id "${starter_id:-0}" \
#     --ender_id "${ender_id:--1}" \
#     --seq_chunk_size "${seq_chunk_size:-350}" \
#     --seq_token_max_size "${seq_token_max_size:-128}" \
#     --file_format "${file_format:-HTML}"

default_embedding_model="intfloat/multilingual-e5-large"
default_inference_model="mistralai/Mistral-7B-v0.1"

python -m ragtime.runner \
    --device "${device:-GPU}" \
    --db  "${db:-data/ref_md0.db}" \
    --seq_chunk_size "${seq_chunk_size:-1400}" \
    --seq_token_max_size "${seq_token_max_size:-512}" \
    --embedding_model "${embedding_model:-${default_embedding_model}}" \
    --embedding_dimensions "${embedding_dimensions:-1024}" \
    --inference_model "${inference_model:-${default_inference_model}}" \
    --inputdir "${inputdir:-"data/dataset/train/input"}" \
    --output "${output:-"tmp/run"}"
