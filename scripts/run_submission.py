# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import argparse
import os
from pathlib import Path
import pandas as pd
from ragtime.logger import logger
from ragtime.logger.logger import log
from ragtime.params import params
from ragtime.query_analyser import query_analyser
from ragtime.datastore_interface import datastore_interface
from ragtime.refiner import refiner
from ragtime.writer import writer
from ragtime.metrics import metrics
from ragtime.writer.llm import CustomLLM
from ragtime.query_analyser import localembeds



def run(question_id, question: str, param_obj, log_obj: logger.RunLogger, llm_obj, embedder_obj):
    input_text = question

    param_obj.question_id = question_id
    path_output_dir = Path(param_obj.output_dir)
    param_obj.path_output_dir_run = path_output_dir / f"{param_obj.run_id}_{param_obj.embedding_model}_{param_obj.max_seq_length}_{param_obj.inference_model}"
    param_obj.path_output_dir_run.mkdir(parents=True, exist_ok=True)
    param_obj.path_output_dir_run = str(param_obj.path_output_dir_run)
    
    log("Input:", input_text)
    log("Output directory:", param_obj.output_dir)
    log("Run ID:", param_obj.run_id)
    log("Device:", param_obj.device)
    log("Current question", param_obj.question_id)

    query_obj = query_analyser.analyse(input_text, embedder_obj, param_obj, log_obj, llm_obj)
    log("🔥❓ Query:", query_obj.__dict__)
    raw_results = datastore_interface.retrieve(query_obj, param_obj, log_obj)
    log("🔥🥩 Raw results processed")
    refined_results = refiner.refine(raw_results, param_obj, log_obj, llm_obj)
    log("🔥🤏 Refined results processed")
    response = writer.write(refined_results, llm_obj, param_obj, log_obj)
    log("🔥❗ Response:", response.response)

    writer.outwrite(
        response,
        question_id,
        param_obj,
        log_obj,
        llm_obj
    )

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--infile",
        help="CSV file"
    )
    parser.add_argument(
        "--outdir",
        help="output directory"
    ) 
    parser.add_argument(
        "--device",
        help="device to run on (LOCAL, CPU, or GPU)",
        default="LOCAL",
        choices=["LOCAL", "CPU", "GPU"],
    )
    parser.add_argument(
        "--db",
        help="(vector) database to use",
        default="data/reference.db",
    )
    parser.add_argument(
        "--seq_chunk_size",
        help="Size of Sequence Chunks",
        type=int,
        default=350,
    )
    parser.add_argument(
        "--seq_token_max_size",
        help="Max size of token sequences",
        type=int,
        default=128,
    )
    parser.add_argument(
        "--embedding_model",
        help="Which Embedding Model to use?",
        type=str,
        default="intfloat/multilingual-e5-large",
    )
    parser.add_argument(
        "--embedding_dimensions",
        help="how many dimensions in the model",
        type=int,
        default=1024,
    )
    parser.add_argument(
        "--inference_model",
        help="what inference model to use",
        type=str,
        default="mistralai/Mistral-7B-v0.1",
    )

    args = parser.parse_args()

    output_dir = args.outdir
    device = args.device
    db = args.db
    seq_chunk_size = args.seq_chunk_size
    seq_token_max_size = args.seq_token_max_size
    embedding_model = args.embedding_model
    embedding_dimensions = args.embedding_dimensions
    inference_model = args.inference_model

    log_obj = logger.RunLogger()
    param_obj = params.Params()

    param_obj.run_id = log_obj.run_id

    # Yes, it's doubling, but whatever
    param_obj.output_dir = output_dir
    param_obj.device = device
    param_obj.db = db
    param_obj.embedding_model = embedding_model
    param_obj.embedding_dimensions = embedding_dimensions

    # Default values
    param_obj.max_seq_length = seq_token_max_size
    param_obj.seq_chunk_size = seq_chunk_size
    param_obj.inference_model = inference_model

    param_obj.path_output_dir_run = Path(param_obj.output_dir)
    log("▶️❗❗ Output directory:", param_obj.path_output_dir_run)

    llm_obj = CustomLLM(inference_model, param_obj, logger.RunLogger())
    llm_obj = llm_obj.start_up()

    embedder_obj = localembeds.LocalEmbeddings(param_obj=param_obj)

    # Get the CSV
    # For each question in the CSV
    # Do run

    with open(args.infile, "r") as f:
        questions = f.readlines()

    for question in questions:
        log("Question:", question)
        qid, question = question.strip().split(";")
        run(
            qid,
            question,
            param_obj,
            log_obj,
            llm_obj,
            embedder_obj,
        )
