# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

srun -A jdh@v100 --qos=qos_gpu-dev --gres=gpu:4 --cpus-per-task=10 \
    --hint=nomultithread --time=00:02:00 --pty -- \
    bash scripts/gpu_execute.sh --output "data/ref0.db" --ender_id 50 \
    --device "GPU" --seq_chunk_size 1400 --seq_token_max_size 512