#!/bin/bash

# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

set -x

HERE=$(dirname $(realpath $BASH_SOURCE))

export PYTHONPATH=$(dirname $HERE)

embedding_model="intfloat/multilingual-e5-large"
embedding_dimensions=1024
seq_chunk_size=1400
seq_token_max_size=512

exec python -m scripts.run_custom_question \
    --device "GPU" \
--db "data/embeds/${embedding_model//\//-}_${seq_token_max_size}/merged.faiss" \
    --embedding_model "$embedding_model" \
    --embedding_dimensions "$embedding_dimensions" \
    --seq_chunk_size "$seq_chunk_size" \
    --seq_token_max_size "$seq_token_max_size" \
    --inference_model "mistralai/Mistral-7B-v0.1"