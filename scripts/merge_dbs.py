# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import os
from langchain_community.vectorstores import FAISS
from ragtime.query_analyser.localembeds import LocalEmbeddings
from ragtime.params import params


def merge_dbs(
    folder_path,
    output_path,
    checkpoint,
    embedding_dimensions,
    seq_chunk_size,
    seq_token_max_size,
):
    param_obj = params.Params()
    param_obj.max_seq_length = seq_token_max_size
    localembeds = LocalEmbeddings(
        checkpoint=checkpoint,
        device="cpu",
        param_obj=param_obj,
    )

    # Get all .faiss files recursively
    faiss_dirs = []
    for root, dirs, files in os.walk(folder_path):
        for dir in dirs:
            if dir.endswith("faiss") and dir not in ["merged.faiss", "index.faiss"]:
                faiss_dirs.append(os.path.join(root, dir))
    # for root, dirs, files in os.walk(folder_path):
    #     for file in files:
    #         if file.endswith(".faiss") and file not in ["merged.faiss", "index.faiss"]:
    #             faiss_files.append(os.path.join(root, file))

    # Merge all databases
    if len(faiss_dirs) > 0:
        # Load the first database
        print(faiss_dirs)
        first_db = FAISS.load_local(faiss_dirs[0], localembeds)

        # Merge the remaining databases
        for i in range(1, len(faiss_dirs)):
            db = FAISS.load_local(faiss_dirs[i], localembeds)
            print(f"Merging {faiss_dirs[i]}...")
            first_db.merge_from(db)

        # Save the merged database
        FAISS.save_local(first_db, output_path)
        print("Merged databases saved to:", output_path)
    else:
        print("No .faiss files found in the specified folder.")


# Usage example
data_folder = "data/embeds"

RUNS = (
    ("intfloat/multilingual-e5-large", 1024, 350, 128),
    ("intfloat/multilingual-e5-large", 1024, 1400, 512),
    ("Salesforce/SFR-Embedding-Mistral", 1024, 1400, 512),
    ("Salesforce/SFR-Embedding-Mistral", 1024, 350, 128),
    ("intfloat/e5-mistral-7b-instruct", 1024, 350, 128),
    ("intfloat/e5-mistral-7b-instruct", 1024, 1400, 512),
    ("Salesforce/codet5p-110m-embedding", 1024, 1400, 512),
    ("Salesforce/codet5p-110m-embedding", 1024, 350, 128),
)

for checkpoint, embedding_dimensions, seq_chunk_size, seq_token_max_size in RUNS:
    folder = f"{checkpoint}_{seq_token_max_size}".replace("/", "-")
    folder_path = os.path.join(data_folder, folder)
    output_path = os.path.join(data_folder, folder, "merged.faiss")
    merge_dbs(
        folder_path,
        output_path,
        checkpoint,
        embedding_dimensions,
        seq_chunk_size,
        seq_token_max_size,
    )
