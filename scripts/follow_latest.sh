#!/bin/bash

# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

## Follows the latest sbatch log file


LOGDIR="logs/sbatch"

LATEST=$(ls -t $LOGDIR | head -n 1)
JOBID=$(echo $LATEST | rev | cut -d'-' -f 1 | rev | cut -d'.' -f 1)

echo "Following latest job: $JOBID"
exec tail $@ -f $LOGDIR/*-$JOBID.*