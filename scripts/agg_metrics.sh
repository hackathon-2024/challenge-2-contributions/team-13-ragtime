#!/bin/bash

# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

REPO="$(dirname $(dirname $(realpath ${BASH_SOURCE[0]})))"
HOST_ALIAS="$(cat $REPO/vars/host_alias.var)"
REMOTE_CODE_DIR="$(cat $REPO/vars/remote_code_dir.var)"

# fetch logs
rsync --info=progress2 -r $HOST_ALIAS:$REMOTE_CODE_DIR/log .

# extract metrics
jq  'select(.metrics != "" ) | {run_id: .run_id, question_id: .question_id, embedding_model: .params.embedding_model, embedding_dimensions: .params.embedding_dimensions, inference_model: .params.inference_model, metrics: .metrics.metrics}' log/default.jsonl

