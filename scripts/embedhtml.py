# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import os
import time
import argparse
from langchain.retrievers import ParentDocumentRetriever
from langchain.storage import LocalFileStore

# Yes, it's a hack, see langchain #9345
from langchain.storage._lc_store import create_kv_docstore
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain_community.document_loaders import TextLoader
from langchain_community.docstore.in_memory import InMemoryDocstore
from langchain_community.vectorstores import FAISS
from langchain_openai import OpenAIEmbeddings
from faiss import IndexFlatL2
import getpass
import glob
from ragtime.params import params
from ragtime.query_analyser.localembeds import LocalEmbeddings
from ragtime.logger.logger import log


def retrieve_url(doc):
    """
    Retrieve the URL that is embedded in the first line of the
    Markdown document as follows:
    # Resource URL: https://developers.facebook.com/docs/ad-targeting-dataset/get-started

    Args:
        doc (Document): The document object.

    Returns:
        doc.metadata["url"] (str): The URL.
    """

    lines = doc.page_content.split("\n")
    for line in lines:
        if line.startswith("# Resource URL:"):
            return line.split(" ")[3]
    return None


def main(args, param_obj):
    """
    Main function that loads HTML files, initializes the vector
    store, and adds documents to the parent document retriever.

    Args:
        args (object): Command line arguments.
        param_obj (object): Parameter object.

    Returns:
        None
    """
    if param_obj.device == "LOCAL":
        os.environ["OPENAI_API_KEY"] = getpass.getpass("OpenAI API Key:")
        embeddings = OpenAIEmbeddings()
    else:
        os.environ[
            "SENTENCE_TRANSFORMERS_HOME"
        ] = "/gpfswork/rech/jdh/uxh65dw/tmp/.cache/huggingface/hub"
        model = args.embedding_model
        checkpoint = f"{model}"
        device = "cuda" if param_obj.device == "GPU" else "cpu"
        print(f"Using {device} for inference on {checkpoint}.")
        param_obj.dimensions = args.embedding_dimensions
        param_obj.max_seq_length = args.seq_token_max_size
        param_obj.seq_chunk_size = args.seq_chunk_size
        embeddings = LocalEmbeddings(
            checkpoint=checkpoint,
            device=device,
            param_obj=param_obj,
        )
        param_obj.file_format = args.file_format

    print(param_obj.__dict__)
    # Load the files
    log("Loading the files...")
    default_dir = (
        "data/snapshots" if param_obj.file_format == "HTML" else "data/versions"
    )
    file_format = "html" if param_obj.file_format == "HTML" else "md"
    directory = args.inputdir if args.inputdir else default_dir
    files = glob.glob(directory + f"/**/*.{file_format}", recursive=True)
    loaders = list(map(lambda file: TextLoader(file), files))
    docs = []
    for loader in loaders:
        doc = loader.load()
        doc[0].metadata["url"] = retrieve_url(doc[0])
        docs.extend(doc)
    print(f"Loaded {len(docs)} documents.")

    # Intialize the Vector Store
    log("Initializing the vector store...")
    dimensions: int = (
        len(embeddings.embed_query("dummy"))
        if param_obj.device == "LOCAL"
        else param_obj.dimensions
    )
    store = InMemoryDocstore()
    vectorstore = FAISS(
        embedding_function=embeddings,
        index=IndexFlatL2(dimensions),
        docstore=store,
        index_to_docstore_id={},
    )
    log(f"Initialized the vector store with FAISS and {dimensions} dimensions.")
    # This text splitter is used to create the child documents
    child_splitter = RecursiveCharacterTextSplitter(chunk_size=param_obj.seq_chunk_size)

    log("Initializing the parent document retriever...")
    # The storage layer for the parent documents
    retriever = ParentDocumentRetriever(
        vectorstore=vectorstore,
        docstore=create_kv_docstore(LocalFileStore(f"{args.output}.docstore")),
        child_splitter=child_splitter,
    )
    # TODO: Push that code to a util function to be shared with
    # the DataStore Interface class for lisibility

    log("Adding documents to the parent document retriever...")
    # Your code here, for example:
    time.sleep(1)  # Simulating a delay
    i = 0
    start_time = time.time()
    log(f"Starting at document #{args.starter_id}")
    log(f"Ending at document #{args.ender_id}")
    for doc in docs[args.starter_id : args.ender_id]:
        i += 1
        log(f"Adding document #{i}")
        log(f"Source: {doc.metadata['source']}")
        retriever.add_documents(docs[i - 1 : i], ids=None)
    end_time = time.time()

    log(f"Execution time: {end_time - start_time} seconds")

    vectorstore.save_local(args.output)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--device",
        help="device to run on (LOCAL, CPU, or GPU)",
        default="LOCAL",
        choices=["LOCAL", "CPU", "GPU"],
    )
    parser.add_argument(
        "--inputdir",
        help="input directory",
        default="data/snapshots",
    )
    parser.add_argument(
        "--output",
        help="input directory",
        default="data/raw.db",
    )
    parser.add_argument(
        "--starter_id",
        help="first document to process",
        type=int,
        default=0,
    )
    parser.add_argument(
        "--ender_id",
        help="last document to process",
        type=int,
        default=-1,
    )
    parser.add_argument(
        "--seq_chunk_size",
        help="Size of Sequence Chunks",
        type=int,
        default=350,
    )
    parser.add_argument(
        "--seq_token_max_size",
        help="Max size of token sequences",
        type=int,
        default=128,
    )
    parser.add_argument(
        "--file_format",
        help="HTML or MD?",
        type=str,
        choices=["HTML", "MD"],
        default="HTML",
    )
    parser.add_argument(
        "--log",
        help="log file",
        default="default.jsonl",
    )
    parser.add_argument(
        "--embedding_model",
        help="Which Embedding Model to use?",
        type=str,
        default="intfloat/multilingual-e5-large",
    )
    parser.add_argument(
        "--embedding_dimensions",
        help="how many dimensions in the model",
        type=int,
        default=1024,
    )

    args = parser.parse_args()
    param_obj = params.Params()
    param_obj.device = args.device
    log(args, param_obj)
    main(args, param_obj)
