#!/bin/bash

# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

#SBATCH --output=logs/sbatch/test-%j.out
#SBATCH --error=logs/sbatch/test-%j.out
##SBATCH -A jdh@cpu
#SBATCH -A jdh@v100
#SBATCH --gres=gpu:1
#SBATCH --qos=qos_gpu-dev
#SBATCH --job-name=my_job
#SBATCH --nodes=8
#SBATCH --ntasks=64
#SBATCH --time=01:00:00 # or whatever time limit you think is reasonable

log() { echo "🦇 $@" >&2; }

RUNS=(
intfloat/multilingual-e5-large,1024,350,128
intfloat/multilingual-e5-large,1024,1400,512
Salesforce/SFR-Embedding-Mistral,4096,350,128
Salesforce/SFR-Embedding-Mistral,4096,1400,512
intfloat/e5-mistral-7b-instruct,4096,350,128
intfloat/e5-mistral-7b-instruct,4096,1400,512
Salesforce/codet5p-110m-embedding,1024,350,128
Salesforce/codet5p-110m-embedding,1024,1400,512
)

log "Starting job $SLURM_JOB_ID on $SLURM_JOB_NUM_NODES nodes with a total of $SLURM_NTASKS tasks..."

for run in "${RUNS[@]}"; do

IFS=',' read -r -a run_args <<< "$run"
log "Running with args: ${run_args[*]} on ${BUCKET_INDEX}"

    for BUCKET_INDEX in $(seq 0 7); do

    srun \
        --ntasks=1 \
        --nodes=1 \
        --exclusive \
        python3 \
            scripts/embedhtml_platforms.py \
            --buckets-file "data/buckets.json" \
            --bucket-index ${BUCKET_INDEX} \
            --embedding_model ${run_args[0]} \
            --embedding_dimensions ${run_args[1]} \
            --seq_chunk_size ${run_args[2]} \
            --seq_token_max_size ${run_args[3]} \
            --device "GPU" \
            --file_format "MD" \
            --inputdir "data/versions" \
            --outputdir "data/multimodel.db" \
        & 

    done
done

wait
