# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

from transformers import AutoModelForCausalLM, AutoTokenizer
from ragtime.params import params

param_obj = params.Params()

model_id = "mistralai/Mistral-7B-v0.1"
filepath = f"{param_obj.hf_dir}/{model_id}"
print(filepath)

print("intializing tokenizer")
tokenizer = AutoTokenizer.from_pretrained(filepath)
print("intializing model")
model = AutoModelForCausalLM.from_pretrained(filepath)

print("let's tokenize")
text = "Hello my name is"
inputs = tokenizer(text, return_tensors="pt")
print("let's generate")
outputs = model.generate(**inputs, max_new_tokens=20)
print(tokenizer.decode(outputs[0], skip_special_tokens=True))
