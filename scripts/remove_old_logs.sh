#!/bin/bash

# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

target_dir=.  # Replace with your actual directory path
threshold=$1

if [[ -z $threshold ]]; then
  echo "Threshold not specified. Exiting..."
  exit 1
fi

for folder in "$target_dir"/*; do
  if [[ -d "$folder" ]]; then  # Ensure it's a directory
    folder_name=$(basename "$folder")
    timestamp=${folder_name%%_*}  # Extract timestamp before the first underscore
    
    if [[ $timestamp =~ ^[0-9]+$ ]] && (( timestamp < threshold )); then  # Ensure timestamp is numeric and less than threshold
      echo "Removing $folder..."
      rm -rf "$folder"  # Dangerous: permanently deletes the folder
    fi
  fi
done

echo "Cleanup complete."
