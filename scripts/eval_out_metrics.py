# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import os
import json
import argparse
import sys
from pathlib import Path

def log(*args, **kwargs):
    print("▶️ ", *args, **kwargs, file=sys.stderr)

here = Path(__file__).absolute().parents[1]
src_dir = str(here)

if src_dir not in sys.path:
    sys.path.append(src_dir)

from ragtime.metrics.metrics import compute_metrics_pair, compute_aggregate_metrics
from ragtime.utils import QASample

# Create the argument parser
parser = argparse.ArgumentParser()
parser.add_argument('--base_dir', type=str, help='Base directory path')
args = parser.parse_args()

# Get the base directory from the command line argument
base_dir = Path(args.base_dir)

model_dirs = []
for root, dirs, files in os.walk(base_dir):
    if 'answers' in dirs:
        model_dirs.append(root)

def process_model_dir(base_dir):
    # Define the prompts, answers, and sources directories
    prompts_dir = os.path.join(base_dir, 'prompts')
    answers_dir = os.path.join(base_dir, 'answers')
    sources_dir = os.path.join(base_dir, 'sources')

    # Define the expected sources directory
    expected_sources_dir = 'data/dataset/train/output/sources'
    expected_answers_dir = 'data/dataset/train/output/answers'

    # Initialize an empty list to store the data
    data = []

    for i in range(10):  # Adjust the range if there are more files
        # Constructing file paths
        prompt_file = os.path.join(prompts_dir, f'{i}.txt')
        answer_file = os.path.join(answers_dir, f'{i}.txt')
        source_file = os.path.join(sources_dir, f'{i}.txt')
        expected_source_file = os.path.join(expected_sources_dir, f'{str(i).zfill(3)}.txt')
        expected_answer_path = os.path.join(expected_answers_dir, f'{str(i).zfill(3)}')

        # Reading file contents
        try:
            with open(prompt_file, 'r', encoding='utf-8') as pf, open(answer_file, 'r', encoding='utf-8') as af, open(source_file, 'r', encoding='utf-8') as sf, open(expected_source_file, 'r', encoding='utf-8') as esf:
                prompt = pf.read().strip()
                answer = af.read().strip()
                source = sf.read().strip()
                expected_source = esf.read().strip()
        except FileNotFoundError as e:
            log(f"⚠️ File not found: {e.filename}")
            continue

        # Handling expected answers
        expected_answers = []
        if os.path.isdir(expected_answer_path):  # Check if path is a directory (for multiple answers)
            for answer_file in sorted(os.listdir(expected_answer_path)):
                with open(os.path.join(expected_answer_path, answer_file), 'r', encoding='utf-8') as af:
                    expected_answers.append(af.read().strip())
        elif os.path.isfile(expected_answer_path + '.txt'):  # Check if there is a single answer file
            with open(expected_answer_path + '.txt', 'r', encoding='utf-8') as af:
                expected_answers.append(af.read().strip())
        
        # Constructing the JSON object
        entry = {
            'id': i + 1,  # IDs starting from 1
            'model_dir': str(base_dir),
            'prompt': prompt,
            'answer': answer,
            'source': source,
            'expected_sources': expected_source,
            'expected_answers': expected_answers  # Adding the expected_answers array
        }
        
        # Adding the entry to the list
        data.append(entry)

    log(f"Processed model directory: {base_dir} with {len(data)} entries.")

    return data

def try_process_model_dirs(model_dirs):
    for d in model_dirs:
        try:
            yield process_model_dir(d)
        except Exception as e:
            log("❌ Error processing model directory:", d, e)

results = list(try_process_model_dirs(model_dirs))

with open("./processed_results_tmp.jsonl", "w") as f:
    for r in results:
        json.dump(r, f)
        f.write("\n")

tmpfile = open("./tmp.jsonl", "w")

def prep_samples(result):
    import re
    exp_sources = result['expected_sources']
    if type(exp_sources) == str:
        exp_sources = [exp_sources]
    target = QASample(result['id'], result['prompt'], result['expected_answers'], exp_sources)
    pred_sources = result['source']
    pred_sources = [
        re.sub(r"^- ", "", s.strip())
        for s in
        pred_sources.split('\n')
    ]
    pred = QASample(result['id'], result['prompt'], [result['answer']], pred_sources)
    return pred, target

def eval_result(results):
    def process():
        for result in results:
            pred, target = prep_samples(result)
            pair_metrics = compute_metrics_pair(pred, target)
            pair_metrics['id'] = result['id']
            tmpfile.write(json.dumps(pair_metrics))
            tmpfile.write("\n")
        yield pred, target
    results = list(process())
    preds = [r[0] for r in results]
    targets = [r[1] for r in results]

    return compute_aggregate_metrics(preds, targets)

def eval_result2(results):
    for result in results:
        pred, target = prep_samples(result)
        pair_metrics = compute_metrics_pair(pred, target)
        pair_metrics['id'] = result['id']
        yield pair_metrics

for r in results:
    for m in eval_result2(r):
        json.dump({
            'model_dir': r[0]['model_dir'],
            **m
        }, sys.stdout)
        print()

tmpfile.close()