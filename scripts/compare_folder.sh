# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

# List and format names from the first directory, removing .faiss
curdir=$(pwd)
cd data/embeds/intfloat-multilingual-e5-large_128/folders/
find . -type d -name "*.faiss" | sed 's/\.faiss$//' | sort > /tmp/list1.txt

cd $curdir
# List names from the second directory
cd data/versions
find . -type d | sort > /tmp/list2.txt

cd $curdir

# Compare the lists
diff /tmp/list1.txt /tmp/list2.txt

