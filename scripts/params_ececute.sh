# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

srun -A jdh@a100 \
    --qos=qos_gpu-dev \
    --gres=gpu:4 \
    --cpus-per-task=10 \
    --hint=nomultithread \
    --time=02:00:00 \
    --pty \
    -C a100 \
    -- bash scripts/gpu_execute.sh \
    --file_format "MD" \
    --ender_id 2 \
    --inputdir "data/versions" \
    --device "GPU" \