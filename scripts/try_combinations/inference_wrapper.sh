#!/bin/bash

# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

set -x

# Path to the CSV file
csv_file="notebooks/combinations.csv"

# This is so arguments can be passed and override the defaults
# while [[ $# -gt 0 ]]; do
#     case "$1" in
#         --inputdir)
#             inputdir="$2"
#             shift 2
#             ;;
#         --output)
#             output="$2"
#             shift 2
#             ;;
#         *)
#             echo "Unknown option: $1"
#             exit 1
#             ;;
#     esac
# done

inputdir=$1
output=$2


# Skip the header line
tail -n +2 "$csv_file" | head -n 1 | while IFS=, read -r embedding_model embedding_dimensions seq_chunk_size seq_token_max_size; do
    bash scripts/gpu_execute_runner.sh \
        --device "GPU" \
	--db "data/embeds/${embedding_model//\//-}_${seq_token_max_size}/merged.faiss" \
        --embedding_model "$embedding_model" \
        --embedding_dimensions "$embedding_dimensions" \
        --seq_chunk_size "$seq_chunk_size" \
        --seq_token_max_size "$seq_token_max_size" \
        --inference_model "mistralai/Mistral-7B-v0.1" \
        --inputdir "${inputdir:-"data/dataset/train/input"}" \
        --output "${output:-"tmp/run"}"
done

#         --inference_model "meta-llama/Llama-2-70b-hf/"