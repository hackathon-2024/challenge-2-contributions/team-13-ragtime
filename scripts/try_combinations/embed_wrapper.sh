#!/bin/bash

# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

# Path to the CSV file
csv_file="notebooks/combinations.csv"

# Skip the header line
tail -n +2 "$csv_file" | while IFS=, read -r embedding_model embedding_dimensions seq_chunk_size seq_token_max_size; do
    bash scripts/gpu_execute.sh \
        --file_format "MD" \
        --inputdir "data/versions" \
        --device "GPU" \
	--outputdir "data/embeds" \
        --embedding_model "$embedding_model" \
        --embedding_dimensions "$embedding_dimensions" \
        --seq_chunk_size "$seq_chunk_size" \
        --seq_token_max_size "$seq_token_max_size"
done

