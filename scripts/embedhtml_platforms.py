# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import os
import time
import argparse
from langchain.retrievers import ParentDocumentRetriever
from langchain.storage import LocalFileStore
from langchain_community.docstore import InMemoryDocstore

# Yes, it's a hack, see langchain #9345
from langchain.storage._lc_store import create_kv_docstore
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain_community.document_loaders import TextLoader
from langchain_community.vectorstores import FAISS
from langchain_openai import OpenAIEmbeddings
from faiss import IndexFlatL2
import getpass
import glob
from ragtime.params import params
from ragtime.query_analyser.localembeds import LocalEmbeddings
from ragtime.logger.logger import log

from pathlib import Path

import pandas as pd


def retrieve_url(doc):
    """
    Retrieve the URL that is embedded in the first line of the
    Markdown document as follows:
    # Resource URL: https://developers.facebook.com/docs/ad-targeting-dataset/get-started

    Args:
        doc (Document): The document object.

    Returns:
        doc.metadata["url"] (str): The URL.
    """

    lines = doc.page_content.split("\n")
    for line in lines:
        if line.startswith("# Resource URL:"):
            return line.split(" ")[3]
    return None


def main(args, param_obj):
    """
    Main function that loads HTML files, initializes the vector
    store, and adds documents to the parent document retriever.

    Args:
        args (object): Command line arguments.
        param_obj (object): Parameter object.

    Returns:
        None
    """
    param_obj.seq_chunk_size = args.seq_chunk_size
    param_obj.file_format = args.file_format
    param_obj.dimensions = args.embedding_dimensions
    param_obj.max_seq_length = args.seq_token_max_size
    param_obj.embedding_model = args.embedding_model
    if param_obj.device == "LOCAL":
        os.environ["OPENAI_API_KEY"] = getpass.getpass("OpenAI API Key:")
        embeddings = OpenAIEmbeddings()
        param_obj.embedding_model = "OpenAIEmbeds"
        param_obj.max_seq_length = 1024
    else:
        os.environ[
            "SENTENCE_TRANSFORMERS_HOME"
        ] = "/gpfswork/rech/jdh/uxh65dw/tmp/.cache/huggingface/hub"
        checkpoint = f"{param_obj.embedding_model}"
        device = "cuda" if param_obj.device == "GPU" else "cpu"
        log(f"Using {device} for inference on {checkpoint}.")
        param_obj.dimensions = args.embedding_dimensions
        param_obj.max_seq_length = args.seq_token_max_size
        param_obj.seq_chunk_size = args.seq_chunk_size
        embeddings = LocalEmbeddings(
            checkpoint=checkpoint,
            device=device,
            param_obj=param_obj,
        )

    if args.buckets_file:
        bucket_mapping = pd.read_csv(args.buckets_file)
        assert (
            args.bucket_index is not None
        ), "Need bucket index if using bucket mapping"
        where = bucket_mapping["index"] == args.bucket_index
        this_bucket = set(bucket_mapping[where]["Buckets"])
    else:
        versions_dir = (
            Path(__file__).absolute().parents[1] / "data/versions"
        )  # XXX: hardcoded paths no good
        this_bucket = set([d.name for d in versions_dir.iterdir()])

    # Intialize the Vector Store
    log("Initializing the vector store...")
    dimensions: int = (
        len(embeddings.embed_query("dummy"))
        if param_obj.device == "LOCAL"
        else param_obj.dimensions
    )

    retrievers = {}

    sanitized_model = param_obj.embedding_model.replace("/", "-")
    args.outputdir = (
        Path(args.outputdir) / f"{sanitized_model}_{param_obj.max_seq_length}"
    )
    args.outputdir.mkdir(parents=True, exist_ok=True)
    log(this_bucket)
    log(args.outputdir)
    for folder in this_bucket:
        if folder in ["versions", ".git"]:
            continue
        # store = InMemoryDocstore()
        docstore = InMemoryDocstore()
        kvstore = LocalFileStore(args.outputdir / "folderstores" / f"{folder}.kvstore")
        vectorstore = FAISS(
            embedding_function=embeddings,
            index=IndexFlatL2(dimensions),
            docstore=docstore,
            index_to_docstore_id={},
        )
        log(
            f"Initialized the vector store with FAISS and {dimensions} dimensions"
            f" kv-docstore at {kvstore.root_path}."
        )
        # This text splitter is used to create the child documents
        child_splitter = RecursiveCharacterTextSplitter(
            chunk_size=param_obj.seq_chunk_size,
            chunkOverlap=int(param_obj.seq_chunk_size / 4),
        )

        log("Initializing the parent document retriever...")
        # The storage layer for the parent documents
        retriever = ParentDocumentRetriever(
            vectorstore=vectorstore,
            docstore=create_kv_docstore(kvstore),
            child_splitter=child_splitter,
        )
        retrievers[folder] = retriever
    # Load the files
    log("Loading the files...")
    default_dir = (
        "data/snapshots" if param_obj.file_format == "HTML" else "data/versions"
    )
    file_format = "html" if param_obj.file_format == "HTML" else "md"
    directory = args.inputdir if args.inputdir else default_dir
    files = glob.glob(directory + f"/**/*.{file_format}", recursive=True)

    start_time = time.time()
    skipped = 0
    added = 0
    for file_path in files:
        file_path = Path(file_path)

        log(file_path.absolute())
        folder = file_path.parts[-2]
        if args.buckets_file and (folder not in this_bucket):
            skipped += 1
            if skipped % 100 == 0:
                log(f"Skipped {skipped} documents.")
            continue
        elif folder in ["versions", ".git"]:
            continue

        loader = TextLoader(file_path)
        doc = loader.load()
        doc[0].metadata["url"] = retrieve_url(doc[0])
        log(f"Adding document: {doc[0].metadata['source']}")
        retriever = retrievers[folder]
        retriever.add_documents(doc, ids=None)
        added += 1
        db_loc = args.outputdir / "folders" / f"{folder}.faiss"
        retriever.vectorstore.save_local(db_loc)
        log(f"Saved vectorstore for {folder} at {db_loc}")
    log(f"Skipped {skipped} documents.")
    log(f"Added {added} documents.")
    log(f"Total time: {time.time() - start_time}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--device",
        help="device to run on (LOCAL, CPU, or GPU)",
        default="LOCAL",
        choices=["LOCAL", "CPU", "GPU"],
    )
    parser.add_argument(
        "--inputdir",
        help="input directory",
        default="data/snapshots",
    )
    parser.add_argument(
        "--outputdir",
        help="input directory",
        default="data/raw.db",
    )
    parser.add_argument(
        "--starter_id",
        help="first document to process",
        type=int,
        default=0,
    )
    parser.add_argument(
        "--ender_id",
        help="last document to process",
        type=int,
        default=-1,
    )
    parser.add_argument(
        "--seq_chunk_size",
        help="Size of Sequence Chunks",
        type=int,
        default=350,
    )
    parser.add_argument(
        "--seq_token_max_size",
        help="Max size of token sequences",
        type=int,
        default=128,
    )
    parser.add_argument(
        "--file_format",
        help="HTML or MD?",
        type=str,
        choices=["HTML", "MD"],
        default="HTML",
    )
    parser.add_argument(
        "--log",
        help="log file",
        default="default.jsonl",
    )
    parser.add_argument(
        "--buckets-file", help="optional platform-buckets assignment", default=None
    )
    parser.add_argument(
        "--bucket-index", help="bucket index for this run", type=int, default=None
    )
    parser.add_argument(
        "--embedding_model",
        help="Which Embedding Model to use?",
        type=str,
        default="intfloat/multilingual-e5-large",
    )
    parser.add_argument(
        "--embedding_dimensions",
        help="how many dimensions in the model",
        type=int,
        default=1024,
    )

    args = parser.parse_args()
    param_obj = params.Params()
    param_obj.device = args.device
    log(args, param_obj)
    main(args, param_obj)
