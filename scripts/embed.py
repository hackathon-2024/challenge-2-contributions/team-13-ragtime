# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import os
import getpass
from langchain_community.document_loaders import TextLoader
from langchain_openai import OpenAIEmbeddings
from langchain.text_splitter import CharacterTextSplitter
from langchain_community.vectorstores import FAISS

current_folder = os.path.dirname(os.path.abspath(__file__))


if __name__ == "__main__":
    os.environ["OPENAI_API_KEY"] = getpass.getpass("OpenAI API Key:")

    # Load the document, split it into chunks, embed each chunk and load it into the vector store.
    print("Loading")
    raw_documents = TextLoader(
        f"{current_folder}/../data/state_of_the_union.txt"
    ).load()
    text_splitter = CharacterTextSplitter(chunk_size=1000, chunk_overlap=0)
    documents = text_splitter.split_documents(raw_documents)
    print("embedding")
    db = FAISS.from_documents(documents, OpenAIEmbeddings())
    db.save_local(f"{current_folder}/../data/faiss.db")

    print("querying")
    query = "What did the president say about Ketanji Brown Jackson"
    docs = db.similarity_search(query)
    print(docs[0].page_content)


# TOMORROW: Parent document retriever, by really relying on the original corpus
