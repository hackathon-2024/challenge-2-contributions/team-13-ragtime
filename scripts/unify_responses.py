# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import os
import json
import argparse
# Create the argument parser
parser = argparse.ArgumentParser()
parser.add_argument('--base_dir', type=str, help='Base directory path')
args = parser.parse_args()

# Get the base directory from the command line argument
base_dir = args.base_dir

# Define the prompts, answers, and sources directories
prompts_dir = os.path.join(base_dir, 'prompts')
answers_dir = os.path.join(base_dir, 'answers')
sources_dir = os.path.join(base_dir, 'sources')

# Define the expected sources directory
expected_sources_dir = 'data/dataset/train/output/sources'
expected_answers_dir = 'data/dataset/train/output/answers'

# Initialize an empty list to store the data
data = []

for i in range(10):  # Adjust the range if there are more files
    # Constructing file paths
    prompt_file = os.path.join(prompts_dir, f'{i}.txt')
    answer_file = os.path.join(answers_dir, f'{i}.txt')
    source_file = os.path.join(sources_dir, f'{i}.txt')
    expected_source_file = os.path.join(expected_sources_dir, f'{str(i).zfill(3)}.txt')
    expected_answer_path = os.path.join(expected_answers_dir, f'{str(i).zfill(3)}')

    # Reading file contents
    with open(prompt_file, 'r', encoding='utf-8') as pf, open(answer_file, 'r', encoding='utf-8') as af, open(source_file, 'r', encoding='utf-8') as sf, open(expected_source_file, 'r', encoding='utf-8') as esf:
        prompt = pf.read().strip()
        answer = af.read().strip()
        source = sf.read().strip()
        expected_source = esf.read().strip()

    # Handling expected answers
    expected_answers = []
    if os.path.isdir(expected_answer_path):  # Check if path is a directory (for multiple answers)
        for answer_file in sorted(os.listdir(expected_answer_path)):
            with open(os.path.join(expected_answer_path, answer_file), 'r', encoding='utf-8') as af:
                expected_answers.append(af.read().strip())
    elif os.path.isfile(expected_answer_path + '.txt'):  # Check if there is a single answer file
        with open(expected_answer_path + '.txt', 'r', encoding='utf-8') as af:
            expected_answers.append(af.read().strip())
    
    # Constructing the JSON object
    entry = {
        'id': i + 1,  # IDs starting from 1
        'prompt': prompt,
        'answer': answer,
        'source': source,
        'expected_sources': expected_source,
        'expected_answers': expected_answers  # Adding the expected_answers array
    }
    
    # Adding the entry to the list
    data.append(entry)


# Writing the JSON data to a file
json_file_path = 'output_with_expected_sources.json'
with open(json_file_path, 'w', encoding='utf-8') as jf:
    json.dump(data, jf, indent=4, ensure_ascii=False)
