# SPDX-FileCopyrightText: 2024 rag-time
# SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: EUPL-1.2

import os
from urllib.parse import urlparse
directory = 'data/versions'

def retrieve_url(filepath):
    with open(filepath, 'r') as file:
        for line in file:
            if line.startswith("# Resource URL:"):
                url = line.split(" ")[3]
                parsed_url = urlparse(url)
                return parsed_url.netloc
    return None

def get_all_results():
    results = set()
    for root, dirs, files in os.walk(directory):
        for dir in dirs:
            dir_path = os.path.join(root, dir)
            for filename in os.listdir(dir_path):
                if filename.endswith('.md'):
                    filepath = os.path.join(dir_path, filename)
                    result = retrieve_url(filepath)
                    if result:
                        results.add(result)
    return results

all_results = get_all_results()
print(all_results)
