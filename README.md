<!--
SPDX-FileCopyrightText: 2024 rag-time
SPDX-FileContributor: Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>

SPDX-License-Identifier: EUPL-1.2
-->

# Team Name : Rag Time

## Licence / License

:fr: Ce code a été produit pour le Challenge 2 du Hackathon 2024 co-organisé par le PEReN et la Commission Européenne : DSA RAG Race. Il est délivré comme récupéré à la fin de l'événement et peut donc ne pas être exécutable.
Sauf mention contraire, le code source est placé sous licence publique de l'Union Européenne, version 1.2 (EUPL-1.2).
Les données des plateformes présentes sur ce projet sont la propriété des plateformes et ne sont fournies qu'à titre d'illustration pour le bon fonctionnement du projet. Elles ne seront en aucun cas mises à jour. Les données complètes peuvent être trouvées vers ce lien : https://code.peren.fr/hackathon-2024/retrieval-modules/platform-docs-versions

:gb: This code was developed for Challenge 2 of the Hackathon 2024 co-organized by the PEReN and the European Commission: DSA RAG Race. It is released as retrieved at the end of the event and may therefore not be executable.
Unless otherwise specified, the source code is licensed under the European Union Public License, version 1.2 (EUPL-1.2).
The data of platforms contained in this project are the property of the platforms and are provided for illustrative purposes only. They will not be updated under any circumstances. The complete data can be found at the following link: https://code.peren.fr/hackathon-2024/retrieval-modules/platform-docs-versions

## Bibliothèques utilisées

[faiss-cpu](https://pypi.org/project/faiss-cpu/)  
[langchain](https://www.langchain.com/)  
[langchain-community](https://pypi.org/project/langchain-community/)  
[langchain-openai](https://pypi.org/project/langchain-openai/)  
[levenshtein](https://pypi.org/project/Levenshtein/)  
[pandas](https://pandas.pydata.org/)  
[rouge-score](https://pypi.org/project/rouge-score/)  
[sacrebleu](https://pypi.org/project/sacrebleu/1.0.0/)  
[sentence-transformers](https://www.sbert.net/)  
[transformers](https://huggingface.co/docs/transformers/index)  


## How to get the data

```bash
git clone ssh://git@code.peren.fr:2224/hackathon-2024/retrieval-modules/platforms-doc-declarations.git
cd platforms-doc-declarations
npm install
npx ota track
cp -r data/snapshots/ ../2024-peren-hackathon-rag-time/data/snapshots/
cp -r data/versions/ ../2024-peren-hackathon-rag-time/data/versions/
```

## Lancer le RAG

```bash
bash run2.sh
```

Il est possible dans ce fichier de modifier :
- le modèle d'embedding utilisé
- la taille des embeddings
- La taille des chunks
- La longueur maximale de génération.

Le dossier avec les réponses est créé à la racine du projet et contient trois sous-dossiers :
- answers
- prompts
- sources

Un fichier `.txt` est créé pour chaque réponse dans chaque sous-dossier.