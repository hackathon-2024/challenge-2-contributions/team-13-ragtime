To know if a Facebook advertising campaign has deviated from its intended target, you can use the Facebook Ads API. The API provides information about all available ads, including the library ID, content of the ad creative (subject to our [Terms of Service](https://www.facebook.com/terms.php)), page name and page ID associated with the ad, ad delivery dates, where the ad appeared (Facebook, Instagram, etc.), ads about social issues, elections or politics will also have total amount spent (range), total impressions received (range), demographic information on ad reach, such as age, gender and location (%), ads that were delivered to the European Union will also have total impressions an ad received in the EU (estimated), targeting and reach demographic information specific to the EU (estimated), beneficiary and payer information, in the case of estimated values, aggregated demographic data is based on a number of factors, including age and gender information users provide in their Facebook profile.
To retrieve data on ads about social issues, elections or politics that contain the term "**california**" and that reached an audience in the United States, you can enter this query:
`curl -G \\`
`-d "search_terms=\'california\'" \\`
`-d "ad_type=POLITICAL_AND_ISSUE_ADS" \\`
`-d "ad_reached_countries=[\'US\']" \\`
`-d "access_token=<ACCESS_TOKEN>" \\`
`"https://graph.facebook.com/<API_VERSION>/ads_archive"`

