Content moderation on X is carried out through a combination of technology and other purpose-built internal proprietary tools. When we remove CSE content, we immediately report it to the National Center for Missing and Exploited Children (NCMEC). NCMEC makes reports available to the appropriate law enforcement agencies around the world to facilitate investigations and prosecutions.

