LinkedIn received 10 data requests from the Italian government in 2022. The source for this information is the [LinkedIn Transparency Report](https://transparency.linkedin.com/government-data-requests/2022/italy), which provides detailed information about the number of data requests received from various governments around the world.


