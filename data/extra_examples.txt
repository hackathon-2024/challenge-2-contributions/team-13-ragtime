Question1: With Google Custom Search, how do I get a gzip-encoded response?
Answer1: Set an `Accept-Encoding: gzip` header in your request, and modify the `User-Agent` header to include `gzip` as well. The response will be gzip-encoded.
