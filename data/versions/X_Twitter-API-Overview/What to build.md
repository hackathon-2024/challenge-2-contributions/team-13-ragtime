# Resource URL: https://developer.twitter.com/en/docs/twitter-api/what-to-build
What to build

Introduction
------------

Our developer community has unique skills, experiences, and perspectives that can fill gaps, solve problems, and seed new innovation on Twitter well beyond what we at Twitter can do on our own. We encourage you to build tools and products that make Twitter better, healthier, and extend the public conversation. In particular, we want to see innovation in the following categories:

* [Moderate conversations for health and safety](#moderate)
* [Enable creation and personal expression](#enable)
* [Measure and analyze what’s happening](#measure)
* [Improve community experiences](#improve)
* [Curate and recommend content](#curate)
* [Impact the greater good](#impact)

### Moderate conversations for health and safety

We want everyone to feel comfortable, safe, and excited to participate in the conversation on Twitter. We’ve worked hard to offer tools that give people more control over their experience, like Tweet reply settings, blocks, and mutes. However, there will always be more work to do, and we want to encourage you to build complementary or additive solutions that help improve the health and safety of the platform. Check out some of our resources below to help you get started:

Relevant endpoints: 

* [Follows](https://developer.twitter.com/en/docs/twitter-api/users/follows/introduction)
* [Blocks](https://developer.twitter.com/en/docs/twitter-api/users/blocks/introduction)
* [Mutes](https://developer.twitter.com/en/docs/twitter-api/users/mutes/introduction)
* [Hide replies](https://developer.twitter.com/en/docs/twitter-api/tweets/hide-replies/introduction)
* [Manage Tweets](https://developer.twitter.com/en/docs/twitter-api/tweets/manage-tweets/introduction) (Tweet reply settings)  
     

Sample apps:  

* [Hide replies with Perspective API](https://glitch.com/edit/#!/twitter-hide-replies-perspective-api)
* [Hide replies with Tweet Annotations](https://glitch.com/edit/#!/twitter-hide-replies-annotations-v2)  
     

Success stories: 

* [Block Party](https://developer.twitter.com/en/community/success-stories/tracy-chou-block-party)

### Enable creation and personal expression

Twitter is where people come to share their perspective, ideas, and passions. It’s the convergence of individuals and brands, founders and athletes, celebrities and CEOs. Developers have an opportunity to build creative solutions that allow people on Twitter to expand their reach, express themselves in new ways, and connect with like minded communities. As creators and innovators themselves, developers can also build things that broaden how people experience Twitter like helpful bots, gamification, or cross-posted content. Below are a few ideas to seed your innovation: 

Relevant endpoints:

* [Manage Tweets](https://developer.twitter.com/en/docs/twitter-api/tweets/manage-tweets/introduction)
* [Spaces](https://developer.twitter.com/en/docs/twitter-api/spaces/overview)
* [Retweets](https://developer.twitter.com/en/docs/twitter-api/tweets/retweets/introduction)  
     

Sample apps:

* [Spaces Reach](https://github.com/twitterdev/spaces-reach)
* [FactualCat Twitter bot](https://github.com/twitterdev/FactualCat-Twitter-Bot/)

### Measure and analyze “what’s happening”

Many people come to Twitter because of the opportunity it presents to reach new or existing audiences, amplify their voice, or have an impact. Developers can help these creators, artists, businesses, and local advocates measure, analyze, or understand the impact of their content. Additionally, developers can build new solutions that help people derive insights from the public conversation about trends, their audience, and so much more. We’ve put together a few resources below to help you get started: 

Relevant endpoints/functionality: 

* [Object model](https://developer.twitter.com/en/docs/twitter-api/data-dictionary/object-model/tweet) (Tweets, Users, Spaces, etc)
* [Advanced metrics](https://developer.twitter.com/en/docs/twitter-api/metrics)
* [Tweet annotations](https://developer.twitter.com/en/docs/twitter-api/annotations/overview)
* [Search Tweets](https://developer.twitter.com/en/docs/twitter-api/tweets/search/introduction)
* [Tweet counts](https://developer.twitter.com/en/docs/twitter-api/tweets/counts/introduction)
* [Filtered stream](https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/introduction)
* [Sampled stream](https://developer.twitter.com/en/docs/twitter-api/tweets/volume-streams/introduction)  
     

Tutorials:

* [Listen for important events](https://developer.twitter.com/en/docs/tutorials/listen-for-important-events)
* [Analyze past conversations](https://developer.twitter.com/en/docs/tutorials/analyze-past-conversations)
* [Analyze the sentiment of your own Tweets](https://developer.twitter.com/en/docs/tutorials/how-to-analyze-the-sentiment-of-your-own-tweets)  
     

Success stories:   

* [Audience](https://developer.twitter.com/en/community/success-stories/audiense)
* [TweepsMap](https://developer.twitter.com/en/community/success-stories/samir-al-battran-tweeps-map)

### Improve community experiences

As new communities grow and convene on Twitter, we want developers to use their expertise to help serve the unique needs of these communities. Twitter is home of the global conversation, and we want everyone to be able to participate regardless of their language, timezone, cultural differences, or other localized needs. Additionally, we want growing communities around topics like finance or gaming to have tools built for their unique experiences and needs. 

### Curate and recommend content

Developers have always come up with creative ways to connect people on Twitter to content they’re interested in. Whether it’s apps, bots, or other tools that help people have more curated or customized experiences, this is an opportunity for developers to connect people to the content they care most about. Get started: 

Relevant endpoints:

* [Spaces](https://developer.twitter.com/en/docs/twitter-api/spaces/overview)
* [Manage Tweets](https://developer.twitter.com/en/docs/twitter-api/tweets/manage-tweets/introduction)
* [Lists](https://developer.twitter.com/en/docs/twitter-api/lists/manage-lists/introduction)  
     

Sample apps:

* [Spaces Search](https://developer.twitter.com/en/community/success-stories/spaces-search)

### Impact the greater good

We’ve seen so many ways that developers have used Twitter to help make the world a better place. From groundbreaking research, to helpful bots, to other non-commercial innovation, developers never cease to amaze us. Below are just a few of the resources to help you get started: 

Relevant endpoints:

* [Manage Tweets](https://developer.twitter.com/en/docs/twitter-api/tweets/manage-tweets/introduction)
* [Conversation ID](https://developer.twitter.com/en/docs/twitter-api/conversation-id)
* [Tweet annotations](https://developer.twitter.com/en/docs/twitter-api/annotations/overview)
* [Search Tweets](https://developer.twitter.com/en/docs/twitter-api/tweets/search/introduction)
* [Tweet counts](https://developer.twitter.com/en/docs/twitter-api/tweets/counts/introduction)
* [Filtered stream](https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/introduction)
* [Sampled stream](https://developer.twitter.com/en/docs/twitter-api/tweets/volume-streams/introduction)  
     

Success stories: 

* [UC Davis and Max Planck Institute](https://developer.twitter.com/en/community/success-stories/uc-davis-max-planck-institute)
* [Penn Medicine CDH](https://developer.twitter.com/en/community/success-stories/penn)
* [HateLab](https://developer.twitter.com/en/community/success-stories/hatelab)

  
Explore [different access levels](https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api)