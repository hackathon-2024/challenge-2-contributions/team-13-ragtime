# Resource URL: https://fort.fb.com/researcher-apis
Researcher API
==============

#### RESEARCHER API

#### **One of the first Meta APIs designed for academics**

Qualifying academics can use the Researcher API to access near real-time data as well as billions of historical data points, which equips researchers to study topics of interest and gain a window into evolving or emerging topics on Facebook.

###### Product Roadmap

We know we have a lot of work to do and our tools are a work in progress. That’s why these products are informed by feedback from researchers all over the world, and we’re always looking for more. The input we’ve already received has helped us iterate and ensure the tools we’re building work for you - and we’ll be launching more soon.

### Now Live: Researcher API Early Access Program

The Researcher API was specifically designed for academic needs. It equips qualified academics to conduct longitudinal research across all public Facebook Pages, Groups, Posts and Events in the US and select EU countries. Researchers can use this product to understand how public discussions on Facebook influence the social issues of the day. We offer this product via the Researcher Platform, which allows us to share privacy-protected data in a secure way.

We have invited a small group of qualified academics to test this product and provide feedback so we can iterate and improve it, before launching to a broader group of researchers.

Features
--------

##### **Analytics**

A collection of API endpoints that provides researchers with aggregated insights on public forums like Groups & Pages

##### **Near real-time + historical data**

A collection of API endpoints that provides researchers access to raw, anonymized data from public forums on Facebook including Groups, Events & Pages

##### **Search**

A collection of API endpoints that gives researchers the ability to search across Facebook’s public forums in order to retrieve data that matches their search queries

###### Sign up to learn more

Information about Facebook Open Research & Transparency, including access and eligibility
-----------------------------------------------------------------------------------------

[Sign up](https://fort.fb.com/intake)