# Resource URL: https://values.snap.com/fr-FR/privacy/transparency
Rapport de transparence

1er janvier 2023 - 30 juin 2023

Publication :

25 octobre 2023

Mise à jour :

25 octobre 2023

Pour donner un aperçu des efforts de Snap en matière de sécurité ainsi que de la nature et du volume du contenu signalé sur notre plateforme, nous publions des rapports sur la transparence deux fois par an. Nous entendons poursuivre la publication de ces rapports dans l'objectif de les rendre plus complets et plus documentés, afin que les nombreuses parties prenantes qui s'intéressent de près à nos pratiques en matière de modération de contenu et d'application de la loi, ainsi qu'au bien-être de notre communauté puissent en profiter.  

Le présent rapport de transparence couvre le premier semestre 2021 (1er janvier - 30 juin). Comme pour nos précédents rapports, nous partageons les données sur le nombre total de contenus dans l'application, les signalements au niveau des comptes que nous avons reçus et sanctionnés dans des catégories spécifiques de violations des politiques, la manière dont nous avons répondu aux demandes des forces de l'ordre et des gouvernements, et nos différentes sanctions ventilées par pays. et nos mesures d’exécution ventilées par pays.  
  
Dans le cadre de notre engagement permanent à améliorer nos rapports sur la transparence, nous introduisons quelques nouveaux éléments avec cette publication. Nous avons ajouté des points de données supplémentaires sur nos pratiques publicitaires et notre modération, ainsi que sur le contenu et les appels sur le compte. Pour se conformer à la loi de l’UE sur les services numériques, nous avons également ajouté de nouvelles informations contextuelles sur nos opérations dans les états membres de l’UE, telles que le nombre de modérateurs de contenu et d’utilisateurs actifs mensuels (MAU) dans la région. Une grande partie de ces informations se trouve dans le rapport et sur la page de notre [Centre de transparence](https://values.snap.com/fr-FR/privacy/transparency/european-union) dédiée à l'Union européenne.

Enfin, nous avons mis à jour notre [Glossaire](https://values.snap.com/fr-FR/privacy/transparency/glossary)en y ajoutant des liens vers nos « Explications sur les règles communautaires », qui offrent un contexte supplémentaire sur la politique de notre plateforme et sur nos efforts opérationnels.

Pour plus d'informations sur nos politiques de lutte contre les contenus préjudiciables en ligne et sur nos plans visant à améliorer nos pratiques de signalement, veuillez consulter notre récent [blog sécurité et impact](https://snap.com/en-US/safety-and-impact?lang=fr-FR) sur ce rapport de transparence.

Pour trouver des ressources supplémentaires en matière de sécurité et de vie privée sur Snapchat, cliquez sur notre onglet [À propos du rapport sur la transparence](https://www.snap.com/en-US/privacy/transparency/about?lang=fr-FR) au bas de la page.

Veuillez noter que la version la plus récente de ce rapport de transparence est disponible en anglais (en-US).

### 

Aperçu du contenu et des comptes en infraction

Du 1er janvier au 30 juin 2023, Snap a fait imposer 6 216 118 contenus dans le monde entier qui ont violé nos politiques.

Pendant ladite période, nous avons enregistré un taux de vues non conformes (TVNC) de 0,03 %, ce qui signifie que sur 10 000 vues de contenu Snap et de Story sur Snapchat, trois contenaient du contenu qui violait nos politiques.

| Nombre total de signalements sur le contenu et les comptes | Nombre total de contenu sanctionné | Nombre total de comptes uniques sanctionnés |
| --- | --- | --- |
| 17,267,985 | 6,216,118 | 3,687,082 |

| Raison | Signalements sur le contenu et les comptes | Contenu sanctionné | % de contenu total sanctionné | Comptes uniques sanctionnés | Délai de réponse (en minutes médianes) |
| --- | --- | --- | --- | --- | --- |
| Contenu à caractère sexuel | 5,843,879 | 3,270,318 | 52.6% | 1,833,194 | 6   |
| Harcèlement et intimidation | 6,645,969 | 920,070 | 14.8% | 778,417 | 13  |
| Menaces et violence | 615,011 | 94,556 | 1.5% | 73,981 | 21  |
| Automutilation et suicide | 122,369 | 24,621 | 0.4% | 22,637 | 21  |
| Fausses informations | 387,242 | 238 | 0.0% | 209 | 12  |
| Usurpation d'identité | 521,168 | 12,379 | 0.2% | 12,314 | 2   |
| Pourriel | 1,924,768 | 1,380,341 | 22.2% | 941,653 | <1  |
| Drogues | 642,421 | 297,554 | 4.8% | 203,939 | 28  |
| Armes | 81,223 | 16,622 | 0.3% | 12,203 | 20  |
| Autres marchandises réglementées | 216,562 | 141,514 | 2.3% | 103,157 | 30  |
| Discours haineux | 267,373 | 57,905 | 0.9% | 49,975 | 32  |

\*L'application correcte et cohérente de la législation contre les fausses informations est un processus dynamique qui nécessite un contexte actualisé et de la diligence.  Comme nous nous efforçons d'améliorer continuellement la précision de l'application de la loi de nos agents dans cette catégorie, nous avons choisi, depuis le premier semestre 2022, de signaler des chiffres dans les catégories « Contenu appliqué » et « Comptes uniques appliqués » qui sont estimés sur la base d'un examen rigoureux d'assurance qualité d'une partie statistiquement significative de l'application de la loi sur les fausses informations.  Plus précisément, nous échantillonnons une partie statistiquement significative des cas d'application de la législation sur les fausses informations dans chaque pays et contrôlons la qualité des décisions d'application.  Nous utilisons ensuite ces contrôles de qualité pour obtenir des évaluations de modération avec un intervalle de confiance de 95 % (marge d'erreur de +/- 5 %), que nous utilisons pour calculer le nombre de modération pour fausses informations signalées dans le rapport sur la transparence. 

### 

Analyse des violations de contenus et de comptes

Nos taux globaux de signalement et d’application sont restés assez similaires à ceux des six mois précédents, à quelques exceptions près dans les catégories clés. Nous avons observé une diminution d’environ 3 % du nombre total de contenu, de rapports et de modération sur ce cycle.

Les catégories les plus importantes étaient le harcèlement et l’intimidation, le spam, les armes et les fausses informations. Le harcèlement et l’intimidation ont augmenté d'environ 56 % comparé au nombre total de signalements, puis une augmentation de près de 39 % du contenu et des impositions sur le compte unique. Ces augmentations des exécutions ont été couplées à une diminution d’environ 46 % du délai d’exécution, ce qui souligne l’efficacité opérationnelle de notre équipe pour ce type de contenu. De même, nous avons observé une augmentation d’environ 65 % du nombre total de signalements de spam, une augmentation de près de 110 % des modérations de contenu et une augmentation de près de 80 % des comptes uniques imposés à la loi et nos équipes ont également réduit le délai de traitement de près de 80 %. Notre catégorie d’armes a vu une diminution d’environ 13 % du nombre total de signalements, une diminution d’environ 51 % des modérations de contenu et une réduction d’environ 53 % des comptes uniques imposés. Enfin, notre catégorie de fausses informations a vu une augmentation d’environ 14 % du nombre total de signalements, mais une diminution d’environ 78 % des modérations de contenu et une diminution d’environ 74 % des comptes uniques imposés. Cela peut être attribué à la poursuite du processus d’assurance de la qualité (QA) et au recours aux ressources que nous appliquons aux rapports de fausses informations pour nous assurer que nos équipes de modération identifient les fausses informations et agissent avec précision sur la plateforme.

Dans l’ensemble, bien que nous ayons observé des chiffres globalement similaires à ceux de la dernière période, nous pensons qu’il est important de poursuivre l’amélioration des outils de notre communauté pour signaler activement et précisément les violations potentielles telles qu’elles apparaissent sur la plateforme.

### 

La lutte contre l'exploitation et les abus sexuels à l'encontre des enfants

L'exploitation sexuelle de tout membre de notre communauté, en particulier des mineurs, est illégale, odieuse et interdite en vertu de nos Règles communautaires. La prévention, la détection et l'éradication des images d'exploitation et d'abus sexuels sur des enfants (CSEAI) sur notre plateforme est une priorité absolue pour Snap, et nous développons continuellement nos capacités à combattre ces types de crimes comme beaucoup d'autres.

Nous utilisons des outils de détection de technologie active tels que le hachage robuste de PhotoDNA et l'appariement d'imagerie pédopornographique de Google, pour identifier les images et vidéos illégales connues d'abus sexuels sur des enfants, respectivement, et les signaler au centre américain pour les enfants disparus et exploités (NCMEC), comme l'exige la loi. Le NCMEC ensuite, à son tour, collabore avec les forces de l'ordre nationales ou internationales, selon les besoins.

Au cours du premier semestre 2023, nous avons détecté et traité de manière proactive 98 % des violations liées à l'exploitation et aux abus sexuels concernant des enfants signalées ici, soit une augmentation de 4 % par rapport à notre rapport précédent.

| Raison | Nombre total de contenu sanctionné | Nombre total de suppressions de comptes | Nombre total de soumissions au NCMEC\* |
| --- | --- | --- | --- |
| CSEAI | 548,509 | 228,897 | 292 489 |

\*\*Notez que chaque envoi au NCMEC peut contenir plusieurs éléments de contenu. Le total des pièces individuelles de média soumises au NCMEC est égal au total du contenu sanctionné.

### 

Contenu d'extrémistes terroristes et violents

Pendant la période de rapport (1er janvier 2023 au 30 juin 2023), nous avons supprimé 18 comptes pour violation de notre politique d’interdiction du contenu terroriste et extrémiste violent.

Chez Snap, nous supprimons les contenus terroristes et extrémistes violents signalés sur des canaux multiples. Nous encourageons les utilisateurs à signaler les contenus terroristes et extrémistes violents via notre menu de signalement dans l'application, et nous travaillons en étroite collaboration avec les forces de l'ordre pour traiter les contenus terroristes et extrémistes violents susceptibles d'apparaître sur Snap.

| Nombre total de suppressions de comptes |
| --- |
| 18  |

### 

Contenu portant sur l'automutilation et le suicide

La santé mentale et le bien-être des Snapchatters nous tiennent à cœur, ce qui a influencé - et continue d'influencer - nos décisions visant à concevoir Snapchat différemment. En tant que plateforme conçue pour les communications entre de véritables amis, nous pensons que Snapchat peut jouer un rôle unique en permettant aux amis de s'entraider dans les moments difficiles.

Lorsque notre équipe Trust & Safety reconnaît un Snapchatter en détresse, elle a la possibilité de transmettre des ressources de prévention et d'assistance en matière d'automutilation, et d'informer le personnel d'intervention d'urgence, le cas échéant. Les ressources que nous partageons sont disponibles sur notre [liste mondiale de ressources en matière de sécurité](https://values.snap.com/fr-FR/safety/safety-resources), et ces ressources sont accessibles à tous les Snapchatters.

| Nombre total de fois où les ressources pour les suicides ont été partagées. |
| --- |
| 24,278 |

### 

Appels

À partir de ce rapport, nous commençons à signaler le nombre d’appels lancés par des utilisateurs dont les comptes ont été verrouillés pour violation de nos politiques. Nous rétablissons uniquement les comptes qui, selon nos modérateurs, ont été injustement verrouillés. Pendant cette période, nous signalons les appels relatifs au contenu lié aux drogues.  Dans notre prochain rapport, nous nous publierons plus de données relatives aux appels découlant d’autres violations de nos politiques.

| Nombre total d’appels soumis | Nombre total de comptes rétablis | % rétablis | Délai de réponse (en minutes médianes) |
| --- | --- | --- | --- |
| 36,682 | 1,249 | 3.40% | 1,008 |

### 

Modération des publicités

Snap s’engage à veiller à ce que toutes les publicités soient conformes aux politiques de nos plateformes. Nous croyons en une approche responsable et respectueuse de la publicité, en créant une expérience sûre et agréable pour tous nos utilisateurs. Ci-dessous, nous avons inclus des informations sur notre modération publicitaire. Notez que les publicités sur Snapchat peuvent être supprimées pour diverses raisons telles que décrites dans les [politiques publicitaires de Snap](https://snap.com/en-US/ad-policies?lang=fr-FR), y compris les contenus trompeurs, les contenus adulte, les contenus violents ou inquiétants, les discours de haine et la violation de la propriété intellectuelle. En outre, vous pouvez maintenant trouver la [Galerie des publicités](https://adsgallery.snap.com/?lang=fr-FR) de Snapchat dans la barre de navigation de ce rapport sur la transparence. 

| Nombre total de publicités signalées | Nombre total de publicités supprimées |
| --- | --- |
| 186,344 | 5,145 |

### 

Aperçu de pays

Cette section fournit un aperçu de l'application de nos Règles communautaires dans un échantillon de régions géographiques. Nos Règles s'appliquent à tout le contenu de Snapchat - et à tous les Snapchatters - à travers le monde, peu importe leur localisation.

Les informations relatives aux différents pays sont disponibles pour le téléchargement via le fichier CSV ci-joint :

[Télécharger le CSV](https://assets.ctfassets.net/kw9k15zxztrs/e2DKlB1h8pa1GVTWezrVT/898af5bce71d35f53064970e55178ae6/Snap_Transparency_Report_H1_2023_Data.csv)

| Région | Signalements sur le contenu et les comptes | Contenu sanctionné | Comptes uniques sanctionnés |
| --- | --- | --- | --- |
| Amérique du Nord | 6,062,193 | 2,688,389 | 1,618,171 |
| Europe | 4,265,947 | 1,570,674 | 1,001,229 |
| Le reste du monde | 6,939,845 | 1,957,055 | 1,192,703 |
| Total | 17,267,985 | 6,216,118 | 3,687,082 |

![](https://images.ctfassets.net/kw9k15zxztrs/6AfbSamWFJtYHKM7HMeG1N/5837f0447d5ef82f1664560d188d11d3/australia-flag_2x.png?q=40&h=600)

[Australie](https://values.snap.com/fr-FR/privacy/transparency/australia)

![](https://images.ctfassets.net/kw9k15zxztrs/7bCP6c9151VMF8o2b3E8Z/6802aaacdcf9717de0cb14a8ed16629c/austria-flag_2x.png?q=40&h=600)

[Autriche](https://values.snap.com/fr-FR/privacy/transparency/austria)

![](https://images.ctfassets.net/kw9k15zxztrs/6HP7dLyg9kARPiT6YzLK4K/4817952339a9c6df4e2c238a78b6e9d6/belgium-flag_2x.png?q=40&h=600)

[Belgique](https://values.snap.com/fr-FR/privacy/transparency/belgium)

![](https://images.ctfassets.net/kw9k15zxztrs/1F6PhnmPP0wqDd7hcxZ9un/fcf0f104e4b8469fa02858ccabdb9915/brazil-flag_2x.png?q=40&h=600)

[Brésil](https://values.snap.com/fr-FR/privacy/transparency/brazil)

![](https://images.ctfassets.net/kw9k15zxztrs/6njYtHB6Xpu9BfHSpXj9eT/e6a577b8dccf6f6dfecaec81c3b36daf/canada-flag_2x.png?q=40&h=600)

[Canada](https://values.snap.com/fr-FR/privacy/transparency/canada)

![](https://images.ctfassets.net/kw9k15zxztrs/4DKKd5zsgsmduoP5nwZFSz/ea17b898d5558fe5e29dbaf6e43e47bc/denmark-flag_2x.png?q=40&h=600)

[Danemark](https://values.snap.com/fr-FR/privacy/transparency/denmark)

![](https://images.ctfassets.net/kw9k15zxztrs/4geuwNVpBiCpMNujEUwTPF/86a4183219c43e2a644b5f00a46aff2e/finland-flag_2x.png?q=40&h=600)

[Finlande](https://values.snap.com/fr-FR/privacy/transparency/finland)

![](https://images.ctfassets.net/kw9k15zxztrs/1PLFsp945ZXadhpp3F6DPs/c772736d78202bd0bcf5b4b652fbb1e0/france-flag_2x.png?q=40&h=600)

[France](https://values.snap.com/fr-FR/privacy/transparency/france)

![](https://images.ctfassets.net/kw9k15zxztrs/3fHPKQziHFfvnqTtpPTPNx/1fe033cad0d534d6fc7485db1ae3437c/germany-flag_2x.png?q=40&h=600)

[Allemagne](https://values.snap.com/fr-FR/privacy/transparency/germany)

![](https://images.ctfassets.net/kw9k15zxztrs/aNcH0hePnYpX4xT9HtaIG/8a61fb75da0187e745e132fa7daae809/india-flag_2x.png?q=40&h=600)

[Inde](https://values.snap.com/fr-FR/privacy/transparency/india)

![](https://images.ctfassets.net/kw9k15zxztrs/4QNr8hXIkhongycz4ybxkv/81b403f904238b8ecc3adad0db4cff13/iraq_flag.svg)

[Irak](https://values.snap.com/fr-FR/privacy/transparency/iraq)

![](https://images.ctfassets.net/kw9k15zxztrs/2Ygz6fhGjkvKARN4xGZcqz/e2d4ffdb937e8363a1adf5b7bb8eeab2/ireland-flag_2x.png?q=40&h=600)

[Irlande](https://values.snap.com/fr-FR/privacy/transparency/ireland)

![](https://images.ctfassets.net/kw9k15zxztrs/79T9KVPTxZhSpsmzTUwADD/5ffb49aa85cec11866a723dc1217dcd4/italy-flag_2x.png?q=40&h=600)

[Italie](https://values.snap.com/fr-FR/privacy/transparency/italy)

![](https://images.ctfassets.net/kw9k15zxztrs/4EPTaDoO3iloGkngFlgBW5/631ad608e91fbf3dbb000eb93e97c707/mexico-flag_2x.png?q=40&h=600)

[Mexique](https://values.snap.com/fr-FR/privacy/transparency/mexico)

![](https://images.ctfassets.net/kw9k15zxztrs/3VDqkn4I720nAGhKRSXM2I/d2c01cbd83727d74be7d9b3b73b08e93/netherlands-flag_2x.png?q=40&h=600)

[Pays-Bas](https://values.snap.com/fr-FR/privacy/transparency/netherlands)

![](https://images.ctfassets.net/kw9k15zxztrs/74ZAz5xg6Z9oqcSs690qzl/88944c98f2d1babb998e5fbfd4fe4a7a/new-zealand-flag_2x.png?q=40&h=600)

[Nouvelle-Zélande](https://values.snap.com/fr-FR/privacy/transparency/new-zealand)

![](https://images.ctfassets.net/kw9k15zxztrs/mUabWVwVVujlsd4Ral5ok/97c12905db97b5bf4c75b6edf7f35045/norway-flag_2x.png?q=40&h=600)

[Norvège](https://values.snap.com/fr-FR/privacy/transparency/norway)

![](https://images.ctfassets.net/kw9k15zxztrs/6KKQ0mjLHeVBHZ7NU5M60E/21ca516d1086bf98b3646252abe9d742/poland-flag_2x.png?q=40&h=600)

[Pologne](https://values.snap.com/fr-FR/privacy/transparency/poland)

![](https://images.ctfassets.net/kw9k15zxztrs/5FHQfykHMwFhQwX7tGgStD/c16adea0d546ed9109d28f4aaf955fb9/SA_flag_-_custom.svg)

[Arabie saoudite](https://values.snap.com/fr-FR/privacy/transparency/saudi-arabia)

![](https://images.ctfassets.net/kw9k15zxztrs/1hn0lYlh711oTzH6i7T8F9/25909b4a42cea43704e5fdc6d6aa13a5/spain-flag_2x.png?q=40&h=600)

[Espagne](https://values.snap.com/fr-FR/privacy/transparency/spain)

![](https://images.ctfassets.net/kw9k15zxztrs/4yY8kyanqJaJUA6jaGsCZJ/c48a17ef1fd43fc038df5de7470ccafb/sweden-flag_2x.png?q=40&h=600)

[Suède](https://values.snap.com/fr-FR/privacy/transparency/sweden)

![](https://images.ctfassets.net/kw9k15zxztrs/123PlPG4k7wY5yIrzTy3Mp/af51b42180cf158f18ca0e6d874baab6/turkey-flag_2x.png?q=40&h=600)

[Turquie](https://values.snap.com/fr-FR/privacy/transparency/turkey)

![](https://images.ctfassets.net/kw9k15zxztrs/56UTZPbdL5kMCCCnjXlkhQ/69a612ce6ce8ad033ad5ab2b9c6b0a08/united-arab-emirates-flag_2x.png?q=40&h=600)

[Émirats Arabes Unis](https://values.snap.com/fr-FR/privacy/transparency/united-arab-emirates)

![](https://images.ctfassets.net/kw9k15zxztrs/SxhOHVyKIrWoq98RwRQq2/a98c8f9bbdbc1004173bbddf0be04e1e/united-kingdom-flag_2x.png?q=40&h=600)

[Royaume-Uni](https://values.snap.com/fr-FR/privacy/transparency/united-kingdom)

![](https://images.ctfassets.net/kw9k15zxztrs/1K5p7n7IpFyay5LcBepDIq/643797e9f2e5fa43211830c4ecf12933/united-states-of-america-flag_2x.png?q=40&h=600)

[États-Unis](https://values.snap.com/fr-FR/privacy/transparency/united-states)

![](https://images.ctfassets.net/kw9k15zxztrs/bhkK1OpEAvkunhgXq15sd/ff2d9f470cd6ef32e1063f4694d13470/shield-link.png?q=40&h=400)

Demandes de suppression pour le gouvernement et la propriété intellectuelle

[En savoir plus](https://values.snap.com/fr-FR/privacy/transparency/legal-requests)

![](https://images.ctfassets.net/kw9k15zxztrs/2QsppMRcTB0c9QSx8LSQBQ/5f5525824215e413122e414cd7ccb740/pages-link.png?q=40&h=400)

À propos des rapports de transparence

[En savoir plus](https://values.snap.com/fr-FR/privacy/transparency/about)

![](https://images.ctfassets.net/kw9k15zxztrs/4dpR6O8HcB7WBS5gJbRTn7/1e5b4007fa67a36ed1979ae7e4e7f261/book-links.png?q=40&h=400)

Glossary of Transparency Report

[En savoir plus](https://values.snap.com/fr-FR/privacy/transparency/glossary)