# Resource URL: https://developers.facebook.com/docs/fort-pages-api/reference
Reference
=========

Methods
-------

| Method | Description |
| --- | --- |
| [`get_engagement_counts()`](https://developers.facebook.com/docs/fort-pages-api/reference/get_engagement_counts) | Defines a page post engagement count search query and returns a Search Generator that can execute the query. |
| [`get_follower_counts()`](https://developers.facebook.com/docs/fort-pages-api/reference/get_follower_counts) | Defines a page follower count search query and returns a Search Generator that can execute the query. |
| [`get_post_counts()`](https://developers.facebook.com/docs/fort-pages-api/reference/get_post_counts) | Defines a page admin post count search query and returns a Search Generator that can execute the query. |
| [`search_pages()`](https://developers.facebook.com/docs/fort-pages-api/reference/search_pages) | Defines a Page search query and returns a Search Generator that can execute the query. |
| [`search_posts()`](https://developers.facebook.com/docs/fort-pages-api/reference/search_posts) | Defines a Post search query and returns a Search Generator that can execute the query. |

Resources
---------

| Node | Description |
| --- | --- |
| [Page](https://developers.facebook.com/docs/fort-pages-api/reference/page) | Represents a public Page. |
| [Post](https://developers.facebook.com/docs/fort-pages-api/reference/post) | Represents a public Post on a public Page. |

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)

# Resource URL: https://developers.facebook.com/docs/fort-pages-api/reference/page
Page Node
=========

Represents a public Page.

Fields
------

You can read the following Fields on a Page node. Fields that return other Nodes support [Field Expansion](https://developers.facebook.com/docs/fort-pages-api/guides/field-expansion); readable Fields on other Nodes are displayed below as sub-bullets.

* `about`
    
* `id`
    
* `category`
    
* `category_list`
    
    * `id`
        
    * `name`
        
    
* `description`
    
* `engagement`
    
    * `count`
        
    * `social_sentence`
        
    
* `fort_page_transparency`
    
    * `admin_countries`
        
        * `country`
            
        * `count`
            
        
    * `name_changes`
        
        * `date`
            
        * `old_name`
            
        * `new_name`
            
        
    * `page_created`
        
    * `page_merges`
        
        * `date`
            
        * `page_merged`
            
        
    * `confirmed_page_owner`
        
    
* `link`
    
* `location`
    
    * `street`
        
    * `city`
        
    * `state`
        
    * `country`
        
    * `zip`
        
    * `latitude`
        
    * `longitude`
        
    * `name`
        
    * `located_in`
        
    
* `name`
    
* `page_about_story`
    
    * `cover_photo`
        
        * `id`
            
        * `link`
            
        
    * `title`
        
    * `composed_text`
        
        * `text`
            
        
    
* `picture`
    
    * `url`
        
    * `height`
        
    * `width`
        
    
* `username`
    
* `verification_status`
    
* `website`
    

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)

# Resource URL: https://developers.facebook.com/docs/fort-pages-api/reference/post
Post Node
=========

Represents a public Post on a public Page.

Fields
------

You can read the following Fields on a Post Node. Fields that return other Nodes support [Field Expansion](https://developers.facebook.com/docs/fort-pages-api/guides/field-expansion); readable Fields on other Nodes are displayed below as sub-bullets.

* `creation_time`
    
* `entities`
    
    * `hashtags`
        
    
* `id`
    
* `lang`
    
* `modified_time`
    
* `page`
    
    * `id`
        
    * `name`
        
    * `username`
        
    * `about`
        
    * `website`
        
    * `description`
        
    * `page_about_story`
        
        * `cover_photo`
            
            * `id`
                
            * `link`
                
            
        * `title`
            
        * `composed_text`
            
            * `text`
                
            
        
    * `verification_status`
        
    * `picture`
        
        * `url`
            
        * `height`
            
        * `width`
            
        
    * `category`
        
    * `category_list`
        
        * `id`
            
        * `name`
            
        
    * `engagement`
        
        * `count`
            
        * `social_sentence`
            
        
    * `location`
        
        * `street`
            
        * `city`
            
        * `state`
            
        * `country`
            
        * `zip`
            
        * `latitude`
            
        * `longitude`
            
        * `name`
            
        * `located_in`
            
        
    * `link`
        
    * `fort_page_transparency`
        
        * `page_created`
            
        * `name_changes`
            
        * `page_merges`
            
        * `page_merges`
            
        * `admin_countries`
            
        * `confirmed_page_owner`
            
        
    
* `post_url`
    
* `statistics`
    
    * `like_count`
        
    * `love_count`
        
    * `wow_count`
        
    * `haha_count`
        
    * `sad_count`
        
    * `angry_count`
        
    * `share_count`
        
    * `comment_count`
        
    * `reaction_count`
        
    
* `text`
    

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)

# Resource URL: https://developers.facebook.com/docs/fort-pages-api/reference/get_engagement_counts
get\_engagement\_counts()
=========================

Defines a page post engagement count search query and returns a [Search Generator](https://developers.facebook.com/docs/fort-pages-api/guides/pagination#search-generator) that can execute the query.

Syntax
------

engagement\_counts = search\_client.get\_engagement\_counts(
    page\_ids=PAGE\_IDS,
    since=SINCE,
    until=UNTIL
)

Arguments
---------

| Placeholder | Value Type | Value Description | Sample Value |
| --- | --- | --- | --- |
| `PAGE_IDS`  <br>_Required_ | List | Iterable list of Page IDs for which you want post engagement count analytics. | `[196802957523030,101505831531203]` |
| `SINCE` | Integer or string | Unix timestamp or ISO 8601 formatted date indicating the beginning of an aggregation window for which you want post engagement count analytics.<br><br>  <br><br>Paired with `until` to define an aggregation window. If `until` is omitted, the aggregation window will be 30 days, starting from `since` value and going forward in time.<br><br>  <br><br>Inclusive of specified timestamp or date.<br><br>  <br><br>**Engagements created within the defined aggregation window are included even if they were later removed.** | `1584835200` or `"2020-03-22"` |
| `UNTIL` | Integer or string | Unix timestamp or ISO 8601 formatted date indicating the end of an aggregation window for which you want post engagment count analytics.<br><br>  <br><br>Paired with `since` to define an aggregation window. If `since` is omitted, the time period will be 30 days, starting from until and going backward in time.<br><br>  <br><br>Inclusive of specified timestamp or date.<br><br>  <br><br>**Engagements created within the defined aggregation window are included even if they were later removed.** | `1590105600` or `"2020-05-22"` |

Reponse
-------

Returns an array of page post engagement count data for each page defined by the `page_ids` argument.

| Property | Description |
| --- | --- |
| `page_id` | Page ID. |
| `timestamp` | Unix timestamp representing the start of the aggregation window indicated by the `since` argument. |
| `engagement_count` | Count of engagements within the aggregation window on all of the page's posts. Engagements can be comments, likes, shares, or clicks. **Engagements created within the defined aggregation window are included even if they were later removed.** |

Sample Call
-----------

from fbri.common.search\_client import SearchClient
search\_client = SearchClient.get\_instance()

engagement\_counts = search\_client.get\_engagement\_counts(
    page\_ids=\[196802957523030,101505831531203\],
    since="2020-03-22",
    until="2020-05-22"
)


result = next(engagement\_counts)
display(result.head())

Sample Resonse
--------------

![](https://scontent-cdg4-1.xx.fbcdn.net/v/t39.2365-6/157897342_488808138955002_2258503728019650303_n.png?_nc_cat=110&ccb=1-7&_nc_sid=e280be&_nc_ohc=VzU6v-0TonoAX8RX-YO&_nc_ht=scontent-cdg4-1.xx&oh=00_AfC3lCRYkTqnUwWspztGkc12cbq6pgQTxpndZurFM-p1WQ&oe=65D44030)

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)

# Resource URL: https://developers.facebook.com/docs/fort-pages-api/reference/get_follower_counts
get\_follower\_counts()
=======================

Defines a page follower count search query and returns a [Search Generator](https://developers.facebook.com/docs/fort-pages-api/guides/pagination#search-generator) that can execute the query.

Syntax
------

follower\_counts = search\_client.get\_follower\_counts(
    page\_ids=PAGE\_IDS,
    since=SINCE,
    until=UNTIL
)

Arguments
---------

| Placeholder | Value Type | Value Description | Sample Value |
| --- | --- | --- | --- |
| `PAGE_IDS`  <br>_Required_ | List | Iterable list of Page IDs for which you want page follower count analytics. | `[196802957523030,101505831531203]` |
| `SINCE` | Integer or string | Unix timestamp or ISO 8601 formatted date indicating the beginning of an aggregation window for which you want page follower counts analytics.<br><br>  <br><br>Paired with `until` to define an aggregation window. If `until` is omitted, the aggregation window will be 30 days, starting from `since` value and going forward in time.<br><br>  <br><br>Inclusive of specified timestamp or date.<br><br>  <br><br>**Users who followed the page(s) within the defined aggregation window are included even if they later unfollowed the page(s).** | `1584835200` or `"2020-03-22"` |
| `UNTIL` | Integer or string | Unix timestamp or ISO 8601 formatted date indicating the end of an aggregation window for which you want page follower count analytics.<br><br>  <br><br>Paired with `since` to define an aggregation window. If `since` is omitted, the time period will be 30 days, starting from until and going backward in time.<br><br>  <br><br>Inclusive of specified timestamp or date.<br><br>  <br><br>**Users who followed the page(s) within the defined aggregation window are included even if they later unfollowed the page(s).** | `1590105600` or `"2020-05-22"` |

Response
--------

Returns an array of page follower count data for each page defined by the `page_ids` argument.

| Property | Description |
| --- | --- |
| `page_id` | Page ID. |
| `timestamp` | Unix timestamp representing the start of the aggregation window indicated by the `since` argument. |
| `country` | ISO 3166-1 formatted country code for which this set of page follower counts applies. |
| `follower_count` | Follower count for this country, at the start of the aggregation window. **Users who followed the page(s) within the defined aggregation window are included even if they later unfollowed the page(s).**<br><br>Data points with **`follower_count`** of less than 100 are removed to preserve privacy. |

Sample Call
-----------

from fbri.common.search\_client import SearchClient
search\_client = SearchClient.get\_instance()

follower\_counts = search\_client.get\_follower\_counts(
    page\_ids=\[196802957523030,101505831531203\],
    since="2022-03-06",
    until="2022-05-01"
)

result = next(follower\_counts)
display(result.head())

Sample Response
---------------

![](https://scontent-cdg4-1.xx.fbcdn.net/v/t39.8562-6/277755629_367268211952613_3095621605788584331_n.png?_nc_cat=108&ccb=1-7&_nc_sid=f537c7&_nc_ohc=y7JNF3-vf0UAX8suIOJ&_nc_ht=scontent-cdg4-1.xx&oh=00_AfDDp59L-yfxIpocCVHedZj9fjuvr3oUobGOzAoLtFCURQ&oe=65BFEE07)

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)

# Resource URL: https://developers.facebook.com/docs/fort-pages-api/reference/get_post_counts
get\_post\_counts()
===================

Defines a page admin post count search query and returns a [Search Generator](https://developers.facebook.com/docs/fort-pages-api/guides/pagination#search-generator) that can execute the query.

Syntax
------

get\_post\_counts(
    page\_ids=PAGE\_IDS,
    since=SINCE,
    until=UNTIL
)

Arguments
---------

| Placeholder | Value Type | Value Description | Sample Value |
| --- | --- | --- | --- |
| `PAGE_IDS`  <br>_Required_ | List | Iterable list of Page IDs for which you want page admin post counts analytics. | `[196802957523030,101505831531203]` |
| `SINCE` | Integer or string | Unix timestamp or ISO 8601 formatted date indicating the beginning of an aggregation window for which you want page admin post counts analytics.<br><br>  <br><br>Paired with `until` to define an aggregation window. If `until` is omitted, the aggregation window will be 30 days, starting from `since` value and going forward in time.<br><br>  <br><br>Inclusive of specified timestamp or date.<br><br>  <br><br>**Posts created by admins within the defined aggregation window are included even if they were later deleted.** | `1584835200` or `"2020-03-22"` |
| `UNTIL` | Integer or string | Unix timestamp or ISO 8601 formatted date indicating the end of an aggregation window for which you want page admin post count analytics.<br><br>  <br><br>Paired with `since` to define an aggregation window. If `since` is omitted, the time period will be 30 days, starting from until and going backward in time.<br><br>  <br><br>Inclusive of specified timestamp or date.<br><br>  <br><br>**Posts created by admins within the defined aggregation window are included even if they were later deleted.** | `1590105600` or `"2020-05-22"` |

Reponse
-------

Executing the query returns an array of page admin post count data for each page defined by the `page_ids` argument.

| Property | Description |
| --- | --- |
| `page_id` | Page ID. |
| `timestamp` | Unix timestamp representing the start of the aggregation window indicated by the `since` argument. |
| `post_count` | Count of posts by page admins published within the aggregation window. Posts can be text, photos, videos, or shares of other posts. **Posts created by admins within the defined aggregation window are included even if they were later deleted.** |

Sample Call
-----------

from fbri.common.search\_client import SearchClient
search\_client = SearchClient.get\_instance()

post\_counts = search\_client.get\_post\_counts(
    page\_ids=\[196802957523030,101505831531203\],
    since="2020-03-22",
    until="2020-05-22"
)

result = next(post\_counts)
display(result.head())

Sample Resonse
--------------

![](https://scontent-cdg4-2.xx.fbcdn.net/v/t39.2365-6/157802375_230243335462125_7685645958676847515_n.png?_nc_cat=107&ccb=1-7&_nc_sid=e280be&_nc_ohc=n8h59ofdzesAX80y4lx&_nc_ht=scontent-cdg4-2.xx&oh=00_AfAIhl-pNG86PTUBoANnn5QzC2tff7SzUkrD4k9SRCxjiw&oe=65D45196)

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)

# Resource URL: https://developers.facebook.com/docs/fort-pages-api/reference/search_pages
search\_pages()
===============

Defines a [Page](https://developers.facebook.com/docs/fort-pages-api/reference/page) search query and returns a [Search Generator](https://developers.facebook.com/docs/fort-pages-api/guides/pagination#search-generator) that can execute the query.

Syntax
------

search\_pages(
    data\_type=DATA\_TYPE,
    fields=FIELDS,
    limit=LIMIT,
    page\_ids=PAGE\_IDS,
    q=Q
)

Arguments
---------

| Placeholder | Value Type | Description |
| --- | --- | --- |
| `DATA_TYPE`  <br>_Optional_ | String | Indicates how the query search results should be [formatted](https://developers.facebook.com/docs/fort-pages-api/guides/response-formats), as either a Pandas DataFrame or using JSON. Value can be either `dataframe` or `json`. Defaults to `dataframe` if omitted. |
| `FIELDS`  <br>_Optional_ | String | A comma-separated list of [Page Fields](https://developers.facebook.com/docs/fort-pages-api/reference/page#fields) you want included in search results. |
| `LIMIT`  <br>_Optional_ | Integer | The number of Pages you want returned in each page of search results. If omitted, default to `10`. Maximum value is `500`. Refer to our [Pagination](https://developers.facebook.com/docs/fort-pages-api/guides/pagination) guide for additional information. |
| `PAGE_IDS`  <br>_Optional_ | List | An iterable list of Page IDs as strings or integers. Limits the query to Pages in the array. Maximum of 500 Page IDs. |
| `Q`  <br>_Optional_ | String | A hashtag, keyword, or phrase to search for. Searches a Page's `about`, `description`, and `page_about_story` Fields. |

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)

# Resource URL: https://developers.facebook.com/docs/fort-pages-api/reference/search_posts
search\_posts()
===============

Defines a [Post](https://developers.facebook.com/docs/fort-pages-api/reference/post) search query and returns a [Search Generator](https://developers.facebook.com/docs/fort-pages-api/guides/pagination#search-generator) that can execute the query.

Syntax
------

search\_pages(
    data\_type=DATA\_TYPE,
    fields=FIELDS,
    lang=LANG,
    limit=LIMIT,
    page\_ids=PAGE\_IDS,
    post\_ids=POST\_IDS,
    q=Q,
    since=SINCE,
    until=UNTIL
)

Arguments
---------

| Placeholder | Value Type | Description |
| --- | --- | --- |
| **`DATA_TYPE`**  <br>_Optional_ | String | Indicates how the query search results should be [formatted](https://developers.facebook.com/docs/fort-pages-api/guides/response-formats), as either a Pandas DataFrame or using JSON. Value can be either **`dataframe`** or **`json`**. Defaults to **`dataframe`** if omitted. |
| **`FIELDS`**  <br>_Optional_ | String | A comma-separated list of [Post Fields](https://developers.facebook.com/docs/fort-pages-api/reference/post#fields) you want included in search results. |
| **`LANG`**  <br>_Optional_ | String | Only return Post created in a specified language. Value must be an [ISO 639-1](https://l.facebook.com/l.php?u=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FList_of_ISO_639-1_codes&h=AT3LsDlsvBEvfoZP02XBoPYaCYMlL8-9vvyUfDgOGLKoAdafjqmAx8L0dzvC-Ih7CidYrAWtDgSknLFVD9t6CS-rx7VTOxiUxTe7n9YsTF90AwyL9oPX2Q1tWJpEH05BT5SZE_RxRsSCfIlg) lowercase two-letter code. |
| **`LIMIT`**  <br>_Optional_ | Integer | The number of Posts you want returned in each page of search results. If omitted, defaults to **`10`**. Maximum value is **`100`**. Refer to our [Pagination](https://developers.facebook.com/docs/fort-pages-api/guides/pagination) guide for additional information. |
| **`PAGE_IDS`**  <br>_Optional_ | List | An iterable list of Page IDs, as strings or integers. Limits the query to Pages in the array. Maximum of 100 Page IDs. |
| **`POST_IDS`**  <br>_Optional_ | List | An iterable list of Post IDs, as strings or integers. Limits the query to Posts in the array. Maximum of 100 Post IDs. |
| **`Q`**  <br>_Required if both **`page_ids`** and **`post_ids`** are omitted._ | String | A hashtag, keyword, or phrase to search for in a Post's **`text`** field. |
| **`SINCE`**  <br>_Optional_ | Integer | A UNIX timestamp or date formatted as YYYY-MM-DD indicating the start of a time range. Used with **`until`** to define a range. Only Posts created within the range will be returned, inclusive of specified date or time. If the **`q`** argument is included without **`page_ids`** and **`post_ids`**, only returns Posts created in the last 30 days. |
| **`UNTIL`**  <br>_Optional_ | Integer | A UNIX timestamp or date formatted as YYYY-MM-DD indicating the end of a time range. Used with **`since`** to define a range. Only Posts created within the range will be returned. Inclusive of specified date or time. If the **`q`** argument is included without **`page_ids`** and **`post_ids`**, only returns Posts created in the last 30 days. |

![](https://www.facebook.com/tr?id=675141479195042&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=574561515946252&ev=PageView&noscript=1)

![](https://www.facebook.com/tr?id=1754628768090156&ev=PageView&noscript=1)