# Resource URL: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2023-3/
Application des Règles communautaires

_1er juillet 2023 – 30 septembre 2023_

_Date de publication: 13 décembre 2023_

### À propos de ce rapport

Chez TikTok, notre mission est d’inspirer la créativité et de divertir. Nous donnons la priorité à la sécurité, au bien-être et à l’intégrité afin que notre communauté puisse se sentir libre de créer, de se connecter les uns les autres et de se divertir.

Plus de 40 000 professionnels en charge de la sécurité, travaillent avec des technologies innovantes afin de maintenir et faire respecter nos [Règles Communautaires](https://www.tiktok.com/community-guidelines/en/), [Conditions Générales](https://www.tiktok.com/legal/page/eea/terms-of-service/en) et [Politiques Publicitaires](https://ads.tiktok.com/help/article/tiktok-advertising-policies-ad-creatives-landing-page-prohibited-content?lang=en), qui s’appliquent à tous les contenus de notre plateforme. Ce dernier rapport donne un aperçu de ces efforts et montre comment nous continuons à maintenir la confiance, l’authenticité et la responsabilité.

Dans le cadre de nos efforts continus pour faciliter l’analyse de la plateforme TikTok, nous fournissons désormais des données supplémentaires concernant la suppression de données par catégorie de politique pour les 50 marchés ayant les volumes les plus élevés de contenu supprimé (cf. Fichier en téléchargement ci-dessous). Ces marchés représentent environ 90 % de toutes les suppressions de contenu pour ce trimestre.

Au cours de la période considérée, nous avons élargi l’éligibilité à notre [API de recherche](https://developers.tiktok.com/products/research-api/) aux chercheurs universitaires en Europe, afin de soutenir la recherche sur des sujets liés aux tendances de consommation, à la désinformation, à la santé mentale, etc. Nous avons également lancé une nouvelle [API de contenu commercial](https://developers.tiktok.com/products/commercial-content-api) et ouvert notre [Bibliothèque de contenu commercial](https://library.tiktok.com/) en Europe pour apporter une plus grande transparence à la publicité payante et aux autres contenus commerciaux sur TikTok.

### Données les plus récentes

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Télécharger](https://sf16-va.tiktokcdn.com/obj/eden-va2/uhsllrta/Q3%20CGER/FRENCH.zip)

**Sécurité**
------------

Dans la continuité de l’engagement pris dans le dernier rapport, nous publions notre deuxième mise à jour trimestrielle sur la mise en œuvre de TikTok LIVE. TikTok LIVE continue d’être une partie essentielle de l’expérience TikTok, favorisant le divertissement en temps réel qui connecte les créateurs à leurs communautés. Malgré l’augmentation du nombre de diffusons en direct suspendues ce trimestre, la proportion du nombre total de diffusions en direct suspendues est restée stable à 1,5 %, ce qui indique que nos mesures de contrôle s’adaptent à la croissance de la plateforme.

#### **Suppressions de vidéos et suspensions LIVE**

#### **Total des suppressions et des rétablissements, par type de contenu**

#### **Taux de suppression et délais de réponse aux signalements**

#### **Politique en matière de suppression**

#### **Modération par marché et par langue**

#### **Opérations d’influence cachées**

Comptes ciblant la guerre entre la Russie et l’Ukraine

Ce réseau semblait opérer depuis la Russie et cibler une audience européenne, dont l’Allemagne, l’Italie et l’Ukraine. Les individus derrière ce réseau ont créé un grand nombre de faux comptes pour amplifier artificiellement des opinions pro-russes sur la politique étrangère en Europe dans le contexte de la guerre entre la Russie et l’Ukraine.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne/Externe | 12,820 | 847,760 |

Comptes ciblant le discours politique au Cambodge

Ce réseau semblait opérer depuis le Cambodge et cibler une audience cambodgienne. Les individus derrière ce réseau ont créé de faux comptes et hyperposté du contenu afin d’amplifier artificiellement des récits spécifiques favorables au Parti du peuple cambodgien, en ciblant le discours sur les élections au Cambodge.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 3,046 | 1,930,179 |

Comptes ciblant la guerre entre la Russie et l’Ukraine

Ce réseau semblait opérer depuis l’Ukraine et cibler une audience ukrainienne. Les individus derrière ce réseau ont créé de faux comptes et hyperposté du contenu à grande échelle, en ukrainien, amplifiant artificiellement des récits visant à collecter des fonds pour l’armée ukrainienne.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 2,350 | 81,570 |

Comptes ciblant le discours politique en Indonésie

Ce réseau semblait opérer depuis l’Indonésie et cibler une audience indonésienne. Les individus derrière ce réseau ont créé de faux comptes et hyperposté du contenu à grande échelle, en amplifiant artificiellement les récits favorables à un candidat présidentiel spécifique, en ciblant le discours des élections indonésiennes.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 706 | 10,699 |

Comptes ciblant le discours politique en Chine

Ce réseau semblait opérer depuis la Chine et cibler une audience mondiale, dont les États-Unis et le Japon. Les individus derrière ce réseau ont créé de faux comptes et émis des publications sur de multiples plateformes en ligne afin d’amplifier artificiellement des récits favorables à la Chine.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 636 | 29,757 |

Comptes ciblant le discours politique en Thaïlande

Ce réseau semblait opérer depuis la Thaïlande et cibler une audience thaïlandaise. Les individus derrière ce réseau ont créé de faux comptes et hyperposté du contenu à grande échelle, afin d’amplifier artificiellement des récits spécifiques favorables à un candidat royaliste thaïlandais, en ciblant le discours sur les élections en Thaïlande.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 457 | 4,069 |

Comptes ciblant le discours politique en Thaïlande

Ce réseau semblait opérer depuis la Thaïlande et cibler une audience thaïlandaise. Les individus derrière ce réseau ont créé de faux comptes et hyperposté du contenu à grande échelle, afin d’amplifier artificiellement des récits spécifiques favorables au parti politique « Change » thaïlandais, en ciblant le discours sur les élections en Thaïlande.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 377 | 741,225 |

Comptes ciblant la guerre entre la Russie et l’Ukraine

Ce réseau semblait opérer depuis l’Ukraine et cibler une audience ukrainienne. Les individus derrière ce réseau ont créé de faux comptes afin d’amplifier des vidéos de désinformation, ciblant le discours sur la guerre en cours entre la Russie et l’Ukraine.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 302 | 57,806 |

Comptes ciblant le discours politique en Iraq

Ce réseau semblait opérer depuis l’Iraq et cibler une audience iraquienne. Les individus derrière ce réseau ont créé de faux comptes afin de promouvoir secrètement les objectifs militaires iraniens au Moyen-Orient.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 229 | 242,409 |

Comptes ciblant le discours politique en Équateur

Ce réseau semblait opérer depuis l’Équateur et cibler une audience équatorienne. Les individus derrière ce réseau ont créé de faux comptes pour amplifier les récits critiques à l’égard d’un mouvement politique de gauche.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 143 | 12,353 |

Comptes ciblant la guerre entre la Russie et l’Ukraine

Ce réseau semblait opérer à partir de la Russie et ciblait les audiences russe et ukrainienne, ainsi que l’audience russophone en Europe. Les individus derrière ce réseau ont créé de faux comptes afin d’amplifier artificiellement des discours pro-russes, ciblant le discours sur la guerre en cours entre la Russie et l’Ukraine.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 133 | 199,569 |

Comptes ciblant le discours politique en Serbie

Ce réseau semblait opérer depuis la Serbie et cibler une audience serbe. Les individus derrière ce réseau ont créé de faux comptes et hyperposté du contenu à grande échelle, afin d’amplifier les récits favorables au Parti progressiste serbe, en ciblant le discours des élections serbes.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 77  | 5,009 |

Comptes ciblant les relations étrangères de la Slovaquie avec la Russie

Ce réseau semblait opérer depuis la Slovaquie et cibler une audience slovaque. Les individus derrière ce réseau ont créé des comptes opaques qui utilisent de fausses méthodes de création d’audience afin d’amplifier de manière inauthentique des contenus anti-UE, anti-OTAN et pro-russes en Slovaquie.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 69  | 2,154 |

Comptes ciblant le discours politique en Malaisie

Ce réseau semblait opérer depuis la Malaisie et cibler une audience malaisienne. Les individus derrière ce réseau ont créé de faux comptes et hyperposté du contenu à grande échelle, afin d’amplifier les récits critiques à l’égard des partis politiques Barisan Nasional, United Malays National Organization et Pakatan Harapan, et favorables au Perikatan Nasional, en ciblant le discours sur les élections en Malaisie.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 53  | 174,472 |

Comptes ciblant le discours politique en Serbie

Ce réseau semblait opérer depuis la Serbie et cibler une audience serbe. Les individus derrière ce réseau ont créé de faux comptes et hyperposté du contenu à grande échelle, afin d’amplifier artificiellement les récits critiques à l’égard de l’opposition du Parti progressiste serbe et à l’égard des médias indépendants.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 38  | 37,361 |

Comptes ciblant la guerre entre la Russie et l’Ukraine

Ce réseau semblait opérer depuis la Russie et cibler les utilisateurs d’Allemagne, d’Italie, de Turquie, de Serbie, de Tchéquie, de Pologne et de Grèce en utilisant les langues locales. Les individus derrière ce réseau ont créé de faux comptes, y compris de fausses nouvelles agences, afin d’amplifier artificiellement des récits pro-russes, ciblant le discours sur la guerre en cours entre la Russie et l’Ukraine.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes du réseau**** | ****Abonnés du réseau**** |
| Interne | 19  | 217,008 |

REMARQUE : Le retrait d’une opération d’influence cachée nécessite des opérations à plusieurs niveaux, comprenant l’enquête, la suppression et l’analyse à l’issue de l’intervention. Nous indiquons l’intervention sur ces réseaux au cours du trimestre où le processus complet des opérations a été achevé.

**Définitions**

* **Les réseaux ont opéré depuis :** indique l’emplacement géographique des opérations du réseau selon des preuves techniques et comportementales récoltées depuis des sources privées et ouvertes. Il est possible que TikTok ne soit pas en mesure de relier ces réseaux à des entités, des individus ou des groupes définis.
* **Source de la détection :** considérée comme interne quand la présence de l’activité a été identifiée uniquement via une enquête menée en interne. La détection externe renvoie à des enquêtes qui ont débuté avec un rapport externe, ayant ensuite entraîné une enquête.
* **Abonnés du réseau :** nombre total cumulé des comptes abonnés aux comptes du réseau au moment de la suppression du réseau.

* * *

**Sûreté**
----------

La protection de notre communauté reste notre priorité. Nous nous engageons à investir dans des mécanismes solides qui donnent la priorité à la sécurité des utilisateurs, à la protection des données et au respect de la réglementation. La transparence sur ces efforts de sauvegarde de la plateforme est un élément essentiel de la pérennité de cet engagement. Nos séries [TikTok Facts](https://newsroom.tiktok.com/en-us/tiktok-truths-a-new-series-on-our-privacy-and-data-security-practices) ont pour but de clarifier nos pratiques en matière de protection de la vie privée et de sécurité des données. Nous avons récemment [approfondi](https://newsroom.tiktok.com/en-us/tiktok-facts-how-we-secure-personal-information-and-store-data) notre approche du stockage des données et la manière dont nous sécurisons les informations personnelles de notre communauté, et nous continuerons à partager des ressources avec notre communauté pour refléter les informations importantes sur la sécurité et la confidentialité chez TikTok.

Il s’agit notamment de mises à jour sur des initiatives clés dans différentes régions du monde. Bien que TikTok soit une plateforme mondiale, nous adoptons une approche locale de la conformité réglementaire en collaborant avec les parties prenantes afin de mieux comprendre les préoccupations locales et respecter nos obligations légales. Nous continuons à progresser sur le [projet Clover](https://newsroom.tiktok.com/en-eu/setting-a-new-standard-in-european-data-security-with-project-clover), une initiative annoncée début 2023 pour créer un environnement de protection spécialement conçu pour les données des utilisateurs européens de TikTok. Nous avons commencé à migrer les données des utilisateurs européens vers notre premier centre de données à Dublin, en Irlande, et d’autres centres de données en Irlande et en Norvège sont en cours de construction. Nous sommes également heureux d’annoncer notre collaboration avec des leaders du secteur pour tester nos défenses et protéger notre plateforme. Nous avons récemment annoncé un partenariat avec NCC Group, la société de sécurité européenne tierce qui effectuera un audit indépendant des contrôles et protections des données du projet Clover, surveillera les flux de données, fournira une vérification indépendante et signalera tout incident.

Conformément à notre engagement en matière de sécurité, nous avons célébré le troisième anniversaire de notre programme mondial de primes à la détection de “bugs” avec HackerOne. Ce programme encourage les chercheurs en sécurité à signaler de manière proactive les failles de sécurité afin que nous puissions les corriger. Au troisième trimestre 2023, notre [programme HackerOne](https://hackerone.com/tiktok?type=team) a trié et résolu 59 rapports, représentant plus de 112 000$ de récompenses.

Comme toujours, nous restons vigilants dans nos efforts pour détecter les menaces extérieures et protéger la plateforme contre la présence de comptes et d’engagements inauthentiques ou faux. Ces menaces attaquent nos systèmes et les mettent à l’épreuve de manière continue, ce qui peut entraîner des fluctuations ponctuelles dans les indicateurs rapportés dans ces domaines. Néanmoins, nous restons déterminés à identifier et à supprimer rapidement tout compte, contenu ou activité visant à accroître artificiellement la popularité sur notre plateforme. Dans ce contexte de menaces en constante évolution, TikTok s’engage à préserver l’intégrité de sa communauté et à renforcer notre réponse aux différents acteurs.

#### **Spam et faux engagement**

* * *

**Contenus publicitaires**
--------------------------

Les comptes d’annonceurs et le contenu des publicités doivent être conformes à nos Règles Communautaires, [Politiques Publicitaires](https://ads.tiktok.com/help/article/tiktok-advertising-policies-industry-entry) et nos Conditions d’utilisation. TikTok a mis en place des politiques strictes pour protéger sa communauté des contenus publicitaires faux, frauduleux ou trompeurs. Garantir la sécurité des utilisateurs et des annonceurs est un engagement permanent. Au cours du troisième trimestre de 2023, nous avons constaté une augmentation de contenus publicitaires retirés pour non-respect de nos Politiques Publicitaires et une diminution du nombre de contenus publicitaires supprimés en raison d’actions au niveau du compte. Nous revoyons et renforçons continuellement nos systèmes afin d’identifier de nouveaux modèles et de supprimer rapidement et avec précision les contenus publicitaires qui enfreignent nos politiques. En appliquant des politiques strictes, en tirant parti de mécanismes de détection avancés et en améliorant continuellement nos systèmes, nous nous efforçons de favoriser une expérience publicitaire sûre, agréable et conforme aux valeurs de notre passionnante communauté TikTok.

#### **Application des règles concernant les contenus publicitaires**

### **Autres rapports**

* [Rapport sur les demandes de retrait émanant de gouvernements](https://www.tiktok.com/transparency/fr-fr/government-removal-requests)
    
* [Rapport de demandes de retrait de contenus pour infractions aux lois sur la propriété intellectuelle](https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests-2021-1/)
    
* [Rapport sur les demandes de renseignements](https://www.tiktok.com/transparency/fr-fr/information-requests-2021-1/)
    

#### Cela a-t-il été utile ?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Oui![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)Non

# Resource URL: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2023-2/
Application des Règles communautaires

_1e avril 2023 – 30 juin 2023_

_publié le 12 octobre 2023_

### À propos de ce rapport

Chez TikTok, notre mission est d’inspirer la créativité et de divertir. La sécurité et le bien-être de notre communauté sont notre priorité, et nous avons plus de 40 000 professionnels de la sécurité qui travaillent pour protéger nos utilisateurs.

Ce rapport reflète la mise à jour de nos règles communautaires, qui sont entrées en vigueur en avril. Elles offrent à notre communauté une plus grande transparence sur nos règles et la manière dont nous les appliquons. Après avoir consulté plus de 100 organisations dans le monde, nous avons revu la manière dont nous organisons nos politiques, simplifié le langage que nous utilisons et ajouté de la granularité pour aider, notamment les créateurs ou bien encore les chercheurs, à accéder facilement aux informations dont ils ont besoin. Nous avons également rafraîchi plusieurs visualisations de données pour les rendre plus lisibles et compréhensibles, y compris pour les personnes qui souffrent d’une déficience de la vision des couleurs.

Ces mises à jour importante soulignent notre engagement à respecter les valeurs de confiance, d’authenticité et de responsabilité qui sont au cœur de notre communauté TikTok.

* * *

### Données les plus récentes

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Télécharger](https://sf16-va.tiktokcdn.com/obj/eden-va2/uhsllrta/Q2%20CGER/2023Q2_v2_raw_data_cger_French.csv)

### **Sécurité**

[Les informations présentées dans ce rapport reflètent nos](https://newsroom.tiktok.com/en-us/community-guidelines-update)[règles communautaires actualisées](https://www.tiktok.com/community-guidelines/en/), la [sécurité et le bien-être des jeunes](https://www.tiktok.com/community-guidelines/en/youth-safety/?cgversion=2023) demeurant une priorité absolue pour nous. Nous nous engageons à faire en sorte que TikTok propose une expérience sûre et positive pour les moins de 18 ans.

Dans le cadre de cet engagement, nos politiques interdisent strictement tout contenu susceptible de compromettre le bien-être des jeunes, y compris le risque d’exploitation ou de dommages psychologiques, physiques ou développementaux potentiels. Nous accordons la priorité à la sécurité et au bien-être des jeunes tout au long de notre processus d’élaboration de nos politiques. Cela comprend l’interdiction du matériel pédopornographique, de la maltraitance des jeunes, du harcèlement, des activités et défis dangereux, de l’exposition à des thèmes trop matures et de la consommation d’alcool, de tabac, de drogues ou de substances réglementées. Lorsque nous identifions des cas d’exploitation de jeunes sur notre plateforme, nous prenons des mesures immédiates, y compris le bannissement du compte incriminé et de tous les comptes associés.

Nous ajoutons également des informations sur l’application de la règlementation pour TikTok LIVE, qui permet aux utilisateurs et aux créateurs d’interagir en temps réel. Au cours du deuxième trimestre 2023, nous avons suspendu 8 074 632 sessions LIVE. Cela représente 1,5 % du nombre total de sessions LIVE au cours de cette même période. Parmi elles, 252 045 suspensions ont fait l’objet d’un recours et ont été levées. Pour que l’expérience TikTok soit la meilleure possible, nous sommes ravis de partager plus d’informations sur nos efforts de modération LIVE et nous continuerons à développer ce sujet dans les prochains rapports.

#### **Suppression des vidéos relatives à la publication**

#### **Nombre total de vidéos et de comptes supprimés et restaurés**

#### **Taux de suppression et délais de réponse aux signalements**

#### **Politique en matière de suppression**

#### **Modération par marché et par langue**

#### Covert influence operations

**Comptes ciblant les relations étrangères de l’Europe avec la Russie**

Nous estimons que ce réseau opérait à partir de la Russie et ciblait des audiences russes en Russie, aux Pays-Bas, en Roumanie, en France et au Royaume-Uni. Les personnes à l’origine de ce réseau ont créé de faux comptes et ont posté en masse du contenu afin d’amplifier artificiellement des points de vue pro-russes spécifiques dans le contexte de la guerre en Ukraine, en contournant l’interdiction régionale d’utiliser TikTok en Russie.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes en réseau**** | ****Personnes qui suivent le réseau**** |
| Interne | 588 | 36,331 |

**Comptes ciblant les relations étrangères de l’Irak avec la Russie**

Nous estimons que ce réseau opérait depuis l’Irak et ciblait des audiences irakiennes. Les personnes à l’origine de ce réseau ont créé de faux comptes, y compris de faux personas et de fausses entités médiatiques, afin d’amplifier artificiellement des contenus soulignant l’importance stratégique de la Russie en Irak.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes en réseau**** | ****Personnes qui suivent le réseau**** |
| Interne | 364 | 524,513 |

**Comptes ciblant les discours sur la Chine**

Nous estimons que ce réseau opérait depuis la Chine et ciblait des audiences américaines. Les personnes à l’origine de ce réseau ont créé de faux comptes afin de susciter l’intérêt des utilisateurs pour les points de vue pro-chinois, le tourisme pro-chinois, la culture pro-chinoise et les récits anti-occidentaux.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes en réseau**** | ****Personnes qui suivent le réseau**** |
| Interne | 108 | 141,621 |

**Comptes ciblant les discours politiques en Grèce**

Nous estimons que ce réseau opérait depuis la Grèce et ciblait des audiences grecques. Les personnes à l’origine de ce réseau ont créé de faux comptes et ont posté en masse du contenu pour amplifier artificiellement des récits spécifiques favorables au parti politique « Nouvelle Démocratie » et critiques à l’égard du parti d’opposition « Syriza », en ciblant les discours des élections grecques.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes en réseau**** | ****Personnes qui suivent le réseau**** |
| Interne | 69  | 25,898 |

**Comptes ciblant les discours politiques en Turquie**

Nous estimons que ce réseau opérait depuis la Belgique et ciblait des audiences turques. Les personnes à l’origine de ce réseau ont créé de faux comptes, y compris de faux personas et de fausses entités médiatiques, afin d’amplifier artificiellement des contenus favorables au président Erdogan et en relation avec le Parti des travailleurs du Kurdistan (PKK), ciblant le discours sur les élections en Turquie.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes en réseau**** | ****Personnes qui suivent le réseau**** |
| Interne | 68  | 61,379 |

**Comptes ciblant les discours politiques en Thaïlande**

Nous estimons que ce réseau opérait depuis la Thaïlande et ciblait des audiences thaïlandaises. Les personnes à l’origine de ce réseau ont créé de faux comptes, y compris la création étendue de faux personas, afin d’amplifier artificiellement des points de vue spécifiques du parti pro-PPRP, en ciblant le discours sur les élections générales thaïlandaises.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes en réseau**** | ****Personnes qui suivent le réseau**** |
| Interne | 31  | 1,817 |

**Comptes ciblant le conflit au Soudan**

Nous estimons que ce réseau opérait depuis le Soudan et ciblait des audiences soudanaises. Les personnes à l’origine de ce réseau ont créé de faux comptes, y compris de faux personas et de fausses entités médiatiques, afin d’amplifier artificiellement des contenus favorables au groupe paramilitaire Rapid Support Force (RSF), ciblant le discours sur le conflit en cours entre les forces RSF et les forces armées soudanaises.

|     |     |     |
| --- | --- | --- |
| ****Source de la détection**** | ****Comptes en réseau**** | ****Personnes qui suivent le réseau**** |
| Interne | 17  | 10,213 |

REMARQUE: Le retrait d’une opération d’influence cachée nécessite des opérations à plusieurs niveaux, comprenant l’enquête, la suppression et l’analyse à l’issue de l’intervention. Nous indiquons l’intervention sur ces réseaux au cours du trimestre où le processus complet des opérations a été achevé.

**Définitions**

* **Les réseaux ont opéré depuis :** indique l’emplacement géographique des opérations du réseau selon des preuves techniques et comportementales récoltées depuis des sources privées et ouvertes. Il est possible que TikTok ne soit pas en mesure de relier ces réseaux à des entités, des individus ou des groupes définis.
* **Source de la détection :** considérée comme interne quand la présence de l’activité a été identifiée uniquement via une enquête menée en interne. La détection externe renvoie à des enquêtes qui ont débuté avec un rapport externe, ayant ensuite entraîné une enquête.
* **Abonnés du réseau :** nombre total cumulé des comptes abonnés aux comptes du réseau au moment de la suppression du réseau.

* * *

### **Sûreté**

La protection de notre communauté est notre priorité. Bien que TikTok soit une plateforme mondiale, nous adoptons une approche locale en ce qui concerne la conformité réglementaire. Nous collaborons avec les parties prenantes afin de mieux comprendre les préoccupations locales et de respecter nos engagements réglementaires. Nous avons récemment lancé le [projet Clover](https://newsroom.tiktok.com/en-eu/setting-a-new-standard-in-european-data-security-with-project-clover), une initiative visant à créer un environnement spécialement conçu pour protéger les données des utilisateurs européens de TikTok. Avec le projet Clover, nous concevons et mettons en œuvre une série de mesures qui visent à renforcer les protections de données existantes, à en ajouter de nouvelles et à aligner notre approche globale de la gouvernance des données sur le principe de la souveraineté européenne en matière de données. En outre, aux États-Unis, nous avons créé une division spécialisée appelée [TikTok U.S. Data Security (USDS)](https://newsroom.tiktok.com/en-us/our-approach-to-keeping-us-data-secure) visant à renforcer nos politiques et nos protocoles de protection des données, accroître la protection de nos utilisateurs et instaurer la confiance dans nos systèmes, notre gouvernance et nos contrôles aux États-Unis.

Nous restons également vigilants afin de continuer à détecter les menaces externes et protéger la plateforme contre les faux comptes et le faux engagement. Ces menaces sondent et attaquent constamment nos systèmes, ce qui entraîne des fluctuations occasionnelles dans les mesures rapportées dans ces domaines. Malgré cela, nous restons fidèles à notre engagement d’identifier et de supprimer rapidement tout compte, contenu ou activité visant à augmenter artificiellement la popularité sur notre plateforme. Alors que nous naviguons dans un paysage de menaces en constante évolution, TikTok reste déterminé à préserver l’intégrité de notre communauté et à renforcer notre cyber-réaction face aux acteurs adverses. Nous nous engageons à préserver la confiance que nous accordent nos utilisateurs et nous continuerons à investir dans des mesures solides qui donnent la priorité à la sécurité des utilisateurs, à la protection des données et au respect des réglementations.

#### **Spam et faux engagement**

* * *

#### **Contenus publicitaires**

Les comptes d’annonceurs et le contenu des publicités doivent être conformes à nos règles communautaires, [Politiques publicitaires](https://ads.tiktok.com/help/article/tiktok-advertising-policies-industry-entry) et nos Conditions d’utilisation. TikTok a mis en place des politiques strictes pour protéger sa communauté des contenus publicitaires faux, frauduleux ou trompeurs. Garantir la sécurité des utilisateurs et des annonceurs est un engagement permanent. Bien que le deuxième trimestre 2023 ait vu une augmentation du volume total de publicités supprimées à la fois pour violation de nos politiques publicitaires ainsi qu’en raison d’actions au niveau du compte, nous examinons et renforçons continuellement nos systèmes afin d’identifier de nouveaux modèles et de supprimer rapidement et de manière ciblée les publicités qui enfreignent nos politiques. Par exemple, pour modérer plus efficacement une augmentation récente des publicités pour les jeux de hasard à haut risque sur la plateforme, nos équipes ont déployé des stratégies automatisées supplémentaires pour détecter et gérer les publicités enfreignant nos politiques relatives au secteur à haut risque. En appliquant des politiques strictes, en tirant parti de mécanismes de détection avancés et en améliorant continuellement nos systèmes, nous nous efforçons de favoriser une expérience publicitaire instaurant la confiance, agréable et conforme aux valeurs de notre communauté TikTok.

#### **Application des règles concernant les contenus publicitaires**

### **Autres rapports**

* [Rapport sur les demandes de retrait émanant de gouvernements](https://www.tiktok.com/transparency/fr-fr/government-removal-requests)
    
* [Rapport de demandes de retrait de contenus pour infractions aux lois sur la propriété intellectuelle](https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests-2021-1/)
    
* [Rapport sur les demandes de renseignements](https://www.tiktok.com/transparency/fr-fr/information-requests-2021-1/)
    

#### Cela a-t-il été utile ?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Oui![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)Non

# Resource URL: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2023-1/
Application des Règles communautaires

_1e janvier – 31 mars 2023_  
_Publication du 30 juin 2023_

### À propos de ce rapport

TikTok est une plateforme mondiale de divertissement qui repose sur la créativité de sa communauté. Nous mettons tout en œuvre pour favoriser un environnement inclusif où les utilisateurs peuvent laisser libre cours à leur créativité, avoir le sentiment d’appartenir à une communauté et se divertir. Pour préserver cet environnement, nous agissons lorsqu’un contenu ou un compte vient enfreindre nos Règles Communautaires ou nos Conditions de service. Par ailleurs, nous publions régulièrement des informations sur les mesures prises afin de rendre compte à notre communauté.

TikTok a recours à des technologies innovantes ainsi qu’à des modérateurs pour identifier, examiner et agir sur les contenus qui enfreignent ses règles. Ce rapport fournit un aperçu trimestriel du volume et de la nature des contenus et des comptes supprimés de notre plateforme.

### Analyse

Les Règles Communautaires de TikTok sont définies pour favoriser une expérience divertissante qui privilégie la sécurité, l’inclusion et l’authenticité. Nos règles s’appliquent à tous et à tous les contenus sans exception, et nous nous efforçons d’être cohérents et justes dans leur application. Cette analyse propose des éléments contextuels pour faciliter l’appréciation des données de ce rapport.

### Sécurité

Dans ce rapport, nous vous présentons nos nouvelles données avec la volonté de transparence que nous continuons à appliquer dans la manière dont nous gérons et protégeons la plateforme. Tout d’abord, nous vous donnons un aperçu des compétences linguistiques des modérateurs de TikTok, dont le travail contribue non seulement à rendre la plateforme plus sûre, mais joue également un rôle essentiel dans l’amélioration de nos systèmes de modération automatisés. Ensuite, nous vous fournirons des données concernant les délais de traitement de suppression des contenus violents signalés par notre communauté à l’aide de nos outils de signalement en ligne et dans l’application.

Au cours de la période couverte par le présent rapport, nous avons mis à jour les [Règles communautaires](https://newsroom.tiktok.com/en-us/community-guidelines-update) de TikTok. Ces règles sont entrées en vigueur en avril 2023 et seront prises en compte dans notre prochain rapport.

### Sûreté

Nous restons vigilants dans nos efforts pour protéger la plateforme contre les menaces, y compris la présence de comptes et d’engagements inauthentiques ou faux. Ces menaces attaquent nos systèmes et les mettent à l’épreuve de manière continue, ce qui peut entraîner des fluctuations ponctuelles dans les indicateurs rapportés dans ces domaines. Néanmoins, nous restons déterminés à identifier et à supprimer rapidement tout compte, contenu ou activité visant à accroître artificiellement la popularité sur notre plateforme.

La protection de notre communauté est notre priorité. Bien que TikTok soit une plateforme mondiale, nous adoptons une approche locale de la conformité réglementaire en collaborant avec les parties prenantes afin de mieux comprendre les préoccupations locales et respecter nos obligations légales. Aux États-Unis, nous avons créé une division spécialisée appelée [TikTok U.S. Data Security (USDS)](https://newsroom.tiktok.com/en-us/our-approach-to-keeping-us-data-secure) pour renforcer nos politiques et protocoles de protection des données, protéger davantage nos utilisateurs et renforcer la confiance dans nos systèmes et contrôles aux États-Unis. En outre, nous avons lancé le [projet Clover](https://newsroom.tiktok.com/en-eu/setting-a-new-standard-in-european-data-security-with-project-clover), une initiative visant à créer un environnement protégé spécialement conçu pour les données des utilisateurs européens de TikTok. À travers le projet Clover, nous concevons et mettons en œuvre une série de nouvelles mesures visant à renforcer les protections existantes, à en ajouter de nouvelles et à aligner notre approche globale de la gouvernance des données sur le principe de la souveraineté européenne en matière de données.

Dans ce contexte de menaces en constante évolution, TikTok s’engage à préserver l’intégrité de sa communauté et à renforcer ses défenses contre les menaces extérieures. Nous nous engageons à préserver la confiance que nous accordent nos utilisateurs et nous continuerons à investir dans des mécanismes solides qui donnent la priorité à la sécurité des utilisateurs, à la protection des données et au respect de la réglementation.

### Contenus publicitaires

TikTok a mis en place des politiques strictes pour protéger les utilisateurs contre les contenus faux, frauduleux ou trompeurs, y compris les contenus publicitaires. Les comptes des annonceurs et le contenu des publicités sont soumis à ces politiques et doivent respecter nos Règles Communautaires, nos Règles publicitaires et nos Conditions d’utilisation. Au cours du premier trimestre de 2023, le volume total de contenus publicitaires retirés pour non-respect de nos politiques publicitaires et le nombre de contenus publicitaires supprimés en raison d’actions au niveau du compte ont diminué. Nous sommes conscients qu’assurer la sécurité des annonceurs et des utilisateurs nécessite un engagement permanent. C’est pourquoi nous nous engageons à revoir et à renforcer constamment nos systèmes afin de pouvoir supprimer rapidement et avec précision les contenus publicitaires contraires à nos politiques. En appliquant des politiques strictes, en tirant parti de mécanismes de détection avancés et en améliorant continuellement nos systèmes, nous nous efforçons de favoriser une expérience publicitaire sûre, agréable et conforme aux valeurs de notre passionnante communauté TikTok.

### Données les plus récentes

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Télécharger](https://sf16-va.tiktokcdn.com/obj/eden-va2/nuvlojeh7ryht/Transparency_CGE_2023Q1/2023Q1_raw_data_cger_French.csv)

#### **Nombre total de vidéos supprimés / total de vidéos par trimestre**

_REMARQUE : le nombre total de vidéos supprimées représente environ 1 % de toutes les vidéos téléchargées sur TikTok. Le nombre de vidéos supprimées mentionné dans ce rapport comprend le contenu vidéo au format court (notamment les vidéos et les stories contenant des photos)._

#### **Nombre total de vidéos supprimées / remises en ligne, par type et trimestre**

_REMARQUE : dans le tableau ci-dessous, les vidéos remises en ligne sont prises en compte à la fois dans les volumes de suppression et les volumes de remise en ligne._

#### **Taux de suppression, par trimestre / règle**

_REMARQUE : la suppression proactive se caractérise par l’identification et le retrait d’une vidéo avant qu’elle ne soit signalée. La suppression dans les 24 heures signifie que l’on retire la vidéo dans les 24 heures suivant sa mise en ligne sur la plateforme._

#### **Nombre total de suppressions de vidéos, par règle et politique**

_REMARQUE : ce graphique montre le volume de vidéos supprimées par infraction. Une vidéo peut enfreindre plusieurs règles et politiques, ainsi le graphique indique chaque infraction. Dans certains cas néanmoins rares, comme lors de situations d’urgence ou des pannes matérielles, la catégorie d’infraction d’une vidéo supprimée peut ne pas être enregistrée. Ces vidéos ne sont pas représentées dans le graphique ci-dessus, mais sont comptabilisées en chiffres absolus tout au long de ce rapport._

#### **Nombre total de suppressions et taux de vidéos, par sous-règle et politique**

_REMARQUE : seules les vidéos qui ont été examinées et supprimées par des modérateurs sont incluses dans le tableau de bord des sous-règles et politiques._

#### **Volume et taux de suppression, par marché**

_REMARQUE : ce graphique dresse la liste des cinquante pays avec le plus grand nombre de vidéos supprimées et qui représentent environ 90 % du volume total des suppressions._

#### _**Répartition**_ _**des langues**_ _**couvertes par**_ _**la modération humaine**_

_REMARQUE : Ce tableau tient compte de la langue principale des modérateurs qui travaillent sur les vidéos de courte durée, les diffusions en direct, les commentaires, les comptes et TikTok Now. Il n’intègre pas les modérateurs qui peuvent être amenés à travailler dans plusieurs langues. Les langues qui constituent la langue principale de moins de 2 % de tous les modérateurs ont été regroupées dans la catégorie « Autres langues ». La modération peut également s’effectuer dans des langues autres que celles qui ont été définies comme langues principales possibles pour les modérateurs._

#### _**Délai de traitement des contenus signalés par la communauté**_

_REMARQUE : Seules les vidéos qui ont été signalées pour la première fois par des utilisateurs et qui ont ensuite fait l’objet d’une mesure de retrait sont incluses. Les données indiquent le temps passé entre le moment où TikTok a reçu un rapport d’utilisateur et le moment où le contenu a été supprimé. Maintenir l’équilibre entre les signalements et les réponses dans le cadre des contrôles humains nécessite des capacités, des formations et du contenu adaptés, ce qui peut parfois entraîner des délais de contrôle plus longs._

#### **Nombre total de comptes supprimés, par trimestre et par motif**

_REMARQUE : en plus du retrait des comptes qui enfreignent nos Règles Communautaires, nous procédons à la suppression des comptes dont les activités de spam sont avérées, ainsi que les vidéos de spam publiées par ces comptes. Nous prenons également des mesures proactives pour empêcher la création de comptes de spam par des moyens automatisés._

#### **Fausse activité**

_REMARQUE : nous prenons des mesures pour supprimer et empêcher les « j’aime », les « abonnés » et les demandes « d’abonnement » si nous estimons que l’activité est automatisée ou frauduleuse._

#### **Activité des comptes spam**

_REMARQUE : lorsque nous supprimons des comptes pour cause de spam, nous supprimons également les vidéos créées par ces comptes, conformément à nos règles en matière de spam._

#### **Opérations d’influence cachées**

|     |     |     |     |
| --- | --- | --- | --- |
| **Réseaux d’opérations d’influence cachées identifiés et supprimés** **au premier trimestre 2023** | **Source de la détection** | **Comptes du réseau** | **Abonnés du réseau** |
| Ce réseau semblait opérer depuis la Russie et cibler principalement les audiences russes. Le réseau a été partiellement constitué avant que nous ne suspendions la diffusion en direct et les nouveaux contenus en Russie et il a largement eu recours à la tactique du masquage de l’origine géographique. Les individus derrière ce réseau ont utilisé de fausses identités, y compris de faux organes d’information, afin d’amplifier artificiellement des opinions pro-russes dans le discours sur la guerre en Ukraine. | Interne | 15  | 38,326 |
| Ce réseau semblait opérer depuis la Russie et cibler les audiences russes. Le réseau a largement utilisé le masquage de l’origine géographique comme tactique pour tromper les systèmes de TikTok. Les individus derrière ce réseau ont utilisé de faux comptes pour publier massivement du contenu afin de promouvoir artificiellement le film de guerre « The Best in Hell » d’Evgueni Viktorovitch Prigojine et d’amplifier artificiellement des opinions pro-russes dans le contexte la guerre en Ukraine. | Interne | 254 | 34,110 |
| Ce réseau semblait opérer depuis la Pologne et cibler les audiences polonaises. Les individus derrière ce réseau ont créé de fausses identités et publié massivement des commentaires au contenu similaire afin d’amplifier artificiellement des opinions anti-russes. | Interne | 41  | 40,256 |
| Ce réseau semblait opérer depuis Israël et cibler les audiences israéliennes. Les individus derrière ce réseau ont utilisé de fausses identités afin d’amplifier artificiellement des opinions pro-israéliennes spécifiques dans le discours sur le conflit en cours en Palestine dans le contexte des dernières élections israéliennes. | Interne | 362 | 168,202 |
| Ce réseau semblait opérer depuis la Russie et cibler les audiences moldaves. Les personnes à l’origine de ce réseau se sont fait passer pour de fausses agences de presse et ont créé de fausses identités afin de créer une base d’abonnés sur TikTok pour ensuite contourner l’interdiction régionale de TikTok en Russie en redirigeant ces abonnés hors de la plateforme. | Interne | 11  | 55,066 |
| Ce réseau semblait opérer depuis la Russie et cibler les audiences russes. Les individus derrière ce réseau ont créé de faux comptes et publié massivement du contenu afin d’amplifier artificiellement certaines opinions pro-russes dans le contexte de la guerre en Ukraine en abusant de la fonction de partage et en utilisant l’application web afin de contourner l’interdiction régionale de TikTok en Russie. | Interne | 1,351 | 226,838 |
| Ce réseau semblait opérer depuis le Royaume-Uni et le Nigeria et cibler principalement les audiences nigérianes. Les individus derrière ce réseau ont utilisé de fausses identités et se sont fait passer pour de faux organes d’information afin d’amplifier artificiellement des opinions liées à la région du Biafra dans le discours sur les élections au Nigeria. | Interne | 15  | 47,103 |
| Ce réseau semblait opérer depuis la Russie et cibler différents pays européens, tels que l’Allemagne, l’Italie et le Royaume-Uni. Les individus derrière ce réseau ont usurpé des identités afin d’amplifier artificiellement certaines opinions liées au président ukrainien Zelensky, aux sanctions économiques actuellement imposées à la Russie et aux réfugiés ukrainiens. | Interne | 12  | 1,480 |
| Ce réseau semblait opérer depuis l’Irlande et cibler les audiences irlandaises. Les personnes à l’origine de ce réseau ont créé de faux comptes, publié massivement des contenus qui sèment la discorde au sujet du nationalisme dans l’Irlande, le Japon, la Russie et Taïwan et publié massivement des commentaires contenant des contenus de mauvaise qualité similaires, et ce dans le but de rediriger les utilisateurs TikTok hors de la plateforme et d’intensifier les conflits sociaux. | Interne | 72  | 94,743 |
| Ce réseau semblait opérer depuis la Malaisie et cibler les audiences malaisiennes. Les personnes à l’origine de ce réseau ont créé de faux comptes et ont publié massivement des commentaires identiques en anglais et en malais sur de nombreuses vidéos au sujet des élections en Malaisie. | Interne | 175 | 285,511 |
| Ce réseau semblait opérer depuis l’Ukraine et cibler les audiences ukrainiennes. Les individus derrière ce réseau ont créé de faux comptes et publié massivement du contenu afin d’amplifier artificiellement certaines opinions liées au gouvernement ukrainien et de promouvoir une image positive du président Zelensky. | Interne | 119 | 90,303 |
| Ce réseau semblait opérer depuis l’Allemagne et cibler les audiences égyptiennes. Les individus derrière ce réseau ont crée de faux comptes afin d’amplifier artificiellement les appels à rejoindre des manifestations inexistantes en Égypte en ciblant le discours sur le gouvernement égyptien actuel. | Interne | 6   | 368,644 |

REMARQUE : le retrait d’une opération d’influence cachée nécessite des opérations à plusieurs niveaux, comprenant l’enquête, la suppression et l’analyse à l’issue de l’intervention. Nous indiquons l’intervention sur ces réseaux au cours du trimestre où le processus complet des opérations a été achevé.

### Définitions

* **Les réseaux ont opéré depuis :** indique l’emplacement géographique des opérations du réseau selon des preuves techniques et comportementales récoltées depuis des sources privées et ouvertes. Il est possible que TikTok ne soit pas en mesure de relier ces réseaux à des entités, des individus ou des groupes définis.
* **Source de la détection :** considérée comme interne quand la présence de l’activité a été identifiée uniquement via une enquête menée en interne. La détection externe renvoie à des enquêtes qui ont débuté avec un rapport externe, ayant ensuite entraîné une enquête.
* **Abonnés du réseau :** nombre total cumulé des comptes abonnés aux comptes du réseau au moment de la suppression du réseau.

#### **Application des règles concernant les contenus publicitaires**

_REMARQUE : Les contenus peuvent être supprimés individuellement ou en bloc en appliquant des mesures à l’ensemble d’un compte d’annonceur._

### **Autres rapports**

* [Rapport sur les demandes de retrait émanant de gouvernements](https://www.tiktok.com/transparency/fr-fr/government-removal-requests)
* [Rapport de demandes de retrait de contenus pour infractions aux lois sur la propriété intellectuelle](https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests-2021-1/)
* [Rapport sur les demandes de renseignements](https://www.tiktok.com/transparency/fr-fr/information-requests-2021-1/)

#### Cela a-t-il été utile ?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Oui![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)Non

# Resource URL: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2022-4/
Application des Règles communautaires

_1er octobre 2022 – 31 décembre 2022_  
_Publication du 31 mars 2023_

### À propos de ce rapport

TikTok est une plateforme mondiale de divertissement qui repose sur la créativité de sa communauté. Nous mettons tout en œuvre pour favoriser un environnement inclusif où les utilisateurs peuvent laisser libre cours à leur créativité, avoir le sentiment d’appartenir à une communauté et se divertir. Pour préserver cet environnement, nous agissons lorsqu’un contenu ou un compte vient enfreindre nos Règles Communautaires ou nos Conditions de service. Par ailleurs, nous publions régulièrement des informations sur les mesures prises afin de rendre compte à notre communauté.

TikTok a recours à des technologies innovantes ainsi qu’à des modérateurs pour identifier, examiner et agir sur les contenus qui enfreignent ses règles. Ce rapport fournit un aperçu trimestriel du volume et de la nature des contenus et des comptes supprimés de notre plateforme.

### Analyse

Les Règles Communautaires de TikTok sont définies pour favoriser une expérience divertissante qui privilégie la sécurité, l’inclusion et l’authenticité. Nos règles s’appliquent à tous et à tous les contenus sans exception, et nous nous efforçons d’être cohérents et justes dans leur application. Cette analyse propose des éléments contextuels pour faciliter l’appréciation des données de ce rapport.

### Sécurité

Nous nous efforçons de créer une expérience sûre, positive et divertissante pour notre communauté. Nous continuons à innover dans notre approche de la protection des personnes, notamment en supprimant des contenus, en rendant certains contenus [inéligibles à la recommandation dans les fils « Pour toi »](https://www.tiktok.com/community-guidelines?lang=en#44), en introduisant de [nouveaux outils de filtrage de contenu](https://newsroom.tiktok.com/fr-fr/de-nouvelles-opportunites-de-divertir-la-communaute) et une [mise à jour de notre politique de modération](https://newsroom.tiktok.com/fr-fr/soutenir-les-createurs-avec-une-mise-a-jour-de-notre-politique-de-moderation) afin de garantir que le fil « Pour toi » de chaque utilisateur soit alimenté en contenus appropriés et divertissants pour lui.En outre, TikTok attache une importance toute particulière aux réglementations en matière de sécurité et de conformité. C’est pourquoi nous allouons des ressources importantes pour appliquer les différentes législations, telles que la législation européenne sur les services numériques (Digital Services Act, DSA). La transparence est au cœur du DSA et fait appel à la responsabilité, ce qui est en totale adéquation avec l’éthique de TikTok.

Pour nous aider à atteindre cet objectif à grande échelle, nous investissons dans des [technologies](https://newsroom.tiktok.com/en-us/advancing-our-approach-to-user-safety) qui nous permettent de détecter et de supprimer automatiquement les contenus violents, ce qui contribue à réduire le volume de contenus examinés par les modérateurs, ce qui permet à un plus grand nombre d’entre eux, de se concentrer sur des domaines plus complexes et contextuels, tels que la désinformation, les discours haineux et le harcèlement. En ce qui concerne le contrôle du respect de la législation, notre approche vise à :

* Réduire au minimum l’affichage de contenus qui enfreignent nos Règles communautaires;
* Donner la priorité au retrait rapide des contenus particulièrement choquants, tels que les contenus pédopornographiques et l’extrémisme violent;
* Veiller à la cohérence et à l’équité dans l’application de la législation afin de réduire le nombre de suppressions injustifiées.

Pour mieux concrétiser cette vision, nous avons continué l’année passée à investir dans le machine learning automatique et dans une mise à niveau de l’architecture qui prend en compte les contenus vidéo, textuels et audio, ainsi que la gravité éventuelle de l’infraction et la portée potentielle de la vidéo (par exemple, en fonction du nombre de personnes qui la suivent) afin de déterminer la meilleure ligne de conduite à adopter. Par exemple, le contenu peut être supprimé automatiquement, rendu inéligible pour une recommandation dans les flux « Pour toi » ou dirigé vers un modérateur pour un examen plus approfondi. Nous avons également mis en place un nouveau système de contrôle plus transparent pour les créateurs. Nous pensons que ces améliorations nous permettent d’offrir une meilleure expérience aux créateurs tout en continuant à faire progresser notre engagement au service de la sécurité sur l’ensemble de la plateforme. Dans l’ensemble, ces efforts ont permis d’améliorer la précision et l’efficacité de notre surveillance, comme le reflètent les données de ce rapport.

### Sûreté

Nous continuons à tester et à améliorer nos capacités de sûreté face à l’évolution constante des menaces en investissant dans des défenses automatisées pour détecter, bloquer et supprimer les faux comptes ainsi que les fausses activités. Quasiment à chaque fois que nous endiguons des menaces, les acteurs malveillants font preuve de persistance et font évoluer dans leurs tactiques afin de contourner nos systèmes de détection et de contrôle. Par conséquent, les fluctuations trimestrielles du volume total de fausses activités contre lesquelles nous prenons des mesures peuvent varier. Cependant, nous restons déterminés à affiner constamment les politiques et les outils de contrôle que nous utilisons pour supprimer tout contenu ou activité visant à gonfler artificiellement la popularité sur la plateforme.

### Contenus publicitaires

TikTok a mis en place des politiques strictes pour protéger les utilisateurs contre les contenus faux, frauduleux ou trompeurs, y compris les contenus publicitaires. Les comptes des annonceurs et le contenu des publicités sont soumis à ces politiques et doivent respecter nos Règles Communautaires, nos Règles publicitaires et nos Conditions d’utilisation. Au cours du dernier trimestre de 2022, le volume total de contenus publicitaires retirés pour non-respect de nos politiques publicitaires et le nombre de contenus publicitaires supprimés en raison d’actions au niveau du compte ont diminué. Les progrès réalisés dans nos capacités de détection et de suppression proactive nous ont permis d’améliorer notre paysage publicitaire grâce à une suppression plus précise et plus rapide des contenus publicitaires contraires à nos politiques. Nous sommes conscients que, pour garantir la sécurité de nos annonceurs et de nos utilisateurs, nous devons faire des efforts constants. Nous nous engageons donc à régulièrement analyser et renforcer nos systèmes de lutte contre les contenus publicitaires ne respectant pas nos politiques, ainsi qu’à créer un environnement où les utilisateurs associent les marques qu’ils voient à de la qualité et de la positivité.

### Données les plus récentes

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Télécharger](https://sf16-va.tiktokcdn.com/obj/eden-va2/nuvlojeh7ryht/Transparency_CGE_2022Q4/2022Q4_raw_data_cger_French.csv)

#### **Nombre total de vidéos supprimés / total de vidéos par trimestre**

_REMARQUE : le nombre total de vidéos supprimées représente environ 1 % de toutes les vidéos téléchargées sur TikTok. Le nombre de vidéos supprimées mentionné dans ce rapport comprend le contenu vidéo au format court (notamment les vidéos et les stories contenant des photos)._

#### Nombre total de vidéos supprimées / remises en ligne, par type et trimestre

_REMARQUE : dans le tableau ci-dessous, les vidéos remises en ligne sont prises en compte à la fois dans les volumes de suppression et les volumes de remise en ligne._

#### Taux de suppression, par trimestre / règle

_REMARQUE : la suppression proactive se caractérise par l’identification et le retrait d’une vidéo avant qu’elle ne soit signalée. La suppression dans les 24 heures signifie que l’on retire la vidéo dans les 24 heures suivant sa mise en ligne sur la plateforme._

#### Nombre total de suppressions de vidéos, par règle et politique

_REMARQUE : ce graphique montre le volume de vidéos supprimées par infraction. Une vidéo peut enfreindre plusieurs règles et politiques, ainsi le graphique indique chaque infraction. Dans certains cas néanmoins rares, comme lors de situations d’urgence ou des pannes matérielles, la catégorie d’infraction d’une vidéo supprimée peut ne pas être enregistrée. Ces vidéos ne sont pas représentées dans le graphique ci-dessus, mais sont comptabilisées en chiffres absolus tout au long de ce rapport._

#### Nombre total de suppressions et taux de vidéos, par sous-règle et politique

_REMARQUE : seules les vidéos qui ont été examinées et supprimées par des modérateurs sont incluses dans le tableau de bord des sous-règles et politiques._

#### Volume et taux de suppression, par marché

_REMARQUE : ce graphique dresse la liste des cinquante pays avec le plus grand nombre de vidéos supprimées et qui représentent environ 90 % du volume total des suppressions._

#### Nombre total de comptes supprimés, par trimestre et par motif

_REMARQUE : en plus du retrait des comptes qui enfreignent nos Règles Communautaires, nous procédons à la suppression des comptes dont les activités de spam sont avérées, ainsi que les vidéos de spam publiées par ces comptes. Nous prenons également des mesures proactives pour empêcher la création de comptes de spam par des moyens automatisés._

#### Fausse activité

_REMARQUE : nous prenons des mesures pour supprimer et empêcher les « j’aime », les « abonnés » et les demandes « d’abonnement » si nous estimons que l’activité est automatisée ou frauduleuse._

#### Activité des comptes spam

_REMARQUE : lorsque nous supprimons des comptes pour cause de spam, nous supprimons également les vidéos créées par ces comptes, conformément à nos règles en matière de spam._

#### **Opérations d’influence cachées**

|     |     |     |     |
| --- | --- | --- | --- |
| Réseaux d’opérations d’influence cachées identifiés et supprimés au quatrième trimestre 2022 | Source de la détection | Comptes du réseau | Abonnés du réseau |
| Ce réseau semblait opérer depuis le Kenya et cibler principalement les audiences du Kenya. Les individus derrière ces comptes ont partagé du contenu en swahili et en anglais via de faux comptes usurpant l’identité de personnalités politiques, ainsi que celle d’organes d’information et de jeunes femmes. Le réseau avait probablement des motivations financières et visait le discours civique au Kenya. | Externe | 33  | 19,120 |
| Ce réseau semblait opérer depuis le Brésil et cibler principalement les audiences brésiliennes. Les individus derrière ces comptes ont partagé du contenu en portugais et en anglais via de faux comptes avec des photos de banques d’images comme images de profil afin d’apparaître comme des personnes vivant au Brésil. Le réseau a diffusé des récits qui ont semé le doute quant à la fiabilité du processus électoral au Brésil. | Interne | 138 | 642,941 |
| Ce réseau semblait opérer depuis l’Azerbaïdjan et cibler principalement les audiences azerbaïdjanaises. Les personnes à l’origine de ces comptes utilisaient de faux comptes et publiaient des contenus de manière répétitive dans le but de diffuser des récits de soutien à l’Azerbaïdjan et à son gouvernement actuel dans le contexte du conflit entre l’Arménie et l’Azerbaïdjan dans la région du Haut-Karabakh. | Interne | 112 | 23,348 |
| Ce réseau semblait opérer depuis la Russie et cibler les pays européens, principalement l’Allemagne. Les individus derrière ces comptes ont créé de faux comptes localisés et ont partagé du faux contenu localisé en allemand sur la guerre actuelle en Ukraine, ainsi que sur les conséquences de celle-ci sur les économies des pays de l’UE. | Interne | 3,181 | 418,196 |
| Ce réseau semblait opérer depuis le Kazakhstan et cibler principalement les audiences kazakhes. Les personnes à l’origine de ces comptes ont utilisé de faux comptes et se sont fait passer pour des groupes d’activistes civils du pays afin d’inciter à des manifestations contre le gouvernement actuel en promouvant artificiellement des hashtags et des récits critiquant le gouvernement kazakh actuel, la structure de l’armée dans le pays et les élections à venir. | Interne | 6   | 38,003 |

_REMARQUE : le retrait d’une opération d’influence cachée nécessite des opérations à plusieurs niveaux, comprenant l’enquête, la suppression et l’analyse à l’issue de l’intervention. Nous indiquons l’intervention sur ces réseaux au cours du trimestre où le processus complet des opérations a été achevé._

#### Définitions

* **Les réseaux ont opéré depuis :** indique l’emplacement géographique des opérations du réseau selon des preuves techniques et comportementales récoltées depuis des sources privées et ouvertes. Il est possible que TikTok ne soit pas en mesure de relier ces réseaux à des entités, des individus ou des groupes définis.
* **Source de la détection :** considérée comme interne quand la présence de l’activité a été identifiée uniquement via une enquête menée en interne. La détection externe renvoie à des enquêtes qui ont débuté avec un rapport externe, ayant ensuite entraîné une enquête.
* **Abonnés du réseau :** nombre total cumulé des comptes abonnés aux comptes du réseau au moment de la suppression du réseau.

#### Application des règles concernant les contenus publicitaires

_REMARQUE : Les contenus peuvent être supprimés individuellement ou en bloc en appliquant des mesures à l’ensemble d’un compte d’annonceur._

### Autres rapports

* [Rapport sur les demandes de retrait émanant de gouvernements](https://www.tiktok.com/transparency/fr-fr/government-removal-requests)
* [Rapport de demandes de retrait de contenus pour infractions aux lois sur la propriété intellectuelle](https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests-2021-1/)
* [Rapport sur les demandes de renseignements](https://www.tiktok.com/transparency/fr-fr/information-requests-2021-1/)

#### Cela a-t-il été utile ?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Oui![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)Non

# Resource URL: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2022-3/
Application des Règles communautaires

_1er juillet 2022 – 30 septembre 2022_  
_Publication du 19 décembre 2022_

### À propos de ce rapport

TikTok est une plateforme mondiale de divertissement qui repose sur la créativité de sa communauté. Nous mettons tout en œuvre pour favoriser un environnement inclusif où les utilisateurs peuvent laisser libre cours à leur créativité, avoir le sentiment d’appartenir à une communauté et se divertir. Pour préserver cet environnement, nous agissons lorsqu’un contenu ou un compte vient enfreindre nos Règles Communautaires ou nos Conditions de service. Par ailleurs, nous publions régulièrement des informations sur les mesures prises afin de rendre compte à notre communauté.

TikTok a recours à des technologies innovantes ainsi qu’à des modérateurs pour identifier, examiner et agir sur les contenus qui enfreignent ses règles. Ce rapport fournit un aperçu trimestriel du volume et de la nature des contenus et des comptes supprimés de notre plateforme.

### **Analyse**

Les Règles Communautaires de TikTok sont définies pour favoriser une expérience divertissante qui privilégie la sécurité, l’inclusion et l’authenticité. Nos règles s’appliquent à tous et à tous les contenus, et nous nous efforçons d’être cohérents et justes dans leur application. Cette analyse propose des éléments contextuels pour faciliter l’appréciation des données de ce rapport.

#### **Sécurité**

Ce rapport a été réalisé pour apporter davantage de transparence à nos actions, nos avancées et nos défis, prendre nos responsabilités vis-à-vis de notre communauté. Pour fournir des informations utiles et pertinentes, nous proposons désormais des informations supplémentaires concernant les retraits des vidéos pour plus de 50 marchés, ce qui représente environ 90 % de l’ensemble des retraits de contenus.

De plus, ce rapport inclut maintenant des informations relatives aux opérations d’influence cachées que nous avons identifiées et supprimées de notre plateforme sur la période de juillet à septembre. Pour enquêter sur ces opérations et les supprimer, nous nous concentrons sur les comportements, et évaluons les relations entre les comptes et les techniques utilisées, pour déterminer si les utilisateurs concernés agissent de manière coordonnée pour tromper les systèmes de TikTok ou notre communauté. Pour chaque cas, nous sommes convaincus que les individus à l’origine de ces activités collaborent afin de donner une fausse image d’eux-mêmes et de leurs actions. Nous savons que les opérations d’influence cachées continueront de se développer dans le but de contourner nos systèmes de détection et certains réseaux pourraient essayer de rétablir leur présence sur notre plateforme. Par exemple, en plus des réseaux dénoncés dans ce rapport, 365 comptes supplémentaires ont été identifiés comme appartenant à des réseaux précédemment retirés et ont donc été supprimés. Face à la hausse de ces nouveaux comportements fallacieux, nous continuerons d’adapter nos réactions, de renforcer notre capacité d’application des règles et de publier les résultats de nos enquêtes.

#### **Sûreté**

Nous continuons à développer et à adapter nos mesures de protection en investissant dans des systèmes automatisés pour détecter, bloquer et supprimer les comptes et les interactions artificielles, et en améliorant notre temps de réaction et notre réponse à l’évolution des menaces. En raison du caractère conflictuel des utilisateurs malveillants, nous allons modifier les données d’application des règles pour les faux comptes. Cela renforce notre engagement visant à retirer tout contenu ou toute activité cherchant à accroître artificiellement sa popularité sur la plateforme.

#### **Contenus publicitaires**

TikTok a mis en place des politiques strictes pour protéger les utilisateurs contre les contenus faux, frauduleux ou trompeurs, y compris les contenus publicitaires. Les comptes des annonceurs et le contenu des annonces sont soumis à ces politiques et doivent respecter nos Règles Communautaires, nos Règles publicitaires et nos Conditions d’utilisation. Bien que le volume total de contenus publicitaires retirés pour non-respect de nos politiques publicitaires ait diminué lors de ce trimestre, le nombre de contenus publicitaires supprimés en raison d’actions au niveau du compte a augmenté. Cela est dû en partie à notre nouvelle approche concernant le non-respect de nos règles publicitaires et au renforcement de nos capacités d’application au niveau des comptes. Nous sommes conscients que, pour garantir la sécurité de nos annonceurs et de nos utilisateurs, nous devons fournir des efforts constants. Nous nous engageons donc à régulièrement analyser et renforcer nos systèmes de lutte contre les contenus publicitaires ne respectant pas nos politiques.

### **Données les plus récentes**

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Télécharger](https://sf16-va.tiktokcdn.com/obj/eden-va2/nuvlojeh7ryht/Transparency_CGE_2022Q3/French_CGE_2022Q3.xlsx)

#### **Nombre total de vidéos supprimés / total de vidéos par trimestre**

_REMARQUE : le nombre total de vidéos supprimées représente environ 1 % de toutes les vidéos téléchargées sur TikTok. le nombre de vidéos supprimées mentionné dans ce rapport comprend le contenu vidéo au format court (notamment les vidéos et les stories contenant des photos)._

#### **Nombre total de vidéos supprimées / remises en ligne, par type et trimestre**

_REMARQUE : dans le tableau ci-dessous, les vidéos remises en ligne sont prises en compte à la fois dans les volumes de suppression et les volumes de remise en ligne._

#### **Nombre total de suppressions de vidéos, par règle et politique**

_REMARQUE : ce graphique montre le volume de vidéos supprimées par infraction. Une vidéo peut enfreindre plusieurs règles et politiques, ainsi le graphique indique chaque infraction. Dans certains cas néanmoins rares, comme lors de situations d’urgence ou des pannes matérielles, la catégorie d’infraction d’une vidéo supprimée peut ne pas être enregistrée. Ces vidéos ne sont pas représentées dans le graphique ci-dessus, mais sont comptabilisées en chiffres absolus tout au long de ce rapport._

#### **Taux de suppression, par trimestre / règle**

_REMARQUE : la suppression proactive se caractérise par l’identification et le retrait d’une vidéo avant qu’elle ne soit signalée. La suppression dans les 24 heures signifie que l’on retire la vidéo dans les 24 heures suivant sa mise en ligne sur la plateforme._

#### **Nombre total de suppressions et taux de vidéos, par sous-règle et politique**

_REMARQUE: Seules les vidéos qui ont été vérifiées par des modérateurs sont incluses dans le tableau de bord des différentes règles et dispositions._

#### **Volume et taux** **de** **suppression,** **par** **pays**

_REMARQUE : ce graphique dresse la liste des cinquante pays avec le plus grand nombre de vidéos supprimées et qui représentent environ 90 % du volume total des suppressions._

#### **Nombre total de comptes supprimés, par trimestre et par motif**

_REMARQUE : en plus du retrait des comptes qui enfreignent nos Règles Communautaires, nous procédons à la suppression des comptes dont les activités de spam sont avérées, ainsi que les vidéos de spam publiées par ces comptes. Nous prenons également des mesures proactives pour empêcher la création de comptes de spam par des moyens automatisés._

#### **Fausse activité**

_REMARQUE : nous prenons des mesures pour supprimer et empêcher les « j’aime », les « abonnés » et les demandes « d’abonnement » si nous estimons que l’activité est automatisée ou frauduleuse._

#### **Activité des comptes spam**

_REMARQUE : lorsque nous supprimons des comptes pour cause de spam, nous supprimons également les vidéos créées par ces comptes, conformément à nos règles en matière de spam._

|     |     |     |     |
| --- | --- | --- | --- |
| Réseaux d’opérations d’influence cachées identifiés et supprimés au troisième semestre 2022 | Source de la détection | Comptes du réseau | Abonnés du réseau |
| Ce réseau semblait opérer depuis la Russie et cibler les pays européens, principalement l’Allemagne, l’Italie et le Royaume-Uni. Les personnes derrière ces comptes ont créé de faux comptes localisés et ont partagé du contenu en allemand, en italien et en anglais grâce à la synthèse vocale, présentant un fort point de vue pro-russe ciblant des discours sur la guerre en Ukraine. | Interne | 1 686 | 133 564 |
| Ce réseau semblait opérer depuis Taïwan et cibler principalement les audiences taïwanaises. Les personnes derrière ces comptes ont utilisé de faux comptes pour écrire de nombreux commentaires désobligeants et partager du contenu en chinois traditionnel ciblant les discours civiques à Taïwan. | Interne | 59  | 60 765 |
| Ce réseau semblait opérer depuis le Kenya et cibler principalement les audiences du Kenya. Les personnes derrière ces comptes ont partagé du contenu en anglais via de faux comptes et ont utilisé des comportements frauduleux pour toucher une audience, notamment la manipulation des médias, ciblant des discours relatifs aux élections au Kenya. | Externe | 14  | 50 200 |
| Ce réseau semblait principalement opérer depuis l’Espagne et cibler les audiences des États-Unis. Les personnes derrière ces comptes ont créé et amplifié du contenu civique partisan des États-Unis en anglais et en espagnol via des comptes se définissant comme des partis politiques, redirigeant les utilisateurs vers des sites externes à la plateforme grâce à des liens de collecte de fonds, des liens de marchandise et des canaux d’information fermés, probablement dans le but de générer un bénéfice financier en ciblant des discours civiques aux États-Unis. | Interne | 104 | 1 731 144 |
| Ce réseau semblait opérer depuis la Géorgie et cibler les audiences russophones principalement basées au Kazakhstan, en Biélorussie et en Ukraine. Les personnes derrière ces comptes ont partagé du contenu en russe via de faux comptes se présentant comme de nouveaux points de vente, présentant un fort point de vue pro-russe ciblant des discours sur la guerre en Ukraine. | Interne | 18  | 85 068 |

**Définitions**

* **Les réseaux ont opéré depuis :** indique l’emplacement géographique des opérations du réseau selon des preuves techniques et comportementales récoltées depuis des sources privées et ouvertes. Il est possible que TikTok ne soit pas en mesure de relier ces réseaux à des entités, des individus ou des groupes définis.
* **Source de la détection :** considérée comme interne quand la présence de l’activité a été identifiée uniquement via une enquête menée en interne. La détection externe renvoie à des enquêtes qui ont débuté avec un rapport externe, ayant ensuite entraîné une enquête.
* **Abonnés du réseau :** nombre total cumulé des comptes abonnés aux comptes du réseau au moment de la suppression du réseau.

#### **Application des règles concernant les contenus publicitaires**

_REMARQUE : Les contenus peuvent être supprimés individuellement ou en bloc en appliquant des mesures à l’ensemble d’un compte d’annonceur._

### **Autres rapports**

* [Rapport sur les demandes de retrait émanant de gouvernements](https://www.tiktok.com/transparency/fr-fr/government-removal-requests)
* [Rapport de demandes de retrait de contenus pour infractions aux lois sur la propriété intellectuelle](https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests-2021-1/)
* [Rapport sur les demandes de renseignements](https://www.tiktok.com/transparency/fr-fr/information-requests-2021-1/)

#### Cela a-t-il été utile ?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Oui![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)Non

# Resource URL: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2022-2/
Application des Règles communautaires

_1er avril 2022 – 30 juin 2022_  
_Publication du 28 septembre 2022_

### À propos de ce rapport

TikTok est une plateforme mondiale de divertissement qui repose sur la créativité de sa communauté. Nous mettons tout en œuvre pour favoriser un environnement inclusif où les utilisateurs peuvent laisser libre cours à leur créativité, avoir le sentiment d’appartenir à une communauté et se divertir. Pour préserver cet environnement, nous agissons lorsqu’un contenu ou un compte vient enfreindre nos Règles Communautaires ou nos Conditions de service. Par ailleurs, nous publions régulièrement des informations sur les mesures prises afin de rendre compte à notre communauté.

TikTok a recours à des technologies innovantes ainsi qu’à des modérateurs pour identifier, examiner et agir sur les contenus qui enfreignent ses règles. Ce rapport fournit un aperçu trimestriel du volume et de la nature des contenus et des comptes supprimés de notre plateforme.

### Analyse

Les Règles Communautaires de TikTok sont définies pour favoriser une expérience divertissante qui privilégie la sécurité, l’inclusion et l’authenticité. Nos règles s’appliquent à tous et à tous les contenus, et nous nous efforçons d’être cohérents et équitables dans leur application. Cette analyse propose des éléments contextuels dont le but est de faciliter l’appréciation des données de ce rapport.

### Sécurité

TikTok associe technologie et intervention humaine pour faire respecter ses Règles Communautaires et préserver une communauté accueillante et bienveillante. Pour y parvenir efficacement à grande échelle, nous continuons d’investir dans les technologies de signalement et de modération. Lorsque nos logiciels permettent de détecter avec fiabilité tout contenu illicite, nous utilisons des dispositifs de modération automatisés afin de pouvoir supprimer rapidement les contenus qui enfreignent nos politiques. Nos capacités en matière de détection et de protection se sont améliorées en conséquence.

L’utilisation des technologies de machine learning a été déterminant dans la lutte contre la circulation de fausses informations. Nous avons amélioré les capacités d’adaptation de nos systèmes pour faire face au caractère imprévisible et en constante évolution des contenus de désinformation, en particulier lors de crises ou d’événements exceptionnels (par exemple, pendant la guerre en Ukraine ou à l’occasion d’une élection). Nous avons également développé plus encore nos capacités à détecter les contenus audiovisuels avérés trompeurs afin de limiter la manipulation des contenus sur la plateforme. Au vu de ces investissements, nous enregistrons ce trimestre des améliorations concernant l’application de nos politiques d’intégrité et d’authenticité : la suppression proactive des vidéos progresse, passant de 83,6 % au premier trimestre à 89,1 % au deuxième trimestre ; la suppression des vidéos sans aucune lecture s’améliore, passant de 60,8 % au premier trimestre à 74,7 % au deuxième trimestre ; enfin, la suppression des vidéos en moins de 24 heures est passée de 71,9 % à 83,9 %.

### Sûreté

Nous continuons à développer et à adapter nos mesures de protection en investissant dans des systèmes automatisés pour détecter, bloquer et supprimer les comptes et les interactions artificielles, et en améliorant notre temps de réaction et notre réponse à l’évolution des menaces. Au deuxième trimestre 2022, les attaques contre nos systèmes ont entraîné une augmentation du volume total de faux abonnés supprimés. Nous avons mis en place des mesures de protection sur nos systèmes pour empêcher les acteurs malveillants de comprendre nos capacités de détection. Cela s’est traduit par une diminution des comptes de spam bloqués à l’inscription et une augmentation des faux comptes supprimés.

### Contenus publicitaires

TikTok a mis en place des politiques strictes pour protéger les utilisateurs contre les contenus faux, frauduleux ou trompeurs, y compris les contenus publicitaires. Les comptes des annonceurs et le contenu des annonces sont soumis à ces politiques et doivent respecter nos Règles Communautaires, nos Règles publicitaires et nos Conditions d’utilisation. Au cours du deuxième trimestre 2022, le volume total d’annonces supprimées pour non-respect de nos politiques et directives publicitaires a diminué. Cela est dû en partie à nos efforts pour renforcer les stratégies de détection et de gestion des comptes frauduleux, qui contribuent à améliorer notre écosystème publicitaire et à créer de meilleures expériences pour les utilisateurs et les annonceurs. Nous nous engageons à créer un environnement sûr et positif pour nos utilisateurs, et nous révisons et renforçons régulièrement nos systèmes pour combattre les publicités qui transgressent nos règles.

### Données les plus récentes

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Télécharger](https://sf16-va.tiktokcdn.com/obj/eden-va2/nuvlojeh7ryht/Transparency_CGE_2022Q2/French_CGE_2022Q2.xlsx)

#### **Nombre total de vidéos supprimés / total de vidéos par trimestre**

_REMARQUE : le nombre total de vidéos supprimées représente environ 1 % de toutes les vidéos téléchargées sur TikTok._

#### **Nombre total de vidéos supprimées / remises en ligne, par type et trimestre**

_REMARQUE : les vidéos remises en ligne sont prises en compte à la fois dans les volumes de suppression et les volumes de remise en ligne dans le tableau ci-dessus._

#### **Nombre total de suppressions de vidéos, par règle et politique**

_REMARQUE : ce graphique montre le volume de vidéos supprimées par infraction. Une vidéo peut enfreindre plusieurs règles et politiques, ainsi le graphique indique chaque infraction. Dans certains cas rares, comme des situations d’urgence ou des pannes matérielles, la catégorie d’infraction d’une vidéo supprimée peut ne pas être enregistrée. Ces vidéos ne sont pas représentées dans le graphique ci-dessus, mais sont comptabilisées en chiffres absolus tout au long de ce rapport._

#### **Taux de suppression, par trimestre / règle**

_REMARQUE : la suppression proactive se caractérise par l’identification et le retrait d’une vidéo avant qu’elle ne soit signalée. La suppression dans les 24 heures signifie que l’on retire la vidéo dans les 24 heures suivant sa mise en ligne sur la plateforme._

#### **Nombre total de suppressions et taux de vidéos, par sous-règle et politique**

_REMARQUE : Seules les vidéos qui ont été vérifiées par des modérateurs sont incluses dans le tableau de bord des différentes règles et dispositions. Nos règles de sécurité concernant les mineurs ont pour but de fournir un niveau supérieur de sécurité et de bien-être aux adolescents. La disposition « nudité et activité sexuelle impliquant des mineurs » interdit un grand nombre de contenus, notamment « les mineurs peu vêtus » et « les danses sexuellement explicites » ; ces deux catégories représentent la majorité des contenus supprimés conformément à cette disposition. Les contenus pédopornographiques (CSAM) sont signalés séparément._

#### **Volume et taux de suppression, par pays**

_REMARQUE : ce graphique dresse la liste des trente pays avec le plus grand nombre de vidéos supprimées et qui représentent environ 80 % du volume total des suppressions._

#### **Nombre total de comptes supprimés, par trimestre et par motif**

_REMARQUE : en plus du retrait des comptes qui enfreignent nos Règles Communautaires, nous procédons à la suppression des comptes dont les activités de spam sont avérées, ainsi que les vidéos de spam publiées par ces comptes. Nous prenons également des mesures proactives pour empêcher la création de comptes de spam par des moyens automatisés._

#### **Fausse activité**

_REMARQUE : nous prenons des mesures pour supprimer et empêcher les « j’aime », les « abonnés » et les demandes « d’abonnement » si nous estimons que l’activité est automatisée ou frauduleuse._

#### **Activité des comptes spam**

_REMARQUE : lorsque nous supprimons des comptes pour cause de spam, nous supprimons également les vidéos créées par ces comptes, conformément à nos règles en matière de spam._

#### **Application des règles concernant les contenus publicitaires**

_REMARQUE : en raison des changements que nous avons apportés à la gestion des infractions publicitaires et du renforcement de nos capacités de contrôle des comptes, le nombre total de suppressions de contenus publicitaires a augmenté au cours du premier semestre 2022. Les contenus peuvent être supprimés individuellement ou en bloc en appliquant des mesures à l’ensemble d’un compte d’annonceur._

### **Autres rapports**

* [Rapport sur les demandes de retrait émanant de gouvernements](https://www.tiktok.com/transparency/fr-fr/government-removal-requests)
* [Rapport de demandes de retrait de contenus pour infractions aux lois sur la propriété intellectuelle](https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests-2021-1/)
* [Rapport sur les demandes de renseignements](https://www.tiktok.com/transparency/fr-fr/information-requests-2021-1/)

#### Cela a-t-il été utile ?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Oui![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)Non

# Resource URL: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2022-1/
Application des Règles communautaires

_1er janvier – 31 mars 2022_  
_Publication du 30 juin 2022_

### À propos de ce rapport

TikTok est une plateforme mondiale de divertissement qui repose sur la créativité de sa communauté. Nous mettons tout en œuvre pour favoriser un environnement inclusif où les utilisateurs peuvent laisser libre cours à leur créativité, avoir le sentiment d’appartenir à une communauté et se divertir. Pour préserver cet environnement, nous agissons lorsqu’un contenu ou un compte vient enfreindre nos Règles Communautaires ou nos Conditions de service. Par ailleurs, nous publions régulièrement des informations sur les mesures prises afin de rendre compte à notre communauté.

TikTok a recours à des technologies innovantes ainsi qu’à des modérateurs pour identifier, examiner et agir sur les contenus qui enfreignent ses règles. Ce rapport fournit un aperçu trimestriel du volume et de la nature des contenus et des comptes supprimés de notre plateforme.

### Analyse

Les Règles Communautaires de TikTok sont définies pour favoriser une expérience divertissante qui privilégie la sécurité, l’inclusion et l’authenticité. Nos règles s’appliquent à tous et à tous les contenus, et nous nous efforçons d’être cohérents et équitables dans leur application.

#### Réponse à la guerre en Ukraine

En tant que plateforme, notre réponse à la guerre en Ukraine a été de renforcer les mesures de protection et de sécurité afin de garantir que les utilisateurs puissent s’exprimer et partager leurs expériences. Aussi, nous nous efforçons de combattre les fausses informations qui portent préjudice et toute autre violation de nos politiques. La guerre nous a imposé de faire face à un monde complexe et en constante évolution. Nous avons ainsi augmenté nos investissements auprès de nos partenaires de vérification des faits et qui contribuent à évaluer l’exactitude des contenus. Nous avons adapté nos méthodes de détection afin de prendre des mesures plus rapides contre les comptes qui tentent d’escroquer ou d’induire en erreur notre communauté par le biais de Lives (vidéos en direct) et de contenus vidéo non originaux. Par ailleurs, nous avons lancé un projet pilote de notre politique relative aux médias contrôlés par un État en désignant par des étiquettes le contenu de certains comptes afin d’apporter des éléments de contexte importants à nos utilisateurs. En réponse à la loi russe sur les « fake news » et de ses conséquences sur la sécurité, nous n’avons eu d’autre choix, à partir du 6 mars, que de suspendre les diffusions en direct et les nouveaux contenus sur notre service vidéo en Russie. La sécurité de nos employés et de nos créateurs est notre priorité absolue. C’est pourquoi, les contenus diffusés par des comptes établis en dehors de la Russie ne sont actuellement pas accessibles en Russie.

Du 24 février au 31 mars 2022, nous avons pris les mesures suivantes pour protéger notre communauté :

* Notre équipe de sécurité, mobilisée sur la guerre en Ukraine, a retiré 41 191 vidéos, dont 87 % enfreignaient nos politiques de lutte contre la désinformation. La grande majorité (78 %) a été identifiée de manière proactive.
* Nos partenaires de vérification des faits ont contribué à l’analyse de 13 738 vidéos dans le monde.
* Nous avons ajouté des avertissements sur 5 600 vidéos pour signaler aux utilisateurs que le contenu ne pouvait pas être validé par nos partenaires de vérification.
* Nous avons identifié au moyen d’étiquettes le contenu de 49 comptes de médias contrôlés par l’État russe.
* Nous avons identifié et supprimé 6 réseaux et 204 comptes dans le monde pour cause de tentative coordonnée d’influencer l’opinion publique et de tromper les utilisateurs sur leur identité.

#### Contenus publicitaires

TikTok a mis en place des politiques strictes pour protéger les utilisateurs contre les contenus faux, frauduleux ou trompeurs, y compris les contenus publicitaires. Les comptes des annonceurs et le contenu des annonces sont soumis à ces politiques et doivent respecter nos Règles Communautaires, nos Règles publicitaires et nos Conditions d’utilisation. Au cours du premier trimestre 2022, le volume total d’annonces supprimées pour non-respect de nos politiques et directives publicitaires a augmenté. Cela est dû à l’évolution de notre approche en matière d’application des règles relatives aux publicités et au renforcement de nos capacités de contrôle des comptes. Auparavant, tout compte jugé contraire à nos politiques entraînait le blocage de tout le trafic publicitaire associé à ce compte. Désormais, avec notre nouvelle approche, nous sommes en mesure de supprimer de nos systèmes internes toutes les publicités associées à ces comptes. Bien que le résultat final soit le même pour nos utilisateurs, nous estimons que cette mesure améliore l’écosystème publicitaire et témoigne de notre volonté de créer un environnement sûr et positif pour nos utilisateurs.

### Données les plus récentes

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Pobierz](https://sf16-va.tiktokcdn.com/obj/eden-va2/uhaeh7pflk/Transparency_CGE_2022_Q1/French_CGE_2022Q1.xlsx)

#### Nombre total de vidéos supprimés / total de vidéos par trimestre

_REMARQUE : le nombre total de vidéos supprimées représente environ 1 % de toutes les vidéos téléchargées sur TikTok._

#### Nombre total de vidéos supprimées/remises en ligne, par type et trimestre

_REMARQUE : les vidéos rétablies sont prises en compte à la fois dans les volumes de suppression et les volumes de remise en ligne dans le tableau ci-dessus._

#### Nombre total de suppressions de vidéos, par règle et politique

_REMARQUE : ce graphique montre le volume de vidéos supprimées par infraction. Une vidéo peut enfreindre plusieurs règles et politiques, ainsi le graphique indique chaque infraction. Dans certains cas rares, comme des situations d’urgence ou des pannes matérielles, la catégorie d’infraction d’une vidéo supprimée peut ne pas être enregistrée. Ces vidéos ne sont pas représentées dans le graphique ci-dessus, mais sont comptabilisées en chiffres absolus tout au long de ce rapport._

#### Nombre total de suppressions et taux de vidéos, par sous-règle et politique

_REMARQUE : Seules les vidéos qui ont été vérifiées par des modérateurs sont incluses dans le tableau de bord des différentes règles et dispositions. Nos règles de sécurité concernant les mineurs ont pour but de fournir un niveau supérieur de sécurité et de bien-être aux adolescents. La disposition « nudité et activité sexuelle impliquant des mineurs » interdit un grand nombre de contenus, notamment « les mineurs peu vêtus » et « les danses sexuellement explicites » ; ces deux catégories représentent la majorité des contenus supprimés conformément à cette disposition. Les contenus pédopornographiques (CSAM) sont signalés séparément._

#### Taux de suppression, par trimestre/règle

_REMARQUE : la suppression proactive se caractérise par l’identification et le retrait d’une vidéo avant qu’elle ne soit signalée. La suppression dans les 24 heures signifie que l’on retire la vidéo dans les 24 heures suivant sa mise en ligne sur la plateforme._

#### Volume et taux de suppression, par pays

_REMARQUE : ce graphique dresse la liste des trente pays avec le plus grand nombre de vidéos supprimées et qui représentent environ 80 % du volume total des suppressions._

#### Nombre total de comptes supprimés, par trimestre et par motif

_REMARQUE : en plus du retrait des comptes qui enfreignent nos Règles Communautaires, nous procédons à la suppression des comptes dont les activités de spam sont avérées, ainsi que les vidéos de spam publiées par ces comptes. Nous prenons également des mesures proactives pour empêcher la création de comptes de spam par des moyens automatisés._

#### Fausse activité

_REMARQUE : nous prenons des mesures pour supprimer et empêcher les « j’aime », les « suiveurs » et les demandes de « suivi » si nous estimons que l’activité est automatisée ou frauduleuse._

#### Activité des comptes spam

_REMARQUE : lorsque nous supprimons des comptes pour cause de spam, nous supprimons également les vidéos créées par ces comptes, conformément à nos règles en matière de spam._

#### Application des règles concernant les contenus publicitaires

_REMARQUE : en 2022, le volume total d’annonces supprimées a augmenté en raison de l’évolution de notre approche en matière d’application des règles relatives aux publicités et au renforcement de nos capacités de contrôle au niveau des comptes. Le retrait peut se faire de deux façons : l’annonce particulière est supprimée, ou la suppression se fait en bloc par une action appliquée à l’intégralité du compte de l’annonceur._

### Autres rapports

* [Rapport sur les demandes de retrait émanant de gouvernements](https://www.tiktok.com/transparency/fr-fr/government-removal-requests)
* [Rapport de demandes de retrait de contenus pour infractions aux lois sur la propriété intellectuelle](https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests-2021-1/)
* [Rapport sur les demandes de renseignements](https://www.tiktok.com/transparency/fr-fr/information-requests-2021-1/)

#### Udało Ci się uzyskać poszukiwaną pomoc?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Tak![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)Nie

# Resource URL: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2021-4/
Application des Règles communautaires

_1er octobre 2021 – 31 décembre 2021_  
_Publication du 13 avril 2022_

### À propos de ce rapport

TikTok est une plateforme mondiale de divertissement qui repose sur la créativité de ses communautés. Nous mettons tout en œuvre pour favoriser un environnement inclusif où les utilisateurs peuvent laisser libre cours à leur créativité, avoir le sentiment d’appartenir à une communauté et se divertir. Pour préserver cet environnement, nous agissons lorsqu’un contenu ou un compte vient enfreindre nos Règles Communautaires ou nos Conditions de service ; par ailleurs, nous publions régulièrement des informations sur les mesures prises par notre société de manière à rendre compte à notre communauté.

TikTok a recours à des technologies innovantes ainsi qu’à des modérateurs pour identifier, examiner et agir sur les contenus qui enfreignent ses règles. Ce rapport fournit un aperçu trimestriel du volume et de la nature des contenus et des comptes supprimés de notre plateforme.

### Analyse

Les Règles communautaires de TikTok sont conçues pour favoriser une expérience qui privilégie la sécurité, l’inclusion et l’authenticité. Nos règles s’appliquent à tous et à tous les contenus, et nous nous efforçons d’être cohérents et équitables dans leur application.

#### Sécurité

Nous associons technologie et intervention humaine pour identifier et supprimer les infractions aux Règles communautaires, et nous investissons continuellement dans nos politiques, nos produits et nos partenariats pour renforcer la sécurité de l’ensemble de notre communauté et de notre plateforme. Au quatrième trimestre de 2021, nous avons continué à améliorer nos techniques de modération par apprentissage automatique (machine learning), ce qui a permis d’améliorer la pertinence et la précision de nos systèmes de suppression automatisés.

Grâce aux améliorations continues de la reconnaissance et de la détection audio et vocale, nous avons perfectionné de manière significative la détection proactive et la suppression des contenus liés au suicide, à l’automutilation, aux actes dangereux, aux comportements haineux ou constitutifs de harcèlement et d’intimidation. Par ailleurs, l’optimisation continue de nos modèles de détection relatifs à la sécurité des mineurs a permis d’améliorer l’efficacité de notre dispositif de modération. Le nombre de vidéos rétablies après avoir fait l’objet d’un appel a diminué par rapport au trimestre précédent, ce que nous attribuons à une réduction des faux positifs et à une amélioration de la précision de notre système de détection et de suppression automatique.

#### Sûreté

Nous continuons à développer et à adapter nos mesures de protection en investissant dans des systèmes automatisés pour détecter, bloquer et supprimer les comptes et les interactions artificielles, et en améliorant notre temps de réaction et notre réponse à l’évolution des menaces. Au quatrième trimestre 2021, nous avons développé des systèmes de surveillance supplémentaires et amélioré les plateformes et processus existants de traitement des tickets d’assistance technique. Cela a renforcé notre capacité à détecter la création de comptes d’utilisateurs frauduleux lors de l’inscription et à supprimer les faux suiveurs, les faux « j’aime » et les vidéos associées à ces comptes.

#### Contenus publicitaires

TikTok a mis en place des politiques strictes pour protéger les utilisateurs contre les contenus mensongers, frauduleux ou trompeurs, y compris les contenus publicitaires. Les comptes des annonceurs et le contenu des annonces sont soumis à ces politiques et doivent respecter nos Règles communautaires, nos Règles publicitaires et nos Conditions d’utilisation. Malgré une augmentation globale des publicités publiées sur TikTok au cours du quatrième trimestre 2021, le volume total d’annonces supprimées pour non-respect de nos politiques et directives publicitaires a diminué. Cela est dû en partie aux efforts de plusieurs équipes consacrées à l’amélioration de notre écosystème publicitaire et qui contribuent à créer de meilleures expériences pour les utilisateurs et les annonceurs sur notre plateforme. Nous nous engageons à créer un environnement sûr et positif pour nos utilisateurs, et nous révisons et renforçons régulièrement nos systèmes pour combattre les publicités qui transgressent nos règles.

### Données les plus récentes

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Télécharger](https://sf16-va.tiktokcdn.com/obj/eden-va2/uhaeh7pflk/Transparency_CGE_Q4/French.xlsx)

#### Nombre total de vidéos supprimées/remises en ligne, par type et trimestre

_REMARQUE : le nombre total de vidéos supprimées représente environ 1 % de toutes les vidéos téléchargées sur TikTok._

#### Nombre total de suppressions de vidéos, par règle

_REMARQUE : ce graphique montre le volume de vidéos supprimées par infraction. Une vidéo peut enfreindre plusieurs règles et politiques, aussi le graphique indique chaque infraction. Dans certains cas rares, comme des situations d’urgence ou des pannes matérielles, il peut arriver que la catégorie d’infraction d’une vidéo supprimée ne soit pas enregistrée. Ces vidéos ne sont pas représentées dans le graphique ci-dessus, mais sont comptabilisées en chiffres absolus tout au long de ce rapport._

#### Taux de suppressions, par trimestre/règle

_REMARQUE : la suppression proactive se caractérise par l’identification et le retrait d’une vidéo avant qu’elle ne soit signalée. La suppression dans les 24 heures signifie que l’on retire la vidéo dans les 24 heures suivant sa mise en ligne sur la plateforme._

#### Suppressions par pays

|     |     |
| --- | --- |
| **Pays / Marché** | **1er octobre 2021 – 31 décembre 2021** |
| États-Unis | 12 943 295 |
| Indonésie | 6 657 310 |
| Pakistan | 6 563 594 |
| Russie | 5 977 996 |
| Philippines | 5 479 091 |
| Brésil | 3 941 136 |
| Bangladesh | 2 636 372 |
| Mexique | 2 567 999 |
| Vietnam | 1 995 738 |
| Royaume-Uni | 1 963 799 |

_REMARQUE : ce graphique dresse la liste des dix marchés avec le plus grand nombre de vidéos supprimées._

#### Nombre total de comptes supprimés, par trimestre et par motif

_REMARQUE : en plus du retrait des comptes qui enfreignent les Règles communautaires, nous procédons à la suppression descomptes dont les activités de spam sont avérées, ainsi que les vidéos de spam publiées par ces comptes. Nous prenons également des mesures proactives pour empêcher la création de comptes de spam par des moyens automatisés._

#### Fausse activité

|     |     |
| --- | --- |
|     | **1er octobre 2021 – 31 décembre 2021** |
| Comptes spam évités | 152 118 769 |
| Suppressions de vidéos publiées par des comptes spam | 46 382 130 |
| Suppressions de faux abonnés | 441 985 135 |
| Demandes de suivi par de faux comptes évitées | 2 794 306 196 |
| Suppressions de faux « j’aime » | 285 323 840 |
| Faux « j’aime » évités | 11 917 679 868 |

#### Application des règles concernant les contenus publicitaires

|     |     |
| --- | --- |
|     | **1er octobre 2021 – 31 décembre 2021** |
| Total des publicités supprimées | 3 181 858 |

### Autres rapports

* [Rapport sur les demandes de retrait émanant de gouvernements](https://www.tiktok.com/transparency/fr-fr/government-removal-requests)
* [Rapport de demandes de retrait de contenus pour infractions aux lois sur la propriété intellectuelle](https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests-2021-1/)
* [Rapport sur les demandes de renseignements](https://www.tiktok.com/transparency/fr-fr/information-requests-2021-1/)

#### Cela a-t-il été utile ?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Oui![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)Non

# Resource URL: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2021-3/
Application des Règles communautaires

_1er juillet – 30 septembre 2021  
__Publication du 8 février 2022_

### À propos de ce rapport

TikTok est une plateforme mondiale de divertissement qui repose sur la créativité de sa communauté plurielle. Nous mettons tout en œuvre pour favoriser un environnement inclusif où les utilisateurs peuvent laisser libre cours à leur créativité, avoir le sentiment d’appartenir à une communauté et se divertir. Pour préserver cet environnement, nous agissons lorsqu’un contenu ou un compte vient enfreindre nos Règles communautaires ou nos Conditions d’utilisation ; par ailleurs, nous publions régulièrement des informations sur les mesures prises par notre société de manière à rendre compte à notre communauté et gagner sa confiance. TikTok a recours à des technologies innovantes ainsi qu’à des modérateurs pour identifier, examiner et agir sur les contenus qui enfreignent ses règles.

Ce rapport fournit un aperçu trimestriel du volume et de la nature des contenus et des comptes supprimés de notre plateforme.

### Données les plus récentes

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Télécharger](https://sf16-va.tiktokcdn.com/obj/eden-va2/uhaeh7pflk/Transparency_CGE_2021_Q3/French_CGE_2021Q3.xlsx)

#### Nombre total de vidéos supprimées/remises en ligne, par type et par trimestre

_REMARQUE : Le total de videos supprimées représente approximativement 1% de l’ensemble des vidéos publiées sur TikTok._

#### Nombre total de vidéos supprimées par catégorie de règle

_REMARQUE : Ce graphique montre le volume de vidéos supprimées par règle enfreinte. Une vidéo peut enfreindre plusieurs règles et chaque violation est comptabilisée._

#### Taux de suppressions, par trimestre/règle

_REMARQUE : lasuppression proactive signifie qu’une vidéo eninfraction est identifiée etsupprimée avant qu’elle ne nous soitsignalée. La suppression dans les 24heures signifie que la vidéoest supprimée dans les 24 heures suivant sapublication sur notre plateforme._

#### Suppressions par pays

|     |     |
| --- | --- |
| **Pays / Marché** | **1er juillet – 30 septembre 2021** |
| Etats- Unis | 13,918,537 |
| Russie | 6,881,904 |
| Indonésie | 6,173,414 |
| Pakistan | 6,019,754 |
| Brésil | 5,906,859 |

_REMARQUE : Ce tableau mentionne les cinq marchés avec le plus grand nombre de vidéos supprimées._

#### Nombre total de comptes supprimés, par trimestre et par motif

 _REMARQUE : nousprocédons à la suppression des comptes qui enfreignent les Règlescommunautaires et les Conditions d’utilisation, y compris les comptes dont lesactivités de spam sont avérées, ainsi que les vidéos de spam publiées par cescomptes. Nous prenonségalement des mesures proactives pour empêcher lacréation de comptes spam par des moyens automatisés._

#### Fausse activité

|     |     |
| --- | --- |
|     | **1er juillet – 30 septembre 2021** |
| Comptes spam évités | 226,557,055 |
| Suppressions de vidéos publiées par des comptes spam | 11,895,555 |
| Suppressions de faux abonnés | 231,150,220 |
| Demandes de suivi par de faux comptes évitées | 2,078,453,724 |
| Suppressions de faux « j’aime » | 203,708,379 |
| Faux « j’aime » évités | 16,594,976,202 |

#### Application des règles concernant les contenus publicitaires

TikTok a mis en place des politiques strictes pour protéger les utilisateurs contre les contenus publicitaires mensongers, frauduleux ou trompeurs, y compris les publicités. Les comptes d’annonceurs et les contenus publicitaires sont soumis à ces règles et doivent se conformer à nos Règles Communautaires, nos Règles Publicitaires ainsi qu’à nos Conditions d’utilisation. Nous nous engageons à créer un environnement sûr et positif pour nos utilisateurs, et nous examinons et renforçons régulièrement nos systèmes pour lutter contre les publicités qui transgressent nos règles.

|     |     |
| --- | --- |
|     | **1er juillet – 30 septembre 2021** |
| Total des publicités supprimées | 3,392,630 |

#### COVID-19

|     |     |
| --- | --- |
| **Application des Règles communautaires** | **1er juillet – 30 septembre 2021** |
| Suppressions de vidéos pour désinformation sur la COVID-19 et la vaccination | 46,577 |
| Taux de suppression proactive | 82.86% |
| Taux de suppression à zéro vue | 69.42% |
| Taux de suppression dans les 24 heures | 84.62% |

|     |     |
| --- | --- |
| **Connexions à des sources d’information fiables** | **1er juillet – 30 septembre 2021** |
| Visites du [centre d’information TikTok dédié à la COVID-19](https://www.tiktok.com/safety/fr-fr/covid-19/) | 2,883,650 |
| Ajouts de bannières COVID-19 aux vidéos | 3,560,364 |
| Nombre de bannières sur la COVID-19 visualisées | 22,631,317,279 |
| Nombre de messages d’intérêt public visualisés du hashtag COVID-19 | 31,007,383,296 |

### Autres rapports

* [Rapport sur les demandes de retrait émanant de gouvernements](https://www.tiktok.com/transparency/fr-fr/government-removal-requests)
* [Rapport sur les demandes de renseignements](https://www.tiktok.com/transparency/fr-fr/information-requests)
* [Rapport de demandes de retrait de contenus pour infractions aux lois sur la propriété intellectuelle](https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests)

#### Cela a-t-il été utile ?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Oui![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)Non

# Resource URL: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2021-2/
Application des Règles communautaires

_1e avril 2021 – 30 juin 2021  
publication du 13 octobre 2021_

### À propos de ce rapport

TikTok est la plateforme qui permet, où que l’on se trouve dans le monde, de se divertir grâce à des contenus authentiques et créatifs. Nous nous efforçons de favoriser une expérience sûre et inclusive pour les créateurs ainsi que les utilisateurs en prenant des mesures contre les contenus ou les comptes qui enfreignent nos [Règles Communautaires](https://www.tiktok.com/community-guidelines?lang=fr).

Le présent Rapport sur l’application des [Règles Communautaires](https://www.tiktok.com/community-guidelines?lang=fr) met en évidence le contenu et les comptes supprimés au cours du second trimestre 2021 (avril-juin) pour avoir enfreint nos règles communautaires. Nous publions également des informations supplémentaires sur le volume de fausses activités auxquelles nous avons mis fin ou supprimées dans le cadre de nos efforts pour préserver l’authenticité de TikTok. En outre, ce rapport comprend désormais les chiffres relatifs aux contenus supprimés avant d’avoir été visionnés.

### Rapport sur les données

#### Vidéos

D’avril à juin 2021, 81 518 334 vidéos ont été supprimées dans le monde pour avoir enfreint nos Règles Communautaires ou nos conditions d’utilisation, soit moins de 1 % de toutes les vidéos mises en ligne. Parmi ces vidéos, nous avons identifié et supprimé 93,0 % d’entre elles dans les 24 heures suivant leur publication et 94,1 % avant qu’un utilisateur ne les signale. 87,5 % des contenus supprimés n’ont pas été vus, ce qui représente une amélioration par rapport au premier trimestre de cette année (81,8 %), et ce grâce au renforcement des systèmes de détection. Nous avons également continué à déployer sur d’autres marchés une technologie qui détecte et supprime automatiquement certaines catégories de contenu non conforme. Ainsi, 16 957 950 des suppressions ont été traitées par cette technologie.

Ce graphique montre les cinq marchés ayant les plus gros volumes de vidéos supprimées.

|     |     |
| --- | --- |
| **Pays** | **Total des retraits** |
| États-Unis | 11,431,198 |
| Pakistan | 9,851,404 |
| Brésil | 7,488,608 |
| Russie | 4,759,623 |
| Indonésie | 4,743,066 |

TikTok offre aux utilisateurs la possibilité de faire appel lors de la suppression de leur vidéo et s’engage à rétablir le contenu qui aurait été supprimé par erreur. D’avril à juin, 4 663 387 vidéos ont été rétablies après avoir fait l’objet d’un appel. Nous nous efforçons d’être cohérents et équitables dans nos pratiques de modération et de réduire les faux positifs grâce à des formations continues sur les politiques et à des contrôles de qualité.

Le graphique suivant montre le volume de vidéos supprimées pour avoir enfreint des règles. Veuillez noter qu’une vidéo peut enfreindre plusieurs règles et que chaque violation est indiquée.

Le graphique suivant montre le taux de suppression des vidéos pour avoir enfreint nos politiques. La suppression proactive signifie qu’une vidéo en infraction est identifiée et supprimée avant qu’elle ne nous soit signalée. La suppression dans les 24 heures signifie que la vidéo est supprimée dans les 24 heures suivant sa publication sur notre plateforme. La suppression à zéro vue signifie que la vidéo est supprimée après avoir été publiée, mais avant que quiconque ne l’ait vue.

|     |     |     |     |
| --- | --- | --- | --- |
|     | **Taux de suppression proactive** | **Taux de suppression dans les 24 heures** | **Taux de suppression à zéro vue** |
| Nudité adulte et activités sexuelles | 90.30% | 90.00% | 78.50% |
| Harcèlement et intimidation | 73.30% | 83.80% | 61.40% |
| Comportement haineux | 72.90% | 80.80% | 60.60% |
| Activités illégales et biens réglementés | 97.10% | 95.70% | 92.30% |
| Intégrité et authenticité | 88.30% | 86.20% | 67.90% |
| Sécurité des mineurs | 97.60% | 95.40% | 93.90% |
| Suicide, automutilation et actes dangereux | 94.20% | 90.80% | 81.80% |
| Contenu violent et choquant | 94.90% | 94.30% | 86.60% |
| Extrémisme violent | 89.40% | 90.10% | 79.50% |

[Nudité adulte et activités sexuelles](https://www.tiktok.com/community-guidelines?lang=fr#30)  
La nudité, le contenu sexuellement explicite et la pornographie ne sont pas autorisés sur TikTok. Sur l’ensemble des vidéos supprimées, 14 % étaient en infraction de cette règle. 90 % de ces vidéos ont été supprimées dans les 24 heures suivant leur publication, 78,5 % ont été supprimées à zéro vues, et 90,3 % ont été supprimées avant tout signalement.

[Harcèlement et intimidation](https://www.tiktok.com/community-guidelines?lang=fr#36)  
Nous souhaitons favoriser une communauté ouverte à l’expression créative et ne tolérons pas que les membres de notre communauté soient humiliés, intimidés ou harcelés. Sur l’ensemble des vidéos supprimées, 6,8 % violaient cette politique. 83,8 % de ces vidéos ont été supprimées dans les 24 heures suivant leur publication, 61,4 % ont été supprimées à zéro vues et 73,3 % ont été supprimées avant tout signalement.

[Comportement haineux](https://www.tiktok.com/community-guidelines?lang=fr#38)  
TikTok est une communauté diverse et inclusive. Les comportements haineux, y compris les discours de haine et les idéologies haineuses n’ont pas leur place sur notre plateforme. 2,2 % des vidéos supprimées ont violé cette politique. 80,8 % de ces vidéos ont été supprimées dans les 24 heures suivant leur publication, 60,6 % ont été supprimées à zéro vues, et 72,9 % ont été supprimées avant tout signalement. Il s’agit d’un domaine très complexe et nuancé dans lequel nous devons continuer à nous améliorer. Nous nous engageons à renforcer nos mécanismes de détection des discours haineux tout en améliorant notre capacité à modérer correctement les contre-discours et les réappropriations.

[Activités illégales et biens réglementés](https://www.tiktok.com/community-guidelines?lang=fr#32)  
Nous veillons à ce que TikTok ne facilite pas les activités criminelles ou la promotion ou le commerce de drogues, de tabac, d’alcool et d’autres substances contrôlées ou biens réglementés. Au cours du deuxième trimestre de cette année, 20,9% des vidéos supprimées étaient non conforme à cette règle. 95,7 % de ces vidéos ont été supprimées dans les 24 heures suivant leur publication, 92,3 % ont été supprimées à zéro vues et 97,1 % ont été supprimées avant tout signalement.

[Intégrité et authenticité](https://www.tiktok.com/community-guidelines?lang=fr#37)  
Nous estimons que la bonne foi constitue le socle de notre communauté, et de ce fait nous n’autorisons point les contenus ou les comptes qui impliquent un faux engagement, une usurpation d’identité voire une désinformation nuisible ou dangereuse. Parmi les vidéos supprimées, 0,8 % ont enfreint cette règle. 86,2 % de ces vidéos ont été supprimées dans les 24 heures suivant leur publication et 67,9 % ont été supprimées à zéro vues. 88,3 % ont été supprimées avant tout signalement.

[Sécurité des mineurs](https://www.tiktok.com/community-guidelines?lang=fr#31)  
Nous sommes profondément attachés à la sécurité des mineurs et renforçons régulièrement nos processus afin d’assurer leur sécurité. Au cours du deuxième trimestre de 2021, 41,3 % des contenus retirés enfreignaient notre politique de sécurité des mineurs. 95,4 % de ces vidéos ont été supprimées dans les 24 heures suivant leur mise en ligne, 93,9 % ont été supprimées à zéro vues et 97,6 % ont été supprimées avant tout signalement.

[Suicide, automutilation et actes dangereux](https://www.tiktok.com/community-guidelines?lang=fr#34)  
Nous tenons le bien-être des individus qui font partie de notre communauté à cœur et nous nous efforçons de créer un environnement de bienveillant. Parmi les vidéos supprimées d’avril à juin, 5,3 % étaient en infraction de cette règle. 90,8 % de ces vidéos ont été supprimées dans les 24 heures suivant leur publication, 81,8 % ont été supprimées à zéro vues. 94,2 % ont été supprimées avant tout signalement.

[Contenu violent et choquant](https://www.tiktok.com/community-guidelines?lang=fr#35)  
TikTok est une plateforme qui célèbre la créativité, mais pas les contenus choquants ou violents. Sur l’ensemble des vidéos supprimées, 7,7 % étaient en non-conformité avec cette règle. 94,3 % de ces vidéos ont été supprimées dans les 24 heures suivant leur publication, 86,6 % ont été supprimées à zéro vues, et 94,9 % ont été supprimées avant tout signalement.

[Extrémisme violent](https://www.tiktok.com/community-guidelines?lang=fr#39)  
TikTok interdit strictement l’extrémisme violent et nous nous efforçons de faire le nécessaire pour empêcher que notre plateforme soit utilisée pour menacer quiconque ou inciter à la violence. Sur l’ensemble des vidéos supprimées, 1,1% ont enfreint cette règle. 90,1 % de ces vidéos ont été supprimées dans les 24 heures suivant leur publication et 79,5 % ont été supprimées à zéro vues. 89,4 % ont été supprimées avant tout signalement.

#### Comptes

Au cours du deuxième trimestre de 2021, 14 871 412 comptes ont été supprimés pour non-respect de nos règles communautaires ou nos conditions d’utilisation. Ce chiffre inclut 11 205 597 comptes soupçonnés d’appartenir à des mineurs et qui ont été supprimés pour avoir potentiellement appartenu à une personne de moins de 13 ans, ce qui représente moins de 1 % de tous les comptes sur TikTok.

#### Fausse activité

Nous continuons à faire évoluer et à adapter nos mesures de protection en investissant dans des défenses automatisées pour détecter, bloquer et supprimer les comptes et les engagements inauthentiques, et en améliorant notre vitesse et notre réactivité par rapport aux menaces qui ne cessent d’évoluer. D’avril à juin, nous avons empêché la création de 148 759 987 faux comptes. Nous avons également supprimé 8 542 037 vidéos publiées par des comptes spam.

Nous avons également identifié et empêché 632 416 873 tentatives de demandes de suivi par de faux comptes et supprimé 71 935 583 comptes qui suivaient d’autres utilisateurs. Nous avons également empêché 9 612 942 242 tentatives de “likes” et corrigé 91 812 066 “likes” inauthentiques par ces comptes.

#### Contenus publicitaires

TikTok a mis en place des [politiques strictes](https://ads.tiktok.com/help/article?aid=10000962) pour protéger les utilisateurs contre les contenus publicitaires mensongers, frauduleux ou trompeurs. Les comptes d’annonceurs et les publicités payantes sont soumis à ces règles et doivent se conformer à nos Règles Communautaires, nos [Règles Publicitaires](https://ads.tiktok.com/help/article?aid=10000962) ainsi que nos conditions d’utilisation. Au cours du second trimestre 2021, nous avons rejeté 1 829 219 de contenus publicitaires pour infraction à nos politiques et règles en matière de publicité. Nous nous engageons à ce que nos utilisateurs aient une expérience positive, authentique et agréable lorsqu’ils regardent des publicités sur TikTok, à cet égard nous continuerons à investir non seulement dans nos ressources humaines mais également dans les moyens technologiques afin d’assurer un contenu publicitaire qui soit conforme à nos attentes.

### Rapport sur les données

#### Sécurité des plus jeunes

Afin de maintenir un environnement sûr et positif pour notre communauté, et en particulier pour les adolescents, nous veillons chaque jour à apprendre, adapter et renforcer nos politiques et pratiques. Nous pensons que nous sommes plus efficaces lorsque nous collaborons avec des pairs du secteur pour faire avancer ces efforts. Ainsi, en mai 2021, nous avons rejoint la [“Technology Coalition”](https://newsroom.tiktok.com/fr-fr/tiktok-rejoint-la-technology-coalition), une organisation qui œuvre à la protection des enfants contre l’exploitation et les abus sexuels en ligne, afin d’approfondir notre approche en matière d’intervention fondée sur des données probantes. Nous avons également expliqué les approches multiples que nous adoptons pour promouvoir des expériences adaptées à l’âge des utilisateurs et faire en sorte que l’expérience TikTok continue à être accessible aux à partir de 13 ans.

Au cours du second trimestre 2021, nous avons également actualisé notre Centre de sécurité. Nous sommes conscients des défis auxquels sont confrontés les parents à l’ère numérique et, dans le cadre de nos efforts pour soutenir et sensibiliser les familles sur notre plateforme, nous avons introduit le [Guide à destination des parents](https://www.tiktok.com/safety/fr-fr/guardians-guide/), un espace unique pour tout savoir sur TikTok. Vous y trouverez toutes les informations afin de débuter sur la plateforme, une présentation de nos outils et fonctionnalités de sécurité et de protection de la vie privée, tels que le [Mode Connexion Famille](https://newsroom.tiktok.com/fr-fr/de-nouvelles-ressources-sur-le-mode-connexion-famille-de-tiktok), et de nombreuses ressources pour répondre aux questions concernant la sécurité en ligne.

#### Bien-être

Chez TikTok, nous mettons l’accent sur le bien-être des utilisateurs afin que notre communauté puisse continuer à se développer, à s’épanouir et à surprendre le monde par sa créativité. Aussi, nous sommes toujours à la recherche de nouvelles façons de contribuer au bien-être de notre communauté. Au cours du second trimestre 2021, nous avons :

* Ajouté de nouveaux paramètres afin notamment de [pouvoir signaler ou supprimer plusieurs commentaires simultanément](https://newsroom.tiktok.com/fr-fr/de-nouveaux-outils-pour-lutter-contre-le-harcelement), et que les comptes qui publient des commentaires d’intimidation ou d’autres commentaires négatifs puissent être bloqués en même temps (jusqu’à 100 à la fois).
* Publié un guide sur la prévention de l’intimidation pour aider les familles à apprendre à identifier l’intimidation ainsi que des outils permettant d’y faire face et d’aider les victimes ou les témoins de l’intimidation.
* Lancé [#CreateKindness](https://newsroom.tiktok.com/fr-fr/encourager-la-bienveillance-sur-tiktok), une campagne internationale et une série de vidéos créatives visant à sensibiliser les utilisateurs aux dangers de l’intimidation en ligne et les encourager à être bienveillants, aimables, les uns envers les autres.

Nous avons pour but d’offrir un espace sûr où les utilisateurs se sentent les bienvenus et peuvent s’exprimer comme tels qu’ils sont. Nous souhaitons que la diversité de notre communauté puisse se sentir libre de partager et de s’exprimer. Au second trimestre 2021, nous avons célébré le mois des fiertés avec notre campagne #FreeToBe. Nous avons mis en avant les créateurs LGBTQ+ et organisé notre défilé mondial de la Pride TikTok, un LIVE de 12 heures qui a permis de collecter des fonds pour les organisations qui soutiennent la communauté LGBTQ+.

#### Protéger l’intégrité de la plateforme

Nous nous efforçons de construire de manière responsable, ouverte et équitable afin que les utilisateurs TikTok continuent à aimer créer et visionner les contenus qui leur tiennent à cœur. Afin de soutenir nos équipes qui élaborent nos stratégies de sécurité et qui sont responsables de l’application de nos politiques, nous avons établi un partenariat avec la [Trust and Safety Professional Association (TSPA)](https://newsroom.tiktok.com/fr-fr/tiktok-rejoint-la-trust-and-safety-professional-association), de sorte que chaque professionnel travaillant sur des problématiques de sécurité soit désormais membre de la TSPA. Cette adhésion leur permet d’accéder à des ressources pour le développement de leur carrière, de participer à des ateliers et à des événements, et de se connecter à un réseau de pairs du secteur. Notre soutien à l’association enrichira aussi la communauté TikTok en nous permettant de continuer d’apprendre et de faire évoluer notre approche de la sécurité.

### Autres rapports

* [Rapport sur les demandes de retrait émanant de gouvernements](https://www.tiktok.com/transparency/fr-fr/government-removal-requests)
* [Rapport sur les demandes de renseignements](https://www.tiktok.com/transparency/fr-fr/information-requests)
* [Rapport de demandes de retrait de contenus pour infractions aux lois sur la propriété intellectuelle](https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests)

#### Cela a-t-il été utile ?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Oui![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)Non

# Resource URL: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2021-1/
Application des Règles communautaires

_1er janvier 2021 – 31 mars 2021_  
_publication du 30 juin 2021_

### À propos de ce rapport

Des millions de personnes dans le monde se rassemblent sur TikTok pour exprimer pleinement leur créativité et se divertir. Nos équipes oeuvrent au quotidien à faire respecter nos [Règles communautaires](https://www.tiktok.com/community-guidelines), en supprimant les comptes et contenus qui ne les respecteraient pas, pour proposer un environnement sûr et bienveillant à notre communauté. Nous veillons à être transparents sur la façon dont nous appliquons nos politiques pour renforcer les liens de confiance avec les membres de notre communauté. Aussi, nous publions régulièrement ces rapports.

Désormais nous passerons à rythme de publication trimestriel – tous les trois mois au lieu de tous les six mois – pour rendre compte de l’application de nos Règles communautaires. En raison des contraintes liées au traitement des demandes, les informations liées aux requêtes soumises par les autorités publiques et gouvernements, ainsi que les demandes de retrait pour violation de la propriété intellectuelle continueront d’être publiées de façon semestrielle.

Nos Règles communautaires s’appliquent à tous les utilisateurs et à tous les contenus présents sur la plateforme. Nos équipes TikTok d’experts en sûreté, sécurité et politique, travaillent ensemble pour concevoir des règles équitables qui puissent être systématiquement appliquées. Nos politiques prennent en compte les retours issus d’experts externes en sécurité numérique et droits de l’Homme, et nous veillons à respecter les cultures des pays où nous opérons. En aucun cas nous n’élaborons des politiques à la demande expresse d’un gouvernement, d’un individu ou d’une organisation ; de plus, aucun collaborateur TikTok n’est autorisé à modifier les Règles communautaires de manière unilatérale. Notre principal objectif est que nos règles fassent de TikTok un espace bienveillant qui encourage la créativité et l’authenticité.

Ce rapport apporte de la visibilité sur le volume et la nature des contenus et des comptes retirés de la plateforme au cours des trois premiers mois de l’année 2021 (du 1er janvier au 31 mars). Pour la première fois, nous publions le nombre de comptes soupçonnés d’être détenus par des utilisateurs de moins de 13 ans, car nous nous efforçons de faire de TikTok un espace uniquement réservé aux personnes de plus de 13 ans.

### Rapport sur les données

#### Vidéos

Au cours du premier trimestre 2021, **61 951 327** vidéos ont été supprimées dans le monde pour infraction à nos Règles communautaires ou Conditions d’utilisation, soit moins de **1 %** de toutes les vidéos téléchargées sur TikTok. Parmi ces vidéos, nous en avons identifié et supprimé **91,3 %** avant même qu’un utilisateur ne les signale, **81,8 %** avant qu’elles ne soient visionnées et **93,1 %** dans les 24 heures suivant leur publication.

Ce graphique indique les cinq marchés présentant le plus grand nombre de vidéos supprimées.

|     |     |
| --- | --- |
| ****Pays / Marché**** | **Suppression total** |
| États-Unis | 8,540,088 |
| Pakistan | 6,495,992 |
| Brésil | 6,128,568 |
| Russie | 3,747,364 |
| Indonesia | 2,757,292 |

Nous continuons de nous appuyer sur les technologies pour détecter et supprimer automatiquement les contenus en infraction sur certains marchés. **8 832 345** de vidéos supplémentaires ont été signalées et retirées automatiquement pour avoir enfreint nos Règles Communautaires à travers le monde.

TikTok permet aux créateurs de contester le retrait de leur vidéo. Lorsque nous recevons une demande de révision, nous examinons la vidéo une seconde fois et la rétablissons si s’avère qu’elle a été supprimée par erreur. Au cours du dernier trimestre, nous avons remis en ligne **2 833 837** vidéos ayant fait l’objet d’une demande de révision. Notre objectif est d’être cohérent et équitable dans nos pratiques de modération et nous poursuivons nos efforts pour réduire le nombre de “faux positifs” et former en continu nos équipes de modération.

Le graphique ci-dessous illustre le nombre de vidéos retirées pour infraction à notre politique. Une vidéo peut enfreindre plusieurs règles et chaque infraction est reflétée dans ce tableau.

Parmi les vidéos supprimées par notre équipe de modérateurs, le tableau suivant indique le taux de suppression proactive des vidéos pour des motifs enfreignant nos Règles Communautaires. Un retrait proactif consiste à détecter et à supprimer une vidéo avant qu’elle ne soit signalée. Un retrait dans les 24 heures signifie que la vidéo est retirée dans les 24 heures suivant sa publication sur notre plateforme.

|     |     |     |
| --- | --- | --- |
|     | **Retrait proactif** | **Retrait dans les 24 heures** |
| Nudité et activités sexuelles des utilisateurs adultes | 86.10% | 89.80% |
| Harcèlement et intimidation | 66.20% | 83.80% |
| Comportement haineux | 67.30% | 83.90% |
| Activités illégales et produits réglementés | 96.00% | 95.60% |
| Intégrité et authenticité | 78.50% | 88.90% |
| Sécurité des mineurs | 97.10% | 96.20% |
| Suicide, automutilation et actes dangereux | 93.70% | 91.70% |
| Contenu violent et visuellement explicite | 93.40% | 93.60% |
| Extrémisme violent | 82.10% | 87.10% |

[Nudité et activités sexuelles des utilisateurs adultes](https://www.tiktok.com/community-guidelines?lang=fr#30)Nous nous efforçons de construire une plateforme accueillante et sécurisée. Ainsi, nous supprimons tout contenu de nudité et à caractère sexuellement explicite. Parmi les vidéos supprimées, 15,6 % enfreignaient ces règles, représentant une baisse par rapport aux 20,5 % dans notre précédent rapport. TikTok retire tous les contenus à caractère pornographique et de nudité, et ce sans exception. Pour les contenus qui seraient plus appropriés pour des audiences matures, nous pouvons en limiter la diffusion dans le fil « Pour toi » de tout utilisateur, plutôt que de les supprimer. Nous avons supprimé 86,1 % de ces vidéos avant qu’elles ne nous soient signalées, tandis que 89,8 % ont été supprimées dans les 24 heures suivant leur publication, ce qui représente une légère baisse depuis notre dernier rapport.  

[Harcèlement et intimidation](https://www.tiktok.com/community-guidelines?lang=fr#36)  
Nous prônons une communauté inclusive où l’on peut s’exprimer librement et nous ne tolérons pas que les membres de notre communauté soient humiliés, intimidés ou harcelés. Parmi les vidéos que nous avons supprimées, 8 % enfreignaient cette règle, marquant une hausse par rapport aux 6,6 % sur le second semestre 2020. Cette augmentation reflète les mises à jour apportées à nos règles en fin d’année, dans le but de lutter contre l’intimidation, ainsi que les légères améliorations dans notre capacité à détecter le harcèlement ou l’intimidation. Parmi ces vidéos, 66,2 % ont été supprimées avant qu’elles ne nous soient signalées, et 83,8 % ont été supprimées dans les 24 heures suivant leur publication. Pour prévenir les comportements d’intimidation, et dans le cadre de nos efforts pour améliorer la détection proactive dans ce domaine, nous avons déployé plusieurs outils pour aider les créateurs à mieux contrôler leur expérience TikTok, dont la gestion des commentaires, le signalement et le blocage d’utilisateurs.  

[Comportement haineux](https://www.tiktok.com/community-guidelines?lang=fr#38)  
TikTok est une communauté plurielle et inclusive qui ne tolère pas de comportement haineux. Parmi les vidéos que nous avons supprimées, 2,3 % enfreignaient cette règle, représentant une légère hausse par rapport aux 2 % sur le second semestre 2020. Nous attribuons cette augmentation des retraits aux mesures de protection renforcées contre les discours haineux et les idéologies haineuses ainsi qu’à notre nouveau modèle qui nous permet de mieux repérer les discours haineux. Nous avons supprimé 67,3 % des vidéos illustrant des comportements haineux avant qu’elles ne nous soient signalées, bien que 83,9 % aient été supprimées dans les 24 heures suivant leur publication.  

[Activités illégales et produits réglementés](https://www.tiktok.com/community-guidelines?lang=fr#32)  
Nous veillons à ce que TikTok ne permette pas des activités criminelles ou faisant la promotion de drogues, tabac, d’alcool ou de tout autre produit réglementé. Sur les trois premiers mois de l’année 2021, 21,1 % des vidéos que nous avons supprimées enfreignaient cette règle. Ce chiffre est en hausse par rapport aux 17,9 % du second semestre 2020, et nous avons constaté une augmentation des tentatives de publication de ce type de contenu sur la plateforme. Parmi ces vidéos, 96 % ont été supprimées avant de nous être signalées, et 95,6 % ont été supprimées dans les 24 heures suivant leur publication. Nous attribuons ces améliorations au perfectionnement continu de nos systèmes de détection.  

[Intégrité et authenticité](https://www.tiktok.com/community-guidelines?lang=fr#37)  
Nous estimons que la confiance constitue le socle de notre communauté, et nous n’autorisons aucun contenu ou compte qui implique une interaction artificielle, une usurpation d’identité et une activité de désinformation. Parmi les vidéos supprimées, 2 % enfreignaient cette règle, soit une baisse par rapport aux 2,4 % sur le second semestre 2020. Parmi ces vidéos, 78,5 % ont été supprimées avant de nous être signalées, et 88,9 % ont été supprimées dans les 24 heures suivant leur publication. Nous avons continué d’améliorer nos modèles de détection des informations trompeuses, et ces efforts ont contribué à une sensible augmentation des repérages proactifs au cours du premier trimestre 2021 par rapport aux six derniers mois de 2020.  

[Sécurité des mineurs](https://www.tiktok.com/community-guidelines?lang=fr#31)  
Nous sommes résolument engagés à assurer la sécurité des mineurs et mettons régulièrement à jour notre politique et nos procédures pour les protéger. Au cours du premier trimestre 2021, 36,8 % des contenus enfreignaient notre politique en matière de sécurité des mineurs, contre 36 % pour la seconde moitié de 2020. Parmi ces vidéos, 97,1 % ont été supprimées avant de nous être signalées, et 96,2 % ont été supprimées dans les 24 heures suivant leur publication. Ces progrès en matière de détection proactive sont le résultat de l’amélioration de nos modèles qui permettent d’identifier et de signaler les infractions dès le téléchargement du contenu, avant toute visualisation.  

[Suicide, automutilation et actes dangereux](https://www.tiktok.com/community-guidelines?lang=fr#34)  
La santé et le bien-être des individus qui composent notre communauté nous tiennent particulièrement à cœur. À cet égard, nous nous employons à favoriser un environnement solidaire. Parmi les vidéos supprimées, 5,7 % enfreignaient cette règle, soit une baisse par rapport aux 6,2 % sur le second 2020. Nous estimons que ces résultats sont liées à une diminution du volume global de ces vidéos, car nous déployons des efforts concertés pour supprimer rapidement les contenus et rediriger les personnes qui les recherchent vers des services d’assistance spécialisés et des ressources d’urgence. Parmi ces vidéos, 93,7 % ont été supprimées avant de nous être signalées, et 91,7 % ont été supprimées dans les 24 heures suivant leur publication  

[Extrémisme violent](https://www.tiktok.com/community-guidelines?lang=fr#39)  
Nous nous opposons fermement à toute forme de violence sur et en dehors de TikTok. Parmi les vidéos supprimées, 0,5 % enfreignaient cette règle, contre 0,3 % sur le second semestre 2020. Parmi ces vidéos, 82,1 % ont été supprimées avant de nous être signalées, et 87,1 % ont été supprimées dans les 24 heures suivant leur publication. Nous estimons que cette augmentation des suppressions est due au fait que nos règles précisent désormais de manière plus détaillée ce qui est considéré comme une menace violente et/ou une incitation à la violence en matière de contenus et de comportements interdits. Nous développons des modèles de détection proactive qui permettent de signaler ce type de contenu, et examinons également les signalements soumis par les utilisateurs et les organisations non gouvernementales

[Contenu violent et visuellement explicite](https://www.tiktok.com/community-guidelines?lang=fr#35)  
TikTok est une plateforme qui encourage l’expression créative mais condamne les contenus choquants ou violents. Parmi les vidéos supprimées, 8,1 % enfreignaient cette règle, ce qui correspond aux chiffres du second semestre 2020. Parmi ces vidéos, 93,4 % ont été supprimées avant de nous être signalées, et 93,6 % ont été supprimées dans les 24 heures suivant leur publication. Il s’agit d’une amélioration par rapport à notre dernier rapport, liée au renforcement de nos politiques et d’une meilleure détection de ce type de contenus.

#### Comptes

Au cours du premier trimestre 2021, **11 149 514** comptes ont été supprimés pour infraction à nos Règles Communautaires ou Conditions d’utilisation. Ce chiffre comprend **7 263 952** comptes retirés car ils étaient soupçonnés d’être détenus par des utilisateurs de moins de 13 ans, soit moins de 1 % des comptes sur TikTok.

En complément, nous avons empêché la création de **71 470 161** de comptes par des méthodes automatisées et avons supprimé **12 378 928** de vidéos supplémentaires publiées par des spammeurs.

#### Contenus publicitaires

TikTok a mis en place des [mesures strictes](https://ads.tiktok.com/help/article?aid=10000962) pour protéger les utilisateurs contre les contenus publicitaires mensongers, frauduleux ou trompeurs. Les comptes d’annonceurs et les publicités payantes sont soumis à ces règles et doivent se conformer à nos Règles Communautaires, [Politiques de publicité TikTok](https://ads.tiktok.com/help/article?aid=10000962) et Conditions d’utilisation. Au cours du premier trimestre 2021, nous avons rejeté **1 921 900** de contenus publicitaires pour infraction à nos politiques et règles en matière de publicité.

Nous nous engageons à ce que nos utilisateurs aient une expérience positive, authentique et agréable lorsqu’ils regardent des publicités sur TikTok. Ainsi, nous continuerons à investir tant dans nos ressources humaines que dans nos moyens technologiques pour veiller à ce que les contenus publicitaires de la plateforme répondent à nos attentes.

#### Sécurité

En octobre 2020, nous avons lancé [le programme mondial « bug bounty »](https://newsroom.tiktok.com/en-au/security-is-our-priority-all-year-long-au) (le « butin des bogues ») qui est le prolongement de notre programme de gestion des vulnérabilités. Son objectif est de repérer de manière proactive les vulnérabilités de sécurité et de les résoudre. Ce programme renforce notre degré de maturité en matière de sécurité en encourageant les chercheurs en sécurité du monde entier à identifier et à signaler de manière responsable les bogues à nos équipes afin que nous puissions les résoudre avant que des malfaiteurs ne les exploitent.

Au cours du premier trimestre 2021, TikTok a reçu **33** signalements de bogue valides dont **29** ont été résolus. Nous avons reçu **8** demandes de divulgation publique, lesquelles ont toutes été publiées. En moyenne, TikTok procède au règlement des récompenses dans un délai de **3 jours**. Quant au temps de réponse de TikTok, en moyenne, le délai de première réponse est de **8 heures** et le délai de résolution de **30 jours**.

### Synthèse

#### Sécurité des mineurs

Chez TikTok, nous nous attachons à créer un environnement adapté à l’âge des utilisateurs en concevant des politiques et des outils qui contribuent à promouvoir une expérience sûre et positive sur notre plateforme. Depuis janvier 2021, tous les paramètres de confidentialité par défaut de nos utilisateurs âgés de 13 à 15 ans sont automatiquement en Mode Privé, limitant ainsi les personnes autorisées à télécharger leurs vidéos et à interargir avec leur contenu par commentaires, ou les fonction Duos et Collage. Pour les utilisateurs âgés de 16 à 17 ans, les fonctions Duo et Collage sont paramétrées sur Amis. Par défaut, l’option permettant de choisir les personnes autorisées à télécharger est mise sur Off.

Il s’agit d’une initiative importante pour renforcer la sécurité et la protection de la vie privée des mineurs. Ces mises à jour s’appuient sur de précédentes fonctionnalités de restriction par l’âge, dont la suppression de la Messagerie Directe, streaming en direct (« livestream ») et les cadeaux virtuels pour tous les utilisateurs de, respectivement, moins de 16 ou 18 ans. Notre ambition est d’aider les utilisateurs à faire des choix éclairés sur ce qu’ils veulent partager et avec qui ils souhaitent le faire. En impliquant les adolescents dès le début, nous espérons leur donner les moyens de gérer avec précaution leur présence en ligne. Nous souhaitons également accompagner les parents en leur fournissant des [outils et des ressources](https://www.tiktok.com/safety/en-us/guardians-guide/) qui leur permettront de dialoguer avec les adolescents sur leur sécurité en ligne et la culture numérique. Par exemple, le [Mode Connexion famille](https://newsroom.tiktok.com/fr-fr/connexion-famille-mise-a-jour) permet aux parents de lier leur compte TikTok à celui de l’adolescent pour définir ensemble certains paramètres de confidentialité, de navigation et de temps d’écran.

#### Bien-être

La grande diversité de notre communauté contribue à faire de TikTok un environnement privilégié où explorer et apprécier du contenu divertissant, que ce soit des recettes de cuisine comme des idées de jardinage. Nous mettons tout en oeuvre pour faire de TikTok un environnement sûr et positif, et dans cette perspective, nous avons élaboré de nouveaux outils pour promouvoir la bienveillance et le respect au sein de notre communauté.

* Nous avons développé [une notification](https://newsroom.tiktok.com/fr-fr/filtres-tous-commentaires) qui invite les utilisateurs à reconsiderer l’impact de leurs mots avant de publier un commentaire qui pourrait être désagréable.
* Nous avons introduit un moyen pour les créateurs de filtrer tous les commentaires sur leur contenu afin que seuls ceux approuvés apparaissent sur leurs vidéos.
* Nous avons annoncé un partenariat avec le Cyberbullying Research Center afin de faire progresser nos connaissances sur l’intimidation et de développer de nouvelles initiatives de lutte contre le harcèlement.

Nous voulons que chacun se sente à l’aise et en confiance pour s’exprimer pleinement sur TikTok. Pour encourager [la diversité corporelle sur TikTok](https://newsroom.tiktok.com/en-us/supporting-nedawareness-and-body-inclusivity-on-tiktok), nous proposons depuis février des ressources sur le bien-être, développées avec le concours d’experts en troubles alimentaires, afin d’aider les utilisateurs à identifier les propos négatifs qu’ils tiennent sur eux-mêmes, à prendre conscience de leurs propres atouts et forces, ou à soutenir un ami qui pourrait traverser une période difficile. Nous avons également publié des messages d’intérêt public permanents sur des hashtags pertinents afin de sensibiliser le public ou d’encourager le soutien de la communauté aux personnes souffrant de troubles alimentaires et à leur guérison.

#### Protéger l’intégrité de la plateforme

Nous maintenons nos efforts pour lutter contre les contenus et comportements qui visent à mettre en péril l’intégrité de notre plateforme. Pour atteindre cet objectif en matière d’activités illégales et de produits réglementés, nous avons rejoint la [Coalition to End Wildlife Trafficking Online](https://newsroom.tiktok.com/en-us/tiktok-joins-the-coalition-to-end-wildlife-trafficking-online), ce qui nous permet de collaborer avec d’autres acteurs du secteur sur les meilleures pratiques ainsi que les contenus et tendances émergents.

Par ailleurs, TikTok supprime ou limite les informations trompeuses dès qu’elles sont identifiées. Nous travaillons avec 11 organisations accréditées par l’International Fact-Checking Network afin de vérifier les faits dans 21 langues et 57 marchés en Amérique, Asie, Europe, au Moyen-Orient et en Afrique. Si les enquêtes confirment qu’un contenu est faux, celui-ci est retiré de la plateforme. Cette année, au cours des trois premiers mois, nous [avons introduit une fonctionnalité](https://newsroom.tiktok.com/en-us/new-prompts-to-help-people-consider-before-they-share) visant à réduire la diffusion de contenus non fondés.

### Autres rapports

* [Demandes de retrait émanant des autorités publiques](https://www.tiktok.com/transparency/fr-fr/government-removal-requests)
* [Demandes d’information](https://www.tiktok.com/transparency/fr-fr/information-requests)
* [Demandes de retrait de contenu portant atteinte à la propriété intellectuelle](https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests)

#### Cela a-t-il été utile ?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Oui![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)Non

# Resource URL: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2020-2/
Application des Règles communautaires

_1er juillet au 31 décembre 2020_  
_publication du 24 février 2021_

### À propos de ce rapport

TikTok possède une communauté internationale, d’une grande diversité, où chacun peut exprimer sa créativité. Nous mettons tout en oeuvre pour maintenir un environnement où chacun se sent libre et en sécurité de créer des vidéos, trouver une communauté et se divertir. Nous pensons qu’il est essentiel de se sentir en sécurité pour s’exprimer librement de manière authentique, et c’est pourquoi nous nous efforçons de faire respecter nos [Règles Communautaires](https://www.tiktok.com/community-guidelines?lang=fr) en supprimant les comptes et les contenus qui les enfreignent. Notre objectif est que TikTok reste un espace source d’inspiration, de créativité et de divertissement pour tous.

Nous nous engageons à faire preuve de transparence sur la façon dont nos règles sont appliquées, car cela contribue à instaurer la confiance avec notre communauté et nous rend responsable de nos actions. Nous publions des Rapports de Transparence qui donnent un aperçu du volume et de la nature des contenus supprimés pour infraction à nos Règles Communautaires ou à nos [Conditions de services](https://www.tiktok.com/legal/terms-of-use?lang=fr).

### Rapport sur les données

#### Vidéos

Au cours du second semestre 2020 (du 1er juillet au 31 décembre), **89 132 938** de vidéos ont été supprimées dans le monde entier pour avoir enfreint nos Règles Communautaires ou nos Conditions de services, soit moins de **1 %** de toutes les vidéos téléchargées sur TikTok. Parmi ces vidéos, nous en avons identifiées et retirées **92,4 %** avant leur signalement par un utilisateur, **83,3 %** ont été retirées avant qu’elles ne soient visionnées et **93,5 %** ont été retirées dans les 24 heures suivant leur publication.

Ce graphique illustre les cinq marchés détenant le plus grand nombre de vidéos supprimées.

|     |     |
| --- | --- |
| **Pays** | **Total des retraits** |
| Etats-Unis | 11,775,777 |
| Pakistan | 8,215,633 |
| Brésil | 7,506,599 |
| Russie | 4,574,690 |
| Indonésie | 3,860,156 |

En raison de la pandémie, nous continuons de nous appuyer sur les technologies pour détecter et supprimer automatiquement les contenus en infraction sur certains marchés, tels que le Brésil et le Pakistan. **8 295 164** de vidéos supplémentaires ont été signalées et retirées automatiquement pour avoir enfreint nos Règles Communautaires. Ces vidéos ne sont pas comptabilisées dans les tableaux ci-dessous.

TikTok permet aux créateurs de contester le retrait de leur vidéo. Lorsque nous recevons une demande de révision, nous examinons la vidéo une seconde fois et la rétablissons si elle n’enfreint pas nos règles. Au cours du dernier semestre, nous avons remis en ligne **2 927 391** de vidéos ayant fait l’objet d’une demande de révision.

Ce graphique illustre le nombre de vidéos retirées pour infraction à notre politique. Une vidéo peut enfreindre plusieurs règles et chaque infraction est reflétée dans ce tableau.

Parmi les vidéos supprimées par notre équipe de modérateurs, le tableau suivant indique le taux de suppression proactive des vidéos pour des motifs enfreignant nos Règles Communautaires. Un retrait proactif consiste à détecter et à supprimer une vidéo avant qu’elle ne soit signalée. Un retrait dans les 24 heures signifie que la vidéo est retirée dans les 24 heures suivant sa publication sur notre plateforme.

|     |     |     |
| --- | --- | --- |
|     | **Taux de vidéos supprimées proactivement** | **Taux de suppression** |
| [Nudité et activités sexuelles des utilisateurs adultes](https://www.tiktok.com/community-guidelines?lang=fr#30) | 88.30% | 90.60% |
| [Harcèlement et intimidation](https://www.tiktok.com/community-guidelines?lang=fr#36) | 66.50% | 84.10% |
| [Comportement haineux](https://www.tiktok.com/community-guidelines?lang=fr#38) | 72.90% | 83.50% |
| [Activités illégales et produits réglementés](https://www.tiktok.com/community-guidelines?lang=fr#32) | 96.30% | 94.80% |
| [Intégrité et authenticité](https://www.tiktok.com/community-guidelines?lang=fr#37) | 70.50% | 91.30% |
| [Sécurité des mineurs](https://www.tiktok.com/community-guidelines?lang=fr#31) | 97.10% | 95.80% |
| [Suicide, automutilation et actes dangereux](https://www.tiktok.com/community-guidelines?lang=fr#34) | 94.40% | 91.90% |
| [Contenu violent et visuellement explicite](https://www.tiktok.com/community-guidelines?lang=fr#35) | 93.20% | 92.70% |
| [Extrémisme violent](https://www.tiktok.com/community-guidelines?lang=fr#39) | 86.90% | 89.40% |

**[Nudité et activités sexuelles des utilisateurs adultes](https://www.tiktok.com/community-guidelines?lang=fr#30)**  
Nous nous efforçons de créer une plateforme sûre et bienveillante, et nous supprimons tout contenu de nudité et à caractère sexuellement explicite. Parmi les vidéos supprimées, **20,5 %** enfreignaient ces règles, ce qui correspond à une baisse de **30,9 %** par rapport au premier semestre 2020. L’une des raisons de cette diminution est l’amélioration de nos systèmes de triage qui séparent les contenus de nudité des utilisateurs adultes de ceux des utilisateurs mineurs. Nous avons supprimé **88,3 %** de ces vidéos avant qu’elles ne nous soient signalées, et **90,6 %** ont été supprimées dans les 24 heures suivant leur publication.

**[Harcèlement et intimidation](https://www.tiktok.com/community-guidelines?lang=fr#36)**  
Nous prônons une communauté inclusive où l’on peut s’exprimer librement sans craindre de subir d’abus. Nous ne tolérons pas que les membres de notre communauté soient humiliés, intimidés ou harcelés. Parmi les vidéos que nous avons supprimées, **6,6 %** enfreignaient ces règles, contre **2,5 %** au premier semestre 2020. Cette augmentation reflète les ajustements apportés aux règles relatives au harcèlement sexuel, aux menaces de piratage informatique et aux victimes de déclarations d’intimidation, qui sont désormais plus complètes. En outre, nous avons constaté de légères améliorations dans notre capacité à proactivement détecter le harcèlement ou l’intimidation, ce qui reste un défi compte tenu des nuances linguistiques et culturelles. Parmi ces vidéos, **66,5 %** ont été supprimées avant qu’elles ne nous soient signalées, et **84,1 %** ont été supprimées dans les 24 heures suivant leur publication. Nous sommes déterminés à réduire cet écart et nous tiendrons notre communauté informée des évolutions dans ce domaine.

**[Comportement haineux](https://www.tiktok.com/community-guidelines?lang=fr#38)**  
TikTok est une communauté plurielle et inclusive qui ne tolère aucun comportement haineux. L’année dernière, nous avons modifié notre politique, passant de « discours haineux » à son nom actuel de « comportement haineux », afin d’adopter une approche plus large pour lutter contre les idéologies haineuses et les activités hors de la plateforme. En conséquence, **2 %** des vidéos que nous avons supprimées enfreignaient cette règle, contre **0,8 %** au premier semestre 2020. Nous disposons de systèmes de détection de symboles haineux, tels que les drapeaux et les icônes, mais la détection proactive de discours haineux reste un défi et nous poursuivons nos investissements afin de nous améliorer. Nous avons supprimé **72,9 %** de vidéos illustrant des comportements haineux avant qu’elles ne nous soient signalées, bien que **83,5 %** aient été supprimées moins de 24 heures suivant leur publication.

**[Activités illégales et produits réglementés](https://www.tiktok.com/community-guidelines?lang=fr#32)**  
Nous veillons à ce que TikTok ne permette pas des activités qui enfreignent les lois ou réglementations en vigueur, telles que la fraude ou les arnaques. **17,9 %** des vidéos que nous avons supprimées enfreignaient cette règle. Un chiffre en légère baisse par rapport aux **19,6 %** du premier semestre 2020. Nous attribuons ce résultat à l’amélioration de nos systèmes d’automatisation et de détection ainsi qu’à des flux de travail plus performants. Parmi ces vidéos, **96,3 %** ont été retirées avant d’être signalées, et **94,8 %** ont été retirées dans les 24 heures suivant leur publication.

**[Intégrité et authenticité](https://www.tiktok.com/community-guidelines?lang=fr#37)**  
Nous estimons que la confiance constitue le socle de notre communauté, et nous n’autorisons aucun contenu ou compte qui implique une interaction artificielle, une usurpation d’identité et une activité de désinformation. Parmi les vidéos supprimées, **2,4 %** enfreignaient cette règle, contre **1,2 %** au premier semestre 2020. Nous avons intégré de nouveaux partenaires vérificateurs de faits sur de nouveaux marchés et disposons désormais d’un réseau en 16 langues. Cela nous aide à évaluer plus précisément les contenus et à retirer les informations trompeuses. Nous avons également amélioré notre capacité à détecter et à supprimer les interactions artificielles et les spams. Parmi les vidéos supprimées, **70,5 %** l’ont été avant de nous être signalées, et **91,3 %** dans les 24 heures suivant leur publication. Nous investissons dans nos infrastructures pour améliorer notre système de détection proactive, en particulier lorsqu’il s’agit d’identifier des informations trompeuses.

**[Sécurité des mineurs](https://www.tiktok.com/community-guidelines?lang=fr#31)**  
Nous sommes résolument engagés à assurer la sécurité des mineurs et mettons régulièrement à jour notre politique et nos procédures pour les protéger. Par exemple, nous avons ajouté une section dédiée aux activités dangereuses et nous supprimons tout contenu représentant ou faisant la promotion de la consommation de boissons alcoolisés, tabac, ou drogues ou tout autre comportement qui pourrait mettre en péril le bien-être des mineurs. En conséquence, **36 %** des contenus que nous avons supprimés enfreignaient cette règle, contre **22,3 %** au cours du premier semestre 2020. Parmi ces vidéos, **97,1 %** ont été supprimées avant de nous être signalées, et **95,8 %** ont été supprimées dans les 24 heures suivant leur publication.

TikTok s’appuie sur [PhotoDNA](https://www.microsoft.com/en-us/photodna), une technologie qui aide à identifier et supprimer les contenus qui exploitent des enfants, pour se protéger des matériaux d’abus sexuels d’enfants. Nous poursuivons d’investir dans nos systèmes internes de détection de ce type de contenus. Ces efforts ont considérablement amélioré notre capacité à supprimer et à signaler les contenus et les comptes concernés au Centre national pour les enfants disparus et exploités (NCMEC) et aux autorités juridiques compétentes. En conséquence, nous avons effectué **22 692 signalements** au NCMEC en 2020, contre **596** en 2019.

**[Suicide, automutilation et actes dangereux](https://www.tiktok.com/community-guidelines?lang=fr#34)**  
La santé et le bien-être des individus qui composent notre communauté nous tiennent particulièrement à cœur. Au cours du second semestre 2020, nous avons mis à jour nos Règles Communautaires sur l’automutilation, le suicide et les troubles alimentaires afin de prendre en compte les commentaires et langages utilisés par les experts en santé mentale. Nous avons également établi des partenariats avec certaines associations afin de soutenir les personnes en difficulté. Nous redirigeons par exemple les recherches et les hashtags concernés vers la ligne d’assistance téléphonique de la « National Eating Disorder Association », la « Crisis Textline » ou la « National Suicide Prevention Lifeline ». Parmi les vidéos supprimées, **6,2 %** enfreignaient ces règles, ce qui représente une baisse par rapport aux **13,4 %** du premier semestre 2020, et s’explique notamment car nous supprimons désormais tout contenu mettant en scène des mineurs aux comportements dangereux ou risqués, selon les termes de notre nouvelle politique de sécurité des mineurs face aux activités à risques. Parmi ces vidéos, **94,4 %** ont été supprimées avant d’être signalées, et **91,9 %** ont été supprimées dans les 24 heures suivant leur publication.

**[Extrémisme violent](https://www.tiktok.com/community-guidelines?lang=fr#39)**  
Nous nous opposons fermement à toute forme de violence sur et en dehors de TikTok. L’automne dernier, lors de la mise à jour de nos Règles Communautaires, nous avons clarifié notre politique contre les personnes et organisations violentes, pour adresser de façon globale les défis de l’extrémisme violent et avons précisé ce que TikTok considère comme une menace ou une incitation à la violence. Sur l’ensemble des vidéos supprimées, **0,3 %** enfreignaient cette règle, ce qui est comparable au contenu supprimé au premier semestre 2020. Parmi ces vidéos, **86,9 %** ont été supprimées avant d’être signalées, et **89,4 %** ont été supprimées dans les 24 heures suivant leur publication.

**[Contenu violent et visuellement explicite](https://www.tiktok.com/community-guidelines?lang=fr#35)**  
TikTok est une plateforme qui encourage l’expression créative mais condamne les contenus choquants ou violents. Parmi les vidéos supprimées, **8,1 %** enfreignaient cette règle, contre **8,7 %** au premier semestre 2020. Parmi ces vidéos, **93,2 %** ont été supprimées avant d’être signalées, et **92,7 %** ont été supprimées dans les 24 heures suivant leur publication. Nous autorisons sur notre plateforme, à des fins documentaires, les vidéos représentant des manifestations violentes, des scènes d’animaux chassant dans la nature et d’autres contenus de ce type. Nous avons d’ailleurs introduit des [systèmes de visionnage personnalisés](https://newsroom.tiktok.com/en-us/refreshing-our-policies-to-support-community-well-being) pour permettre aux utilisateurs de mieux choisir les vidéos qu’ils regardent.

#### Comptes

Au cours du second semestre 2020, **6 144 040** de comptes ont été supprimés pour violation de nos Règles Communautaires. En complément, **9 499 881** de comptes de spam ont été supprimés, ainsi que **5 225 800** de vidéos de spam publiées par ces comptes. Nous avons empêché **173 246 894** de comptes d’être créés par des spammeurs.

#### Contenus publicitaires

TikTok a mis en place des mesures strictes pour protéger les utilisateurs contre les contenus mensongers, frauduleux ou trompeurs, dont les publicités. Les comptes d’annonceurs et les contenus publicitaires sont soumis à ces règles et doivent se conformer à nos Règles Communautaires, Règles publicitaires et à nos Conditions de services. Au cours du second semestre 2020, nous avons supprimé **3 501 477** de contenus publicitaires pour violation de nos politiques et règles en matière de publicité. Nous nous engageons à créer un environnement sûr et bienveillant pour nos utilisateurs, et nous réévaluons et améliorons régulièrement nos systèmes de lutte contre les contenus publicitaires qui enfreignent nos politiques.

### Synthèse

Nous faisons sans cesse évoluer nos politiques, nos produits et nos partenariats pour garantir le bien-être de notre communauté. Voici quelques-unes des principales mises à jour que nous avons effectuées au cours du second semestre 2020.

#### Garantir le bien-être de la communauté

Nous avons renforcé nos [Règles C](https://www.tiktok.com/community-guidelines?lang=en)[ommunautaires](https://www.tiktok.com/community-guidelines?lang=fr) afin de promouvoir le bien-être de la communauté, en prenant en compte les comportements observés sur la plateforme et les commentaires de nos utilisateurs et d’experts. Par exemple, la mise à jour de nos politiques relatives à l’automutilation, le suicide et les troubles alimentaires reflètent les recommandations émanant d’experts en santé mentale. Nos [politiques publicitaires](https://newsroom.tiktok.com/en-us/coming-together-to-support-body-positivity-on-tiktok) interdisent désormais les annonces publicitaires d’applications sur le jeûne et les compléments alimentaires pour la perte de poids. Nous avons également amélioré la façon dont nous [informons nos utilisateurs](https://newsroom.tiktok.com/en-us/adding-clarity-to-content-removals) afin de les aider à comprendre les raisons pour lesquelles leur vidéo a été supprimée.

TikTok s’est associée à différentes organisations pour venir en aide aux personnes qui pourraient être aux prises avec un trouble alimentaire, un comportement d’automutilation ou des pensées suicidaires. Désormais, les recherches de contenus et les hashtags liés à ces thématiques sont redirigés vers des lignes téléphoniques d’assistance telles que « Suicide Ecoute » ou « S.O.S Amitié » notamment, qui permettent aux utilisateurs d’accéder à une aide gratuite et confidentielle. Nous mettons en avant des conseils pour améliorer le bien-être émotionnel de la personne.

#### Accompagner les familles

Nous discutons régulièrement avec des parents, des adolescents et des experts en sécurité des jeunes afin de proposer aux familles des outils efficaces pour qu’elles bénéficient d’une expérience TikTok adaptée. Fin 2020, nous avons élargi les fonctionnalités de notre [Mode Connexion Famille](https://newsroom.tiktok.com/fr-fr/connexion-famille-mise-a-jour) pour permettre aux parents de renforcer les protections sur les contenus et les paramètres de confidentialité de leurs adolescents. Grâce à des dispositifs permettant de limiter les recherches, les commentaires et le temps d’écran, nous espérons que ces outils encourageront les familles à dialoguer plus ouvertement sur la sécurité numérique.

### Autres rapports

* [Rapport sur les demandes de retrait émanant de gouvernements](https://www.tiktok.com/transparency/fr-fr/government-removal-requests)
* [Rapport sur les demandes de renseignements](https://www.tiktok.com/transparency/fr-fr/information-requests)
* [Rapport de demandes de retrait de contenus pour infractions aux lois sur la propriété intellectuelle](https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests)

#### Cela a-t-il été utile ?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Oui![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)Non

# Resource URL: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2020-1/
Application des Règles communautaires

_Du 1e janvier au 30 juin 2020  
Publié le 22 Septembre 2020_

### À propos de ce rapport

Des centaines de millions de personnes à travers le monde se rendent sur TikTok pour se divertir, s’exprimer et échanger. Notre priorité absolue est d’aider à leur assurer en environnement sûr, une expérience positive qui leur procurera le sentiment d’appartenir à une communauté internationale qui ne cesse de croître. Nous ne pouvons y parvenir sans faire preuve de transparence.

TikTok s’efforce d’être l’entreprise la plus transparente et la plus responsable de son secteur sur des sujets tels que la protection de nos utilisateurs. Nous avons mis nos actes en accord avec nos paroles en ouvrant nos [Centres sur la transparence et la responsabilité](https://www.tiktok.com/transparency), où nous levons le voile sur nos pratiques en matière de modération de contenus, d’algorithmes, de confidentialité et de sécurité. Nous publions des Rapports de transparence comme celui-ci afin de transmettre des informations sur la quantité et la nature des contenus supprimés pour avoir enfreint nos Règles Communautaires ou à nos Conditions de service. Nous y dressons aussi un bilan des actions engagées au premier trimestre 2020 en vue de renforcer la sécurité de notre plateforme et d’y promouvoir un environnement positive.

### Rapport sur les données

#### Vidéo

Au premier semestre 2020 (du 1er janvier au 30 juin 2020), 104 543 719 vidéos ont été supprimées à l’échelle internationale pour avoir enfreint nos Règles Communautaires ou nos Conditions de service, soit moins de 1 % des vidéos postées sur TikTok. 96,4 % de ces vidéos ont été identifiées et supprimées par nos soins avant même de faire l’objet d’un signalement, et 90,3 % ont été retirées de la plateforme avant d’avoir été visionnées ne serait-ce qu’une fois.

Le tableau ci-après recense le nombre de vidéos supprimées pour avoir enfreints nos politiques. Quand une vidéo enfreint nos Règles Communautaires, elle est catégorisée en fonction de la règle (ou des règles) enfreinte(s), puis supprimée. Cela signifie qu’une même vidéo peut être comptabilisée dans plusieurs catégories de règles.

Du fait de la pandémie de coronavirus, nous avons eu plus largement recours à la technologie pour détecter et supprimer automatiquement les contenus en infraction avec nos règles sur des marchés comme l’Inde, le Brésil et le Pakistan. Sur l’ensemble des vidéos retirées de la plateforme, 10 698 297 ont été signalées et supprimées automatiquement pour avoir enfreint nos Règles Communautaires. Ces vidéos ne sont pas comptabilisées dans le tableau ci-dessous.

Le tableau ci-après présente les 5 pays/marchés comptant le plus grand nombre de vidéos supprimées.

|     |     |
| --- | --- |
| **Pays** | **Total des retraits** |
| Inde | 37,682,924 |
| États-Unis | 9,822,996 |
| Pakistan | 6,454,384 |
| Brésil | 5,525,783 |
| Royaume Uni | 2,949,620 |

### Synthèse

Nous avons réalisé de grandes avancées au cours du premier trimestre 2020 en mettant en place plusieurs règles, produits et partenariats destinés à renforcer la sécurité, la responsabilité et la positivité au sein de l’application.

#### Politiques

Nous avons débuté l’année en complétant nos [Règles Communautaires](https://newsroom.tiktok.com/en-us/adding-clarity-to-our-community-guidelines) afin d’assurer à nos utilisateurs un environnement stimulant et accueillant. Ces Règles comprennent de nouvelles directives relatives aux [contenus trompeurs](https://newsroom.tiktok.com/en-us/building-to-support-integrity) et qui visent à prévenir la propagation de désinformation ou de contenus qui pourraient prêter à confusion, ainsi que des directives plus nuancées dans des domaines comme les discours haineux et encourager l’inclusion. Nous nous sommes aussi engagés publiquement à promouvoir et soutenir les [Voluntary Principles to Counter Online Child Sexual Exploitation and Abuse](https://newsroom.tiktok.com/en-us/tiktok-supports-and-promotes-the-voluntary-principles-to-counter-online-child-exploitation) et le [Code de bonnes pratiques contre la désinformation](https://newsroom.tiktok.com/en-gb/supporting-our-community-through-covid-19?utm_source=POLITICO.EU&utm_campaign=d0c22ce942-EMAIL_CAMPAIGN_2020_07_17_05_00&utm_medium=email&utm_term=0_10959edeb5-d0c22ce942-190627564) adopté par l’Union Européenne.

#### Produit

##### Mettre en avant nos politiques

Pour soutenir nos politiques, nous avons proposé une nouvelle fonctionnalité qui permet aux utilisateurs de signaler facilement différents types de fausses informations, que notre équipe se charge ensuite d’examiner. Disponible dans 8 pays, ce programme de _vérification des faits_ permet de vérifier plus efficacement les contenus trompeurs et frauduleux, comme les contre-vérités circulant au sujet du nouveau coronavirus, les élections ou encore le réchauffement climatique. Nous avons par ailleurs introduit des messages pédagogiques d’intérêt général qui s’affichent dans l’application lors de l’utilisation de hashtags associés à des sujets qui occupent une place importante dans le débat public, comme les élections, le mouvement Black Lives Matter, et les thèses conspirationnistes dangereuses, à l’image de Qanon.

##### Mettre en avant, auprès des utilisateurs, des contenus fiables et pédagogiques

Nous avons continué à sensibiliser les utilisateurs à nos politiques et aux fonctionnalités de sécurité en diffusant, au sein de l’application, une série de [vidéos sur la thématique](https://newsroom.tiktok.com/en-us/youre-in-control-video-series-stars-tiktok-creators-to-educate-users-about-safety-features). Pour sensibiliser les adolescents et leurs parents à la culture du numérique, nous avons mis en ligne le [portail TikTok pour les jeunes](https://www.tiktok.com/safety/youth-portal?lang=fr), qui présente les bons réflexes à avoir pour une expérience en ligne sûre ainsi que les outils de contrôle intégrés à l’application. Nous avons aussi créé des vidéos au format TikTok dans lesquelles des créateurs reconnus sensibilisent notre communauté aux [média](https://newsroom.tiktok.com/en-us/tiktoks-be-informed-series-stars-tiktok-creators-to-educate-users-about-media-literacy) et à la [désinformation](https://newsroom.tiktok.com/en-in/tiktoks-matkarforward-initiative-urges-netizens-to-curb-the-spread-of-misinformation).

Nous avons axé notre travail sur la création d’expériences qui orientent nos utilisateurs vers des contenus fiables et reconnus et ce, sur de nombreux domaines. Lors de l’apparition du nouveau coronavirus nous avons mis en avant des [contenus faisant autorité](https://www.tiktok.com/safety/resources/covid-19) au travers de pages d’information visibles directement sur l’application. En parallèle, nous avons organisé des _Lives_ pédagogiques et des hashtags challenges en partenariat avec l’Organisation mondiale de la santé, la Fédération internationale de la Croix‑Rouge, UNICEF Inde, et des figures bien connues de ceux qui s’intéressent à la santé publique et aux sciences, à l’image de Bill Nye the Science Guy et de [Prince’s Trust](https://newsroom.tiktok.com/en-gb/welcoming-the-princes-trust-to-tiktok). Pour soutenir la [communauté noire](https://newsroom.tiktok.com/en-us/progress-report-how-were-supporting-black-communities-and-promoting-diversity-and-inclusion), nous avons déployé sur notre plateforme des [pages dédiées](https://newsroom.tiktok.com/en-us/recognizing-juneteenth) où les utilisateurs peuvent s’informer sur l’histoire afro-américaine et les organisations qui se sont battues pour la justice sociale, tout en mettant à l’honneur des créateurs et artistes noirs tout au long du Black Music Month.

##### Renforcer la sécurité des jeunes

TikTok a lancé une série de fonctionnalités de pointe qui visent à protéger les jeunes et à permettre aux parents et à leurs adolescents d’adapter leur expérience sur TikTok à leurs besoins. Grâce au mode [Connexion Famille](https://newsroom.tiktok.com/en-us/tiktok-introduces-family-pairing), les parents ont la possibilité de relier leur compte TikTok à celui de leurs adolescents afin de limiter la visibilité de certains contenus, d’inciter ces derniers à modérer leur temps de visionnage, et de modifier les paramètres de la messagerie directe. TikTok désactive par défaut cette fonctionnalité pour les comptes des utilisateurs de moins de 16 ans et ne permet pas l’envoi d’images privées afin de prévenir le partage de contenus à caractère nuisible ou abusif.

#### Partenariats

Nous prêtons une attention particulière aux observations et recommandations des spécialistes, des organisations à but non lucratif notamment, car elles renforcent l’efficacité de nos règles, de nos produits et systèmes de sécurité et nous aident à les rendre plus accessibles auprès de notre communauté. Durant le premier semestre 2020, TikTok a noué plusieurs partenariats importants avec des organisations internationales faisant référence dans le domaine de la sécurité. Parmi les plus notables, ceux établis avec le [National Center for Missing and Exploited Children](https://www.tiktok.com/safety/youth-portal/get-access-to-more-info?lang=en&region=US) et avec la [WePROTECT Global Alliance](https://newsroom.tiktok.com/en-us/tiktok-joins-weprotect-global-alliance), au travers desquels nous nous engageons dans la lutte contre l’exploitation sexuelle des enfants et la diffusion de matériels pédopornographiques. En Inde, nous collaborons avec le [Data Security Council of India](https://newsroom.tiktok.com/en-in/dsci-and-tiktok-launch) pour sensibiliser le public aux gestes à adopter pour rester en sécurité sur internet. Aux États-Unis, nous avons mis sur pied notre [Conseil consultatif sur les contenus](https://www.tiktok.com/transparency), qui rassemble des spécialistes des questions telles que les discours haineux, à l’intelligence artificielle inclusive ou bien encore la sécurité des jeunes. Nous avons également constitué un [Creator Diversity Collective](https://newsroom.tiktok.com/en-us/progress-report-how-were-supporting-black-communities-and-promoting-diversity-and-inclusion) qui réunit des personnes d’horizons divers chargés de veiller à la diversité, l’inclusion et leur présence au sein de nos initiatives et de notre plateforme.

### Autres rapports

* [Rapport sur les demandes de retrait émanant de gouvernements](https://www.tiktok.com/transparency/fr-fr/government-removal-requests)
* [Rapport sur les demandes de renseignements](https://www.tiktok.com/transparency/fr-fr/information-requests)
* [Rapport de demandes de retrait de contenus pour infractions aux lois sur la propriété intellectuelle](https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests)

#### Cela a-t-il été utile ?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Oui![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)Non

# Resource URL: https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement-2019-2/
Application des Règles communautaires

_1er juillet 2019 –31 décembre 2019_  
_Date de publication : 9 juin 2020_

### À propos de ce rapport

TikTok est une plateforme d’expression créative et nous encourageons nos utilisateurs à y partager ce qui les rend uniques. Evoluer dans un environnement sûr permet à nos utilisateurs de se sentir à l’aise, libre de s’exprimer et de laisser libre cours à leur créativité. C’est pourquoi notre priorité absolue est de leur offrir un environnement sûr et une expérience positive.

Notre avons publié notre premier rapport de transparence le 30 décembre 2019 et nous nous sommes engagés à informer notre communauté de façon régulière afin de lui présenter la façon dont nous répondons aux demandes d’obtention de données et dont nous protégeons la propriété intellectuelle et ce, de façon responsable.

Le rapport de transparence a été complété pour fournir des informations concernant :

* Notre approche et nos politiques pour assurer la sécurité de notre communauté ;
* La manière dont nous définissons et renforçons nos Règles Communautaires ;
* Les outils mis à disposition de notre communauté et les démarches éducatives et de sensibilisation mises en oeuvre ;
* Le volume de vidéos supprimées pour non-respect de nos Règles Communautaires.

En complément, nous commençons à prendre en compte des éléments de mesure en lien avec neuf catégories de contenus.

Fin 2019, nous avons mis en place une nouvelle infrastructure de modération de contenu qui nous permet d’offrir plus de transparence lorsque nous indiquons les raisons pour lesquelles un contenu a été retiré. Dans ce rapport, nous mettons en avant les raisons qui ont conduit à la suppression de vidéos durant le mois de décembre au regard de cette nouvelle infrastructure de modération. Dans nos prochains rapports nous pourrons présenter ses données sur une période complète de six mois.

Nous mettons tout en oeuvre pour mériter chaque jour la confiance de notre communauté. Aussi, nous continuerons sans cesse à faire évoluer nos rapports de transparence afin de répondre aux questions de nos utilisateurs, du législateur et d’experts. Notre but principal est que TikTok reste un lieu source d’inspiration et de divertissement pour tous.

### Rapport sur les données

#### Vidéos

Sur la seconde partie de l’année (du 1e juillet au 31 décembre 2019), nous avons supprimé 49.247.689 de vidéos au global ce qui représente moins de 1% de l’ensemble des vidéos créées par nos utilisateurs. Ces vidéos ont été supprimées pour avoir avoir avoir enfreint nos Règles Communautaires ou nos conditions de services. Nos systèmes ont proactivement identifié et supprimé 98.2% de ces vidéos avant qu’un utilisateur ne les signale. Sur l’ensemble des vidéos, supprimées, 89.4% l’ont été avant qu’elles aient été vues.

Le tableau ci-après présente les 5 marchés avec le plus grand nombre de vidéos supprimées.

|     |     |
| --- | --- |
| **Pays** | **Total des retraits** |
| Inde | 16,453,360 |
| États-Unis | 4,576,888 |
| Pakistan | 3,728,162 |
| Royaume Uni | 2,022,728 |
| Russie | 1,258,853 |

Fin 2019, nous avons mis place un nouveau système de modération de contenu qui nous permet d’offrir plus de transparence lors de l’indication des raisons pour lesquelles un contenu a été retiré. Lorsqu’une vidéo va à l’encontre de nos Règles Communautaires, elle est identifiée en fonction de la règle ou des différentes règles qu’elle enfreint et est ensuite supprimée. Ainsi, une même vidéo peut apparaitre sous de multiples catégories. Pour le mois de décembre 2019, lorsque nous avons mis en place notre nouveau système de modération de contenu, la répartition des règles enfreintes s’appuie sur ces nouvelles catégories.

Au cours du mois de décembre, 25,5% des vidéos retirées l’ont été sous la catégorie ‘Nudité Adulte et activités sexuelles’. Pour préserver la sécurité des enfants, 24,8% des vidéos ont été supprimées car elles enfreignaient nos règles relatives à la protection des mineurs. Ceci couvre des contenus tels que des comportements à risque, dangereux ou des comportements illégaux pour des mineurs comme la consommation d’alcool ou de drogue ou bien encore plus graves et pour lesquels nous avons décidé un retrait immédiat et la fermeture des comptes incriminés. Ces derniers ont été signalés auprès de la NCMEC et des autorités compétentes lorsque nécessaire. Les contenus liés aux activités illégales et marchandises réglementées représentent 21,5% des suppressions. En outre, 15,6% des vidéos retirées l’ont été pour avoir enfreint nos règles relatives au suicide, à l’automutilation ou pour comportement dangereux qui reflète principalement la suppression de challenges à risque. Enfin, 8,6% des vidéos supprimées ont enfreint nos règles liées aux contenus violents et visuellement explicites ; 3% pour harcèlement ou intimidation ; et moins de 1% pour atteinte à nos règles relatives aux discours haineux, à l’intégrité et à l’authenticité ou relatives aux personnes ou organisations dangereuses.

Depuis, la majorité de nos contenus ont été traités par notre nouveau système de modération. Nos rapports ultérieurs comprendront des données plus détaillées, tant pour le contenu que les comptes et porteront sur l’ensemble de la période concernée.

### Synthèse

Pour TikTok, rien n’est plus important que d’assurer la sécurité de ses utilisateurs. En tant que plateforme mondiale, des milliers de collaborateurs travaillent au maintien d’un environnement sûr. Les contenus et comportements en ligne sont traités par un ensemble de règles, de technologies et de modérations, qui peuvent mener à la suppression de vidéos, au bannissement de comptes qui iraient à l’encontre de nos [Règles Communautaires](https://www.tiktok.com/community-guidelines) et de nos [Conditions de Services](https://www.tiktok.com/legal/terms-of-use).

### Responsabiliser la communauté via différents outils

En plus des technologies et des systèmes de modération mis en place (voir ci-dessous), de nombreuses fonctionnalités sont à la disposition des utilisateurs pour qu’ils puissent prendre le contrôle de leur expérience en ligne. Il s’agit de moyens très simples à activer pour restreindre l’accès à leur contenu, définir des filtres de commentaires automatiques, désactiver la fonction de messagerie ou bloquer d’autres utilisateurs. De plus, s’ils constatent un manquement à nos Règles Communautaires, nous les invitons à le signaler auprès de nos équipes, directement depuis l’application.

#### Confidentialité des comptes

TikTok propose un large éventail de paramètres de confidentialité que les utilisateurs peuvent activer lors de la création de leur compte ou à tout autre moment. Actuellement, concernant les comptes privés, seuls les abonnés autorisés peuvent visionner ou commenter les vidéos de l’utilisateur concerné ou lui envoyer un message direct. La fonctionnalité de messagerie peut être très simplement désactivée ou bien encore limitée ; cette fonctionnalité est désactivée et inaccessible sur les comptes des utilisateurs de moins de 16 ans. Par ailleurs, les utilisateurs peuvent supprimer un abonné ou bien encore empêcher un autre utilisateur de les contacter.

### Autres contrôles de contenus

Les créateurs de contenu doivent avoir le contrôle sur la façon dont les autres utilisateurs peuvent interagir avec leurs vidéos. TikTok propose aux utilisateurs de nombreuses options pour paramétrer leurs comptes et vidéos comme par exemple la possibilité de limiter les commentaires ou de décider de qui peut réaliser des duos. Ils peuvent également activer des filtres de commentaires en créant une liste personnalisée de mots clés qui seront automatiquement bloqués. Ils peuvent également désactiver les commentaires sur une vidéo spécifique, limiter les commentaires à un public sélectionné ou désactiver entièrement les commentaires sur leurs vidéos.

De plus, nous sensibilisons régulièrement nos utilisateurs aux options de sécurité et de bien-être digital à leur disposition via des formats vidéo diffusés sur l’application et le [Centre de sécurité](https://www.tiktok.com/safety/resources). Par exemple, nous avons réalisé avec l’aide de créateurs une série de vidéos qui encouragent les utilisateurs à maîtriser leur temps d’écran. Notre fonction de gestion du temps d’écran permet aux utilisateurs de fixer un seuil maximum d’utilisation de l’application. Ils peuvent également décider d’activer le mode restreint qui limite l’apparition de contenus qui pourraient ne pas convenir à tous les publics. Ces fonctionnalités sont disponibles en permanence dans la section Bien-être numérique des paramètres de l’application.

### Accorder nos Règles Communautaires avec les attentes de notre communauté

TikTok est une communauté mondiale dont les membres sont à la recherche d’une expérience authentique et positive. Notre engagement envers notre communauté débute avec nos Règles Communautaires. Elles constituent un code de conduite qui permet de proposer un environnement sécurisé et bienveillant. Ces Règles Communautaires sont régulièrement mises à jour afin de protéger nos utilisateurs de tendances et contenus qui pourraient être dangereux. Elles sont destinées à favoriser la confiance, le respect et la dimension positive de la communauté TikTok. Nous attendons de l’ensemble de nos utilisateurs qu’ils respectent nos Règles Communautaires pour que TikTok reste un lieu divertissant, fun, où chacun est le bienvenu. Le non-respect de ces règles peut entraîner la suppression de contenus et de comptes.

### Application de nos règles

A travers le monde, des dizaines de milliers de vidéos sont téléchargées sur TikTok chaque minute. Chacune de ces vidéos exige une responsabilité accrue de notre part pour protéger la sécurité et le bien-être de nos utilisateurs. Pour faire respecter nos Règles Communautaires, nous utilisons un ensemble de technologies et de systèmes de modération de contenu qui identifient et suppriment les contenus et les comptes qui les transgresseraient.

#### Technologie

Le volet technologique est un élément clé dans la mise en œuvre de nos politiques ; nos systèmes sont développés pour signaler automatiquement les contenus qui pourraient transgresser nos Règles Communautaires. Ces systèmes prennent en compte des modèles d’identification de formes ou de comportements afin de signaler les contenus susceptibles d’enfreindre nos Règles Communautaires. Ceci nous permet de prendre des mesures rapides et de limiter les préjudices éventuels. Par exemple, la grande majorité des contenus tels que les spams et les escroqueries sont automatiquement supprimés. Nous étudions régulièrement l’évolution des tendances, les résultats de travaux universitaires et les meilleures pratiques du secteur pour améliorer constamment nos systèmes.

### Autres rapports

* [Rapport sur les demandes de retrait émanant de gouvernements](https://www.tiktok.com/transparency/fr-fr/government-removal-requests)
* [Rapport sur les demandes de renseignements](https://www.tiktok.com/transparency/fr-fr/information-requests)
* [Rapport de demandes de retrait de contenus pour infractions aux lois sur la propriété intellectuelle](https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests)

#### Cela a-t-il été utile ?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Oui![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)Non